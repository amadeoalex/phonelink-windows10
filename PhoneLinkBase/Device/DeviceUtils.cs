﻿using DeviceNS;
using PhoneLinkBase.DeviceNS.Connection.Pairing;
using PhoneLinkBase.Service.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneLinkBase.DeviceNS
{
	static class DeviceUtils
	{
		public static string GetDeviceName()
		{
			return Environment.MachineName;
		}

		public static string GetDeviceType()
		{
			return Utils.IsDesktopMachine() ? Device.TYPE_PC : Device.TYPE_LAPTOP;
		}

		public static NetPackage GetDeviceLookupPackage()
		{
			return new NetPackage(DeviceDiscovery.PACKAGE_TYPE_LOOKUP)
			{
				[Device.KEY_ID] = GetDeviceName(),
				[Device.KEY_TYPE] = GetDeviceType()
			};
		}

		public static NetPackage GetDeviceIdentityPackage(int port)
		{
			NetPackage netPackage = GetDeviceLookupPackage();
			netPackage.Type = PairingProcess.PACKAGE_TYPE_IDENTITY;
			netPackage[Device.KEY_TCP_PORT] = port;
			netPackage[Device.KEY_CERTIFICATE] = Convert.ToBase64String(SecurityUtils.GetCertificate().RawData);

			return netPackage;
		}

		public static NetPackage GetDeviceLookupResponsePackage()
		{
			return new NetPackage(DeviceDiscovery.PACKAGE_TYPE_LOOKUP_RESPONSE)
			{
				[Device.KEY_ID] = GetDeviceName(),
				[Device.KEY_TYPE] = GetDeviceType()
			};
}
	}
}
