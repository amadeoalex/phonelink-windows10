﻿using PhoneLinkBase.Service;
using PhoneLinkBase.Service.Network;

namespace DeviceNS
{
	public static class DeviceManagerHelper
	{
		public static void SendNetPackageToAll(NetPackage netPackage)
		{
			foreach (Device device in PhoneLinkService.DeviceManagerI.Devices)
				device.Connection?.SendNetPackage(netPackage);
		}
	}
}
