﻿using PhoneLinkBase;
using PhoneLinkBase.DeviceNS.Connection;
using PhoneLinkBase.Feature;
using System;
using System.Json;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading;

namespace DeviceNS.Types
{
	public sealed class Computer : Device
	{
		private const string TAG = "Computer";

		#region Properties
		/// <summary>
		/// Returns device id.
		/// </summary>
		public override string Id { get; protected set; }
		/// <summary>
		/// Returns device name.
		/// </summary>
		public override string Name { get; protected set; }
		/// <summary>
		/// Returns device type.
		/// </summary>
		public override string Type { get; protected set; } = TYPE_PC;
		/// <summary>
		/// Returns device certificate.
		/// </summary>
		public override byte[] Certificate { get; set; }
		/// <summary>
		/// Returns device IP address.
		/// </summary>
		public override string Address { get; set; }
		/// <summary>
		/// Returns device TCP port to which it tries to connect.
		/// </summary>
		public override int TcpPort { get; set; }
		/// <summary>
		/// Returns device TCP port at which it listens for connections.
		/// </summary>
		public override int ListeningTCPPort { get; set; }
		/// <summary>
		/// Return the SSID of the network the device is linked to
		/// </summary>
		public override string SSID { get; set; }
		/// <summary>
		/// Return the BSSID of the network the device is linked to
		/// </summary>
		public override string BSSID { get; set; }
		/// <summary>
		/// Returns device connection server
		/// </summary>
		public override DeviceConnection Connection { get; set; }
		/// <summary>
		/// Returns device feature manager
		/// </summary>
		public override FeatureManager FeatureManager { get; set; }
		#endregion


		#region Constructors

		public Computer(string deviceId, string name, byte[] certificate, string address, int tcpPort, int listeningTCPPort)
		{
			Id = deviceId;
			Name = name;
			Certificate = certificate;
			Address = address;
			TcpPort = tcpPort;
			ListeningTCPPort = listeningTCPPort;
		}

		public Computer() : this("", "", Array.Empty<byte>(), "0.0.0.0", 0, 0)
		{

		}

		#endregion

	}
}
