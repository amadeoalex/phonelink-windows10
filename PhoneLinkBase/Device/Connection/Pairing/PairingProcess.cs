﻿using DeviceNS;
using PhoneLinkBase.DeviceNS;
using PhoneLinkBase.Service;
using PhoneLinkBase.Service.Network;
using PhoneLinkBase.UtilClasses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace PhoneLinkBase.DeviceNS.Connection.Pairing
{
	public class PairingProcess : SimpleThread
	{
		private const string TAG = "Pairing Process";

		public const string PACKAGE_TYPE_PAIR_REQUEST = "pairRequest";
		public const string PACKAGE_TYPE_IDENTITY = "identity";
		public const string PORT_CHECK_OK = "portCheckOk";

		private readonly DeviceManager deviceManager;
		private readonly Device device;
		private readonly bool isClient;
		private readonly UDPServer udpServer;

		public enum Status
		{
			Initializing,
			WaitingForAccept,
			Pairing,
			Finished,
			Error
		}

		public delegate void OnUpdateDelegate(Status status);
		public delegate void OnFinishedDelegate(bool success, Device device);

		public OnUpdateDelegate OnUpdate { get; set; }
		public OnFinishedDelegate OnFinished { get; set; }

		//public interface OnUpdateListener
		//{
		//	void OnUpdate(Status status);
		//}

		//public interface OnFinishedListener
		//{
		//	void OnFinished(bool success, Device device);
		//}

		//public OnUpdateListener UpdateListener { get; set; }
		//public OnFinishedListener FinishedListener { get; set; }

		public PairingProcess(DeviceManager deviceManager, Device device, bool isClient, UDPServer udpServer) : base(TAG)
		{
			this.deviceManager = deviceManager;
			this.device = device;
			this.isClient = isClient;
			this.udpServer = udpServer;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Intended")]
		public override void Run()
		{
			TcpClient tcpClient = null;
			SslStream sslStream = null;
			StreamReader streamReader = null;
			StreamWriter streamWriter = null;

			OnUpdate?.Invoke(Status.Initializing);

			int connectionPort = GetConnectionPort(device.TcpPort);

			try
			{
				Device deviceCandidate = null;

				if (isClient)
				{
					Log.D(TAG, $"trying to connect to {device.Address} at {connectionPort}");

					using (TimeoutTcpClient timeoutTcpClient = new TimeoutTcpClient())
					{
						OnUpdate?.Invoke(Status.WaitingForAccept);
						tcpClient = timeoutTcpClient.Connect(device.Address, connectionPort, 1000);


						sslStream = new SslStream(tcpClient.GetStream(), false, ValidateCertificate);
						if (isClient)
							sslStream.AuthenticateAsClient(device.Address);
						else
							sslStream.AuthenticateAsServer(SecurityUtils.GetCertificate());
						streamReader = new StreamReader(sslStream);
						streamWriter = new StreamWriter(sslStream)
						{
							AutoFlush = true
						};

						OnUpdate?.Invoke(Status.Pairing);

						Log.D(TAG, "before my");
						NetPackage myIdPackage = SendMyIdentityPackage(streamWriter, streamReader);
						Log.D(TAG, "before remote");
						NetPackage remoteDeviceIdPackage = GetRemoteDeviceIdentityPackage(streamWriter, streamReader);
						Log.D(TAG, "after remote");

						remoteDeviceIdPackage[Device.KEY_LISTENING_TCP_PORT] = myIdPackage[Device.KEY_TCP_PORT];
						deviceCandidate = Device.CreateFromIdNetPackage(remoteDeviceIdPackage);
					}
				}
				else
				{
					Log.D(TAG, "sending pair request package");
					NetPackage pairRequestPackage = DeviceUtils.GetDeviceLookupResponsePackage();
					pairRequestPackage.Type = PACKAGE_TYPE_PAIR_REQUEST;
					pairRequestPackage[Device.KEY_TCP_PORT] = deviceManager.GetFreePort();

					udpServer.SendNetPackage(pairRequestPackage, new IPEndPoint(IPAddress.Parse(device.Address), UDPServer.LISTEN_PORT));

					Log.D(TAG, $"creating listener at {pairRequestPackage[Device.KEY_TCP_PORT]}");

					using (TimeoutTcpListener timeoutTcpListener = new TimeoutTcpListener(IPAddress.Any, pairRequestPackage[Device.KEY_TCP_PORT])) //TODO: consider using device addres instead of IPAddress.Any
					{
						OnUpdate?.Invoke(Status.WaitingForAccept);
						tcpClient = timeoutTcpListener.Accept(8000);

						sslStream = new SslStream(tcpClient.GetStream(), false, ValidateCertificate);
						if (isClient)
							sslStream.AuthenticateAsClient(device.Address);
						else
							sslStream.AuthenticateAsServer(SecurityUtils.GetCertificate());
						streamReader = new StreamReader(sslStream);
						streamWriter = new StreamWriter(sslStream)
						{
							AutoFlush = true
						};

						OnUpdate?.Invoke(Status.Pairing);

						Log.D(TAG, "before remote");

						NetPackage remoteDeviceIdPackage = GetRemoteDeviceIdentityPackage(streamWriter, streamReader);
						Log.D(TAG, "before my");
						NetPackage myIdPackage = SendMyIdentityPackage(streamWriter, streamReader);
						Log.D(TAG, "after my");

						remoteDeviceIdPackage[Device.KEY_LISTENING_TCP_PORT] = myIdPackage[Device.KEY_TCP_PORT];
						deviceCandidate = Device.CreateFromIdNetPackage(remoteDeviceIdPackage);
					}
				}

				if (deviceCandidate == null)
				{
					Log.D(TAG, "device candidate is invalid");
					OnUpdate?.Invoke(Status.Error);
				}
				else
				{
					OnUpdate?.Invoke(Status.Finished);
					deviceCandidate.Address = device.Address;
				}


				OnFinished?.Invoke(deviceCandidate != null, deviceCandidate);
			}
			catch (TimeoutException e)
			{
				Log.D(TAG, $"timeout exception: {e.Message}");
				OnUpdate?.Invoke(Status.Error);
			}
/*			catch (Exception e)
			{
				Log.D(TAG, $"exception occurred: {e.Message}");
				OnUpdate?.Invoke(Status.Error);
			}*/
			finally
			{
				if (tcpClient != null)
					tcpClient.Close();

				if (sslStream != null)
					sslStream = null;

				if (streamReader != null)
					streamReader = null;

				if (streamWriter != null)
					streamWriter = null;
			}
		}

		private int GetConnectionPort(int port)
		{
			if (port == 6161)
#if DEBUG
				return 6162;
#else
					return port
#endif
			else
				return port;
		}

		private bool ValidateCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
		{
			return true;
		}

		private NetPackage SendMyIdentityPackage(StreamWriter streamWriter, StreamReader streamReader)
		{
			NetPackage idPackage;
			do
			{
				int port = deviceManager.GetFreePort();
				idPackage = DeviceUtils.GetDeviceIdentityPackage(port);
				streamWriter?.WriteLine(idPackage.ToString());
			} while (streamReader?.ReadLine() != PORT_CHECK_OK);

			return idPackage;
		}

		private NetPackage GetRemoteDeviceIdentityPackage(StreamWriter streamWriter, StreamReader streamReader)
		{
			NetPackage idPackage;
			while (true)
			{
				idPackage = NetPackage.Deserialize(streamReader?.ReadLine());

				if (deviceManager.CheckPortAvailability(idPackage[Device.KEY_TCP_PORT]))
				{
					streamWriter?.WriteLine(PORT_CHECK_OK);
					break;
				}
				else
					streamWriter?.WriteLine(" ");
			}

			return idPackage;
		}
	}
}
