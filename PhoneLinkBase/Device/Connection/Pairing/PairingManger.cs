﻿using DeviceNS;
using PhoneLinkBase.DeviceNS;
using PhoneLinkBase.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneLinkBase.DeviceNS.Connection.Pairing
{
	public class PairingManager
	{
		private readonly object lockObject = new object();

		private DeviceManager deviceManager;
		private UDPServer udpServer;

		private Dictionary<Device, PairingProcess> pairingProcesses = new Dictionary<Device, PairingProcess>();

		public PairingManager(DeviceManager deviceManager, UDPServer udpServer)
		{
			this.deviceManager = deviceManager;
			this.udpServer = udpServer;
		}

		public PairingProcess GetPairingProcess(Device device, bool isClient)
		{
			lock (lockObject)
			{
				if (!pairingProcesses.ContainsKey(device) || pairingProcesses[device] == null || (!pairingProcesses[device].IsAlive && pairingProcesses[device].Finished))
					pairingProcesses[device] = new PairingProcess(deviceManager, device, isClient, udpServer);

				return pairingProcesses[device];
			}
		}

		public PairingProcess PeekPairingProcess(Device device)
		{
			lock (lockObject)
			{
				return pairingProcesses[device];
			}
		}

		public void Cleanup()
		{
			lock (lockObject)
			{
				foreach (var entry in pairingProcesses)
				{
					entry.Value.Interrupt();
					entry.Value.Join();
				}
			}
		}
	}
}