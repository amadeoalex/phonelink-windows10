﻿using PhoneLinkBase.Service.Network;
using PhoneLinkBase.UtilClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneLinkBase.DeviceNS.Connection
{
	public abstract class DeviceConnection : SimpleThread
	{
		public abstract void SendNetPackage(NetPackage netPackage);
		public abstract void Disconnect();
	}
}
