﻿using PhoneLinkBase.Service.Network;
using PhoneLinkBase.UtilClasses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PhoneLinkBase.DeviceNS.Connection.Network
{
	class DevicePackageListener : SimpleThread
	{
		private const string TAG = "Package Listener";

		public interface IOnNetPackageEventListener
		{
			void OnNetPackageReceived(NetPackage netPackage);
			void OnConnectionBroken();
		}

		public IOnNetPackageEventListener OnNetPackageEventListener { get; set; }


		private readonly StreamReader streamReader;
		private bool running = true;

		public DevicePackageListener(StreamReader streamReader) : base(TAG)
		{
			this.streamReader = streamReader;
		}

		public override void Run()
		{
			Log.D(TAG, "started");

			while (running)
			{
				try
				{
					string serialized = streamReader.ReadLine();
					if(string.IsNullOrEmpty(serialized)) throw new IOException("serialized was null or empty");

					NetPackage netPackage = NetPackage.Deserialize(serialized);
					Log.I(TAG, $"received package {netPackage.Type} WId: {netPackage.OptInt(DeviceConnectionWatchdog.KEY_WATCHDOG_ID, -1234)}");

					OnNetPackageEventListener.OnNetPackageReceived(netPackage);
				}
				catch (Exception e) when (e is SocketException || e is IOException)
				{
					Log.D(TAG, $"connection problem, breaking: {e.Message}");
					Interrupt();

					OnNetPackageEventListener.OnConnectionBroken();
				}
			}

			Log.D(TAG, "thread finished");
		}

		public override void Interrupt()
		{
			base.Interrupt();

			if (running)
			{
				running = false;
				Log.D(TAG, "interrupted");
			}
		}
	}
}
