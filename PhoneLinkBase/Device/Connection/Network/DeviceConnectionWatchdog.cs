﻿using PhoneLinkBase.Service.Network;
using System;
using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace PhoneLinkBase.DeviceNS.Connection.Network
{
	class DeviceConnectionWatchdog
	{
		private const string TAG = "Device Connection Watchdog";

		public interface IOnConnectionBrokenListener
		{
			void OnConnectionBroken();
		}

		public IOnConnectionBrokenListener OnConnectionBrokenListener { get; set; }

		public const string PACKAGE_TYPE_WATCHDOG_CONFIRMATION = "watchdogConfirmation";
		public const string KEY_WATCHDOG_ID = "watchdogId";

		private const int DELAY = 5000;

		private int lastId = 0;
		private List<int> receivedNetPakcages = new List<int>();

		private readonly Timer timer;

		public DeviceConnectionWatchdog()
		{
			timer = new Timer(DELAY);
			timer.Elapsed += Check;
			timer.AutoReset = false;
		}

		private void Check(object sender, ElapsedEventArgs e)
		{
			Log.D(TAG, "check");
			if (!receivedNetPakcages.Contains(lastId))
			{
				Log.D(TAG, $"check failed, required {lastId}, received {receivedNetPakcages}");
				OnConnectionBrokenListener.OnConnectionBroken();
			}
			receivedNetPakcages.Clear();
		}

		public void OnSent(NetPackage netPackage)
		{
			if (netPackage.Type == PACKAGE_TYPE_WATCHDOG_CONFIRMATION)
				return;

			lastId = netPackage[KEY_WATCHDOG_ID];
			timer.Stop();
			timer.Start();
		}

		public void OnReceived(NetPackage netPackage)
		{
			receivedNetPakcages.Add(netPackage[KEY_WATCHDOG_ID]);
		}

		internal void Cleanup()
		{
			Dispose();
		}

		#region IDisposable Support

		private bool disposed;
		public void Dispose()
		{
			if (disposed)
				return;

			timer.Dispose();
			disposed = true;
		}

		~DeviceConnectionWatchdog()
		{
			Dispose();
		}

		#endregion
	}
}
