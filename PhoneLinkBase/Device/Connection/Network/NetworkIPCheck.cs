﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Documents;
using DeviceNS;
using Org.BouncyCastle.Utilities.Encoders;
using PhoneLinkBase.Service;
using PhoneLinkBase.Service.Network;

namespace PhoneLinkBase.DeviceNS.Connection.Network
{
	class NetworkIPCheck : IUDPNetPackageHandler
	{
		private const string TAG = "NetworkIPCheck";

		/// <summary>
		/// Package should contain:
		///		[<see cref="Device.KEY_ID"/>] - id of the device requesting ip check
		///		[<see cref="KEY_IP_CHECK_DATA"/> - base64 string of bytes to be decrypted and sent back
		/// </summary>
		private const string PACKAGE_TYPE_IP_CHECK = "ipCheck";

		/// <summary>
		/// Package should contain:
		///		[<see cref="Device.KEY_ID"/>] - id of the device which requested the ip check
		///		[<see cref="KEY_IP_CHECK_DATA"/> - decrypted data in string form
		/// </summary>
		private const string PACKAGE_TYPE_IP_CHECK_RESPONSE = "ipCheckResponse";

		private const string KEY_IP_CHECK_DATA = "ipCheckToken";

		private const int TOKEN_LIFE_SPAN = 1000 * 5;

		private readonly List<CheckToken> tokens = new List<CheckToken>();

		private readonly Device device;
		private readonly UDPServer udpServer;

		public bool IPCheckRequired { get; private set; }

		public List<string> PackageTypes => throw new NotImplementedException();

		public NetworkIPCheck(Device device, UDPServer udpServer)
		{
			this.device = device;
			this.udpServer = udpServer;

			udpServer.AddPackageHandler(this);
		}

		public void Perform()
		{
			if (!IPCheckRequired)
				return;

			//TODO("consider making device address variable volatile")
			Log.D(TAG, $"check ip for {device.Id}");


			SendIPCheckRequest();
			Thread.Sleep(2000);
		}

		public bool OnNetPakcageFailed()
		{
			if (IPCheckRequired)
			{
				IPCheckRequired = false;
				return true;
			}
			else
				return false;
		}

		private void SendIPCheckRequest()
		{
			string ipCheckToken = SecurityUtils.GenerateRandomString();
			tokens.Add(new CheckToken(ipCheckToken, DateTime.Now.Millisecond + TOKEN_LIFE_SPAN));

			byte[] encryptedData = SecurityUtils.Encrypt(Encoding.ASCII.GetBytes(ipCheckToken), SecurityUtils.GetPublicKey());
			udpServer.SendNetPackage(new NetPackage(PACKAGE_TYPE_IP_CHECK)
			{
				[Device.KEY_ID] = device.Id,
				[KEY_IP_CHECK_DATA] = Convert.ToBase64String(encryptedData)
			}, new IPEndPoint(IPAddress.Parse(device.Address), UDPServer.LISTEN_PORT));
		}

		private class CheckToken
		{
			public string Token { get; }
			public long ExpirationTime { get; }

			public CheckToken(string token, long expirationTime)
			{
				this.Token = token;
				this.ExpirationTime = expirationTime;
			}
		}


		public bool HandleUDPNetPackage(NetPackage netPackage, IPEndPoint endPoint)
		{
			if (!netPackage.ContainsKey(Device.KEY_ID) || netPackage[Device.KEY_ID] != device.Id)
				return false;

			bool handled = false;
			switch (netPackage.Type)
			{
				case PACKAGE_TYPE_IP_CHECK:
					{
						if (!netPackage.ContainsKey(KEY_IP_CHECK_DATA)) throw new InvalidOperationException($"missing {nameof(KEY_IP_CHECK_DATA)}");

						tokens.RemoveAll(token => token.ExpirationTime < DateTime.Now.Millisecond);

						byte[] encryptedData = Convert.FromBase64String(netPackage[KEY_IP_CHECK_DATA]);
						string data = Encoding.ASCII.GetString(SecurityUtils.Decrypt(encryptedData, SecurityUtils.GetPrivateKey()));

						udpServer.SendNetPackage(new NetPackage(PACKAGE_TYPE_IP_CHECK_RESPONSE)
						{
							[Device.KEY_ID] = device.Id,
							[KEY_IP_CHECK_DATA] = data
						}, endPoint);

						handled = true;
						break;
					}

				case PACKAGE_TYPE_IP_CHECK_RESPONSE:
					{
						if (!netPackage.ContainsKey(KEY_IP_CHECK_DATA)) throw new InvalidOperationException($"missing {nameof(KEY_IP_CHECK_DATA)}");

						tokens.RemoveAll(token => token.ExpirationTime < DateTime.Now.Millisecond);

						if (tokens.Find(token => token.Token == netPackage[KEY_IP_CHECK_DATA]) != null)
						{
							Log.D(TAG, $"updating {device.Id} address from {device.Address} to {endPoint.Address.ToString()}");
							device.Address = endPoint.Address.ToString();
						}
						else
							Log.W(TAG, "receviced ip check response but token is not valid");

						handled = true;
						break;
					}
			}


			return handled;
		}
	}
}
