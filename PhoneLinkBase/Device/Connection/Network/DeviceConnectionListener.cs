﻿using DeviceNS;
using PhoneLinkBase.UtilClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace PhoneLinkBase.DeviceNS.Connection.Network
{
	class DeviceConnectionListener : SimpleThread
	{
		private const string TAG = "Connection Listener";

		public interface IOnNewConnectionListener
		{
			void OnNewConnection(Socket socket);
		}

		public IOnNewConnectionListener OnNewConnectionListener { get; set; }

		private readonly Device device;
		private bool running = true;
		private readonly TcpListener tcpListener;

		public DeviceConnectionListener(Device device) : base(TAG)
		{
			this.device = device;
			tcpListener = new TcpListener(IPAddress.Any, device.TcpPort);
		}

		public override void Run()
		{
			Log.D(TAG, $"starting at {device.TcpPort}");

			try
			{
				tcpListener.Start();
				TcpClient tcpClient = tcpListener.AcceptTcpClient();
				OnNewConnectionListener?.OnNewConnection(tcpClient.Client);
			}
			catch (Exception e)
			{
				if (running)
					Log.W(TAG, e);
			}
			finally
			{
				running = false;
				tcpListener?.Stop();
			}

			Log.D(TAG, "thread finished");
		}

		public override void Interrupt()
		{
			base.Interrupt();

			if (running)
			{
				running = false;
				tcpListener?.Stop();
			}
		}
	}
}
