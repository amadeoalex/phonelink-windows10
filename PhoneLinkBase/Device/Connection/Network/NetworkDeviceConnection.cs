﻿using DeviceNS;
using PhoneLinkBase.DeviceNS.Connection;
using PhoneLinkBase.DeviceNS.Connection.Network;
using PhoneLinkBase.Service;
using PhoneLinkBase.Service.Network;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static PhoneLinkBase.DeviceNS.Connection.Network.DeviceConnectionListener;
using static PhoneLinkBase.DeviceNS.Connection.Network.DeviceConnectionWatchdog;
using static PhoneLinkBase.DeviceNS.Connection.Network.DevicePackageListener;

namespace PhoneLinkBase.DeviceNS.Connection.Network
{
	class NetworkDeviceConnection : DeviceConnection, IOnNewConnectionListener, IOnNetPackageEventListener, IOnConnectionBrokenListener
	{
		private const string TAG = "Connection Server";
		/**
		 * Package should contain:
		 * [KEY_ID] - id of the device requesting ip check
		 * [KEY_IP_CHECK_DATA] - base64 string of bytes to be decrypted and sent back
		 */
		private const string PACKAGE_TYPE_IP_CHECK = "ipCheck";
		/**
		 * Package should contain:
		 * [KEY_ID] - id of the device which requested the ip check
		 * [KEY_IP_CHECK_DATA] - decrypted data in string form
		 */
		private const string PACKAGE_TYPE_IP_CHECK_RESPONSE = "ipCheckResponse";
		private const string KEY_IP_CHECK_DATA = "ipCheckToken";

		private BlockingCollection<NetPackage> netPackages = new BlockingCollection<NetPackage>();
		private readonly CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

		private Device device;

		private TcpClient tcpClient;
		private Socket socket;
		private SslStream sslStream;
		private DeviceConnectionListener connectionListener;
		private StreamWriter streamWriter;
		private StreamReader streamReader;
		private DevicePackageListener packageListener;
		private DeviceConnectionWatchdog connectionWatchdog;
		private NetworkIPCheck ipCheck;

		private bool running = true;

		public NetworkDeviceConnection(Device device, UDPServer udpServer)
		{
			this.device = device;

			connectionWatchdog = new DeviceConnectionWatchdog()
			{
				OnConnectionBrokenListener = this
			};

			ipCheck = new NetworkIPCheck(device, udpServer);
		}

		public override void SendNetPackage(NetPackage netPackage)
		{
			if (!netPackage.ContainsKey(KEY_WATCHDOG_ID))
				netPackage[KEY_WATCHDOG_ID] = DateTime.Now.Millisecond;

			netPackages.Add(netPackage);
		}

		/// <summary>
		/// Adds the <paramref name="netPackage"/> as the first.
		/// REPLACES NET PACKAGE COLLECTION WITH NEW ONE, USE ONLY IN MAIN CONNECTION THREAD
		/// </summary>
		/// <param name="netPackage"></param>
		private void AddNetPackageAsFirst(NetPackage netPackage)
		{
			netPackages = new BlockingCollection<NetPackage>(new ConcurrentQueue<NetPackage>(netPackages.Reverse()));
			netPackages.Add(netPackage);
			netPackages = new BlockingCollection<NetPackage>(new ConcurrentQueue<NetPackage>(netPackages.Reverse()));
		}

		public override void Run()
		{
			StartConnectionListener();

			while (running)
			{
				NetPackage netPackage = new NetPackage();

				try
				{
					Log.D(TAG, "taking package");
					netPackage = netPackages.Take(cancellationTokenSource.Token);

					if (socket == null)
					{
						ipCheck.Perform();

						Log.D(TAG, $"socket is null, trying to conect to {device.Id}");
						tcpClient = new TcpClient(device.Address, device.ListeningTCPPort);

						Log.D(TAG, "socket connected");
						connectionListener?.Interrupt();
						OnNewConnection(tcpClient.Client, false);
					}

					Log.D(TAG, $"sending {netPackage.Type} WId: {netPackage[KEY_WATCHDOG_ID]}");
					streamWriter.WriteLine(netPackage.ToString());
					connectionWatchdog.OnSent(netPackage);
				}
				catch (OperationCanceledException)
				{
					Log.I(TAG, "Take() interrupted");
					break;
				}
				catch (SocketException e)
				{
					switch (e.SocketErrorCode)
					{
						case SocketError.ConnectionRefused:
							{
								Log.D(TAG, $"connect exception {e.Message}");
								socket = null;

								if (ipCheck.OnNetPakcageFailed())
									device.FeatureManager.OnNetPackageFailed(netPackage);
								else
									AddNetPackageAsFirst(netPackage);

								break;
							}
						case SocketError.TimedOut:
							{
								Log.D(TAG, $"socket timed out exception {e.Message}");
								socket = null;

								if (ipCheck.OnNetPakcageFailed())
									device.FeatureManager.OnNetPackageFailed(netPackage);
								else
									AddNetPackageAsFirst(netPackage);

								break;
							}
						default:
							{
								Log.D(TAG, $"socket exception, closing socket and trying again: {e.Message}");
								socket?.Close();
								socket = null;

								AddNetPackageAsFirst(netPackage);
								break;
							}
					}

				}
				catch (Exception e)
				{
					Log.D(TAG, $"unknown exception: {e.Message}");
					Debugger.Break();
					break;
				}
			}

			connectionWatchdog.Cleanup();

			tcpClient?.Close();
			socket?.Close();
			sslStream?.Close();
			streamReader?.Close();
			streamWriter?.Close();

			connectionListener?.Interrupt();
			connectionListener?.Join();

			packageListener?.Interrupt();
			packageListener?.Join();

			Log.D(TAG, "thread finished");
		}

		private bool ValidateServerCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
		{
			if ((sslPolicyErrors & SslPolicyErrors.RemoteCertificateNameMismatch) > 0 || (sslPolicyErrors & SslPolicyErrors.RemoteCertificateNotAvailable) > 0)
				return false;

			X509Certificate2 deviceCertificate = new X509Certificate2(device.Certificate);
			if (((X509Certificate2)certificate).Thumbprint != deviceCertificate.Thumbprint)
				return false;

			X509Chain additionalChain = new X509Chain
			{
				ChainPolicy =
				{
					VerificationFlags = X509VerificationFlags.AllowUnknownCertificateAuthority
				}
			};

			bool verified = additionalChain.Build((X509Certificate2)certificate);
			additionalChain.Reset();

			return verified;
		}

		public void OnNewConnection(Socket socket) => OnNewConnection(socket, true);
		public void OnNewConnection(Socket socket, bool isServer)
		{
			Log.D(TAG, "preparing connection");
			this.socket = socket;
			sslStream = new SslStream(new NetworkStream(socket), false, ValidateServerCertificate);

			if (isServer)
				sslStream.AuthenticateAsServer(SecurityUtils.GetCertificate(), true, SslProtocols.Default, false);
			else
				sslStream.AuthenticateAsClient(device.Id, new X509Certificate2Collection(SecurityUtils.GetCertificate()), SslProtocols.Default, false);

			streamWriter = new StreamWriter(sslStream)
			{
				AutoFlush = true
			};
			streamReader = new StreamReader(sslStream);

			if (packageListener?.IsAlive == true) throw new InvalidOperationException("package listener should be dead");
			packageListener = new DevicePackageListener(streamReader);
			packageListener.OnNetPackageEventListener = this;
			packageListener.Start();

			device.FeatureManager.OnConnected();
		}

		private void CheckIpAddress()
		{
			//TODO: implement
		}

		private void StartConnectionListener()
		{
			if (!running)
			{
				Log.D(TAG, "thread is not running, startConnectionListener() ignored");
				return;
			}

			socket?.Close();
			socket = null;

			if (connectionListener?.IsAlive == true) throw new InvalidOperationException("startConnectionListener() should not be called when connection listener is already running");

			connectionListener = new DeviceConnectionListener(device);
			connectionListener.OnNewConnectionListener = this;
			connectionListener.Start();
		}

		public void OnNetPackageReceived(NetPackage netPackage)
		{
			connectionWatchdog.OnReceived(netPackage);

			if (netPackage.Type != PACKAGE_TYPE_WATCHDOG_CONFIRMATION)
			{
				SendNetPackage(new NetPackage(PACKAGE_TYPE_WATCHDOG_CONFIRMATION)
				{
					[KEY_WATCHDOG_ID] = netPackage[KEY_WATCHDOG_ID]
				});
			}

			device.FeatureManager.OnNetPackageReceived(netPackage);
		}

		public void OnConnectionBroken()
		{
			socket?.Close();

			device.FeatureManager.OnDisconnected();

			StartConnectionListener();
		}

		public override void Disconnect()
		{
			socket?.Close();
		}

		public override void Interrupt()
		{
			base.Interrupt();

			if (running)
			{
				running = false;
				connectionWatchdog.Cleanup();

				tcpClient.Close();
				socket?.Close();
				sslStream?.Close();
				streamReader?.Close();
				streamWriter?.Close();

				connectionListener?.Interrupt();
				connectionListener?.Join();

				packageListener?.Interrupt();
				packageListener?.Join();
			}
		}
	}
}
