﻿using DeviceNS;
using DeviceNS.Types;
using PhoneLinkBase.DeviceNS.Connection.Network;
using PhoneLinkBase.Feature;
using PhoneLinkBase.Service;
using System;
using System.Collections.ObjectModel;
using System.Json;
using System.Linq;
using System.Windows.Data;

namespace PhoneLinkBase.DeviceNS
{
	public class DeviceManager : IDisposable
	{
		private const string TAG = "Device Manager";

		private const string KEY_SERIALIZED_DEVICES = "devices";

		private readonly object devicesLockObject = new object();
		public ObservableCollection<Device> Devices { get; } = new ObservableCollection<Device>();

		private readonly JsonObject preferences;
		private readonly UDPServer udpServer;
		public readonly DeviceDiscovery DeviceDiscovery;

		private readonly Random random = new Random();

		public DeviceManager(UDPServer udpServer)
		{
			BindingOperations.EnableCollectionSynchronization(Devices, devicesLockObject);

			preferences = PreferenceManager.GetObjectPreference(this.GetType().Name);
			this.udpServer = udpServer;
			DeviceDiscovery = new DeviceDiscovery(udpServer);

			DeserializeDevices();

			Add(new Phone("dp1", "Design Phone 1", new byte[0], "1.2.3.4", 1, 2));
			Add(new Phone("dp2", "Design Phone 2", new byte[0], "2.3.4.5", 3, 4));
			Add(new Phone("dp3", "Design Phone 3", new byte[0], "6.7.8.9", 5, 6));
			Add(new Phone("dp4", "Design Phone 4", new byte[0], "10.11.12.13", 7, 8));
		}

		private void DeserializeDevices()
		{
			string serializedDevices = preferences.OptString(KEY_SERIALIZED_DEVICES, "[]");
			JsonArray devicesJsonArray = JsonUtils.VerifyJsonArray(serializedDevices);

			if (devicesJsonArray == JsonUtils.InvalidJsonArray)
			{
				Log.W(TAG, "devices json is corrupted");
				preferences[KEY_SERIALIZED_DEVICES] = "[]";
			}
			else
			{
				foreach (JsonObject deviceJson in devicesJsonArray)
				{
					Device device = Device.CreateFromJSONObject(deviceJson);

					//TODO: add invalid device check after switching to null safe variables
					if (device == null)
					{
						Log.W(TAG, $"device {deviceJson} is corrupted");
						continue;
					}

					Add(device);
				}
			}
		}

		private void InitializeDevice(Device device)
		{
			if (device.Certificate.Length > 0)
			{
				device.Connection = new NetworkDeviceConnection(device, udpServer);
				device.Connection.Start();

				device.FeatureManager = new FeatureManager(device);
				device.FeatureManager.StartEnabledFeatures();
			}
			else
			{
				Log.D(TAG, $"device {device.Id} is missing a certifcate, skipping initialization");
			}
		}

		public bool CheckPortAvailability(int port)
		{
			lock (Devices)
			{
				foreach (var device in Devices)
				{
					if (device.TcpPort == port || device.ListeningTCPPort == port)
						return false;
				}

				return true;
			}
		}

		public int GetFreePort()
		{
			lock (Devices)
			{
				int port;
				do
				{
					port = random.Next(5555, 9999);
				} while (!CheckPortAvailability(port));

				return port;
			}
		}

		//TODO: consider locking the method
		public bool IsPaired(Device device)
		{
			return Devices.Contains(device);
		}

		public bool IsDeviceWithIdPaired(string deviceId)
		{
			lock (Devices)
			{
				return Devices.Any(device => device.Id == deviceId);
			}
		}

		public void Add(Device device)
		{
			lock (Devices)
			{
				//TODO: replace with invalid device check
				if (device == null)
					return;

				if (IsPaired(device))
					return;

				Devices.Add(device);
				InitializeDevice(device);
			}
		}

		public void Remove(Device device)
		{
			lock (Devices)
			{
				if (!IsPaired(device))
					return;

				//TODO: consider deinitialization in this place
				Devices.Remove(device);
			}
		}

		public Device Get(string deviceId)
		{
			//TODO("currently there may be situation where two devices share the same id - add display name
			// and assign id based on the model number and uid provided by api29")
			return Devices.First(device => device.Id == deviceId);
		}

		public void Cleanup()
		{
			lock (Devices)
			{
				JsonArray serializedDevices = new JsonArray();
				foreach (var device in Devices)
				{
					if (device.Certificate.Length <= 0)
						continue;

					device.Connection.Interrupt();
					device.Connection.Join();

					device.FeatureManager.Cleanup();

					serializedDevices.Add(device.ToJsonObject());
				}

				preferences[KEY_SERIALIZED_DEVICES] = serializedDevices.ToString();
			}
		}

		#region IDisposable Support

		private bool disposed;
		public void Dispose()
		{
			if (disposed)
				return;

			if (DeviceDiscovery != null)
				DeviceDiscovery.Dispose();

			Cleanup();

			disposed = true;
		}

		~DeviceManager()
		{
			Dispose();
		}
		#endregion
	}
}
