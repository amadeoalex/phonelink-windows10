﻿using DeviceNS;
using Org.BouncyCastle.Crypto.Generators;
using PhoneLinkBase.DeviceNS.Connection.Pairing;
using PhoneLinkBase.Service;
using PhoneLinkBase.Service.Network;
using PhoneLinkBase.UtilClasses;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Documents;

namespace PhoneLinkBase.DeviceNS
{
	public class DeviceDiscovery : IUDPNetPackageHandler, IDisposable
	{
		private const string TAG = "Device Discovery";

		public const string PACKAGE_TYPE_LOOKUP = "lookup";
		public const string PACKAGE_TYPE_LOOKUP_RESPONSE = "lookupResponse";

		private readonly UDPServer udpServer;

		private readonly object foundDevicesLockObject = new object();
		public ObservableCollection<Device> FoundDevices { get; } = new ObservableCollection<Device>();
		//public CancellationTokenSource scanCancelationTokenSource = new CancellationTokenSource();
		private readonly System.Timers.Timer stopScanTimer = new System.Timers.Timer();
		public bool Scanning { get; private set; }

		public List<string> PackageTypes { get; } = new List<string>
		{
			PACKAGE_TYPE_LOOKUP,
			PACKAGE_TYPE_LOOKUP_RESPONSE
		};

		public DeviceDiscovery(UDPServer udpServer)
		{
			BindingOperations.EnableCollectionSynchronization(FoundDevices, foundDevicesLockObject);

			this.udpServer = udpServer;
			udpServer.AddPackageHandler(this);

			stopScanTimer.AutoReset = false;
			stopScanTimer.Elapsed += (sender, e) =>
			{
				Scanning = false;
				Log.D(TAG, "stopping scan");
			};
		}

		public bool HandleUDPNetPackage(NetPackage netPackage, IPEndPoint endPoint)
		{
			switch (netPackage.Type)
			{
				case PACKAGE_TYPE_LOOKUP:
					{
						Log.D(TAG, $"lookup package received from {endPoint.Address}");
						udpServer.SendNetPackage(DeviceUtils.GetDeviceLookupResponsePackage(), endPoint);
						return true;
					}

				case PACKAGE_TYPE_LOOKUP_RESPONSE:
					{
						Log.D(TAG, $"lookup response package received from {endPoint.Address}");
						if (Scanning)
						{
							Device device = Device.CreateFromIdNetPackage(netPackage);
							if (device != null)
							{
								if (!FoundDevices.Contains(device))
								{
									device.Address = endPoint.Address.ToString();
									FoundDevices.Add(device);
								}
							}
							else
								Log.D(TAG, $"error creating device from package {netPackage}");
						}

						return true;
					}

				default:
					return false;
			}
		}

		private readonly object scanLockObject = new object();
		public void StartScan(int milliseconds = 5 * 1000)
		{
			lock (scanLockObject)
			{
				stopScanTimer.Stop();
				stopScanTimer.Interval = milliseconds;

				FoundDevices.Clear();

				stopScanTimer.Start();
				Scanning = true;
				udpServer.SendNetPackage(DeviceUtils.GetDeviceLookupPackage(), new IPEndPoint(IPAddress.Broadcast, UDPServer.LISTEN_PORT));
			}
		}

		public void StopScan()
		{
			lock (scanLockObject)
			{
				Scanning = false;
				stopScanTimer.Stop();
			}
		}

		#region IDisposable Support

		private bool disposed;
		public void Dispose()
		{
			if (disposed)
				return;

			//if (scanCancelationTokenSource != null)
			//	scanCancelationTokenSource.Dispose();

			disposed = true;
		}

		~DeviceDiscovery()
		{
			Dispose();
		}
		#endregion
	}
}
