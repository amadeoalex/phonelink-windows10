﻿using DeviceNS.Types;
using PhoneLinkBase;
using PhoneLinkBase.DeviceNS.Connection;
using PhoneLinkBase.Feature;
using PhoneLinkBase.Service.Network;
using System;
using System.ComponentModel;
using System.Json;
using System.Linq;

namespace DeviceNS
{
	public abstract class Device : INotifyPropertyChanged
	{
		#region Property changed
#pragma warning disable 67
		public event PropertyChangedEventHandler PropertyChanged;
#pragma warning restore 67
		#endregion

		public const string KEY_ID = "id";
		public const string KEY_NAME = "name";
		public const string KEY_TYPE = "type";
		public const string KEY_CERTIFICATE = "certificate";
		public const string KEY_ADDRESS = "inetAddress";
		public const string KEY_TCP_PORT = "tcpPort";
		public const string KEY_LISTENING_TCP_PORT = "listeningTCPPort";
		public const string KEY_BSSID = "bSSID";
		public const string KEY_SSID = "SSID";

		public const string TYPE_INVALID = "invalid";
		public const string TYPE_PHONE = "phone";
		public const string TYPE_TABLET = "tablet";
		public const string TYPE_PC = "computer";
		public const string TYPE_LAPTOP = "laptop";


		#region Properties
		/// <summary>
		/// Returns device id.
		/// </summary>
		public abstract string Id { get; protected set; }
		/// <summary>
		/// Returns device name.
		/// </summary>
		public abstract string Name { get; protected set; }
		/// <summary>
		/// Returns device type.
		/// </summary>
		public abstract string Type { get; protected set; }
		/// <summary>
		/// Returns device certificate.
		/// </summary>
		public abstract byte[] Certificate { get; set; }
		/// <summary>
		/// Returns device IP address.
		/// </summary>
		public abstract string Address { get; set; }
		/// <summary>
		/// Returns device TCP port to which it tries to connect.
		/// </summary>
		public abstract int TcpPort { get; set; }
		/// <summary>
		/// Returns device TCP port at which it listens for connections.
		/// </summary>
		public abstract int ListeningTCPPort { get; set; }
		/// <summary>
		/// Return the SSID of the network the device is linked to
		/// </summary>
		public abstract string SSID { get; set; }
		/// <summary>
		/// Return the BSSID of the network the device is linked to
		/// </summary>
		public abstract string BSSID { get; set; }
		/// <summary>
		/// Returns device connection server
		/// </summary>
		public abstract DeviceConnection Connection { get; set; }
		/// <summary>
		/// Returns device feature manager
		/// </summary>
		public abstract FeatureManager FeatureManager { get; set; }
		#endregion

		#region Static Methods

		/// <summary>
		/// Returns new specific device based on <paramref name="deviceJson"/>.
		/// Null is returned if no device can be matched to type contained within <paramref name="deviceJson"/>.
		/// </summary>
		/// <param name="deviceJson"></param>
		/// <returns></returns>
		public static Device CreateFromJSONObject(JsonObject jsonObject)
		{
			string deviceType = jsonObject.OptString(KEY_TYPE, TYPE_INVALID);

			Device device = null;
			switch (deviceType)
			{
				case TYPE_PHONE:
					device = new Phone();
					break;

				case TYPE_PC:
				case TYPE_LAPTOP:
					device = new Computer();
					break;
			}

			device?.InitializeFromJsonObject(jsonObject);
			return device;
		}

		/// <summary>
		/// Returns new specific device based on <paramref name="idNetPackage"/>.
		/// Null is returned if no device can be matched to information contained within <paramref name="idNetPackage"/>.
		/// <paramref name="idNetPackage"/> is required to have only <see cref="KEY_ID"/> and <see cref="KEY_ADDRESS"/> string entries.
		/// </summary>
		/// <param name="idNetPackage"></param>
		/// <returns></returns>
		public static Device CreateFromIdNetPackage(NetPackage idNetPackage) => CreateFromJSONObject(idNetPackage);

		#endregion

		#region Methods

		/// <summary>
		/// Initialized the device with values contained withinh <paramref name="jsonObject"/>.
		/// Leaves default value if <paramref name="jsonObject"/> is missing it. 
		/// </summary>
		/// <param name="jsonObject"></param>
		public void InitializeFromJsonObject(JsonObject jsonObject)
		{
			Id = jsonObject.OptString(KEY_ID, Id);
			Name = jsonObject.OptString(KEY_NAME, Name);
			if (jsonObject.ContainsKey(KEY_CERTIFICATE))
				Certificate = Convert.FromBase64String(jsonObject[KEY_CERTIFICATE]);
			else
				Certificate = Certificate;

			Address = jsonObject.OptString(KEY_ADDRESS, Address);
			TcpPort = jsonObject.OptInt(KEY_TCP_PORT, TcpPort);
			ListeningTCPPort = jsonObject.OptInt(KEY_LISTENING_TCP_PORT, ListeningTCPPort);
			SSID = jsonObject.OptString(KEY_SSID, SSID);
			BSSID = jsonObject.OptString(KEY_BSSID, BSSID);
		}

		/// <summary>
		/// Returns json representation of the device
		/// </summary>
		/// <returns></returns>
		public JsonObject ToJsonObject()
		{
			return new JsonObject()
			{
				[KEY_ID] = Id,
				[KEY_NAME] = Name,
				[KEY_TYPE] = Type,
				[KEY_CERTIFICATE] = Convert.ToBase64String(Certificate),
				[KEY_ADDRESS] = Address,
				[KEY_TCP_PORT] = TcpPort,
				[KEY_LISTENING_TCP_PORT] = ListeningTCPPort,
				[KEY_BSSID] = BSSID,
				[KEY_SSID] = SSID
			};
		}

		/// <summary>
		/// Returns device data in JsonObject format
		/// </summary>
		public override string ToString()
		{
			return ToJsonObject().ToString();
		}

		/// <summary>
		/// Returns hash code of the device
		/// </summary>
		public override int GetHashCode()
		{
			var result = Id.GetHashCode();
			result = (31 * result) + Certificate.GetHashCode();
			result = (31 * result) + Type.GetHashCode();

			return result;
		}

		public override bool Equals(object other)
		{
			if (other == null || !(other is Device))
				return false;

			if (ReferenceEquals(other, this))
				return true;

			Device device = (Device)other;

			return device.Id == Id &&
					device.Certificate.SequenceEqual(Certificate) &&
					device.Type == Type;
		}

		#endregion

		//#region Pairing
		//public async Task AsyncPair(bool client)
		//{
		//	await Task.Run(() => Pair(client)).ConfigureAwait(false);
		//}

		//public void Pair(bool client)
		//{
		//	if (Pairing)
		//		return;

		//	Pairing = true;

		//	if (PairingProcess != null && PairingProcess.Status != PairingStatus.PairingIdle)
		//	{
		//		PairingProcess.Dispose();
		//		PairingProcess = null;
		//	}

		//	using (PairingProcess = CreatePairingProcess(client))
		//	{
		//		PairingProcess?.Begin();
		//	}

		//	Pairing = false;
		//}
		//#endregion
	}
}
