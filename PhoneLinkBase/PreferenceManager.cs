﻿using System;
using System.IO;
using System.Json;

namespace PhoneLinkBase
{
	public class PreferenceManager
	{
		private const string TAG = "Preference Manager";

		#region Instance
		private static readonly Lazy<PreferenceManager> lazyInstance = new Lazy<PreferenceManager>(() => new PreferenceManager(), true);

		/// <summary>
		/// Returns PreferenceManager instance
		/// </summary>
		public static PreferenceManager Instance { get { return lazyInstance.Value; } }
		#endregion

		private readonly object lockObject = new object();
		private readonly JsonObject preferences;

		private PreferenceManager()
		{
			try
			{
				if (File.Exists("preferences.json"))
				{
					string jsonString = File.ReadAllText("preferences.json");
					preferences = (JsonObject)JsonValue.Parse(jsonString);
				}
				else
				{
					preferences = new JsonObject();
				}
			}
			catch (Exception e)
			{
				Log.W(TAG, $"exception parsing preferences file: {e.Message}");
				preferences = new JsonObject();
			}
		}

		public JsonObject GetObjectPreferenceInternal(string key)
		{
			lock (lockObject)
			{
				if (preferences.ContainsKey(key) && preferences[key] is JsonObject)
					return (JsonObject)preferences[key];
				else
				{
					preferences[key] = new JsonObject();
					return (JsonObject)preferences[key];
				}
			}
		}

		public JsonArray GetArrayPreferenceInternal(string key)
		{
			lock (lockObject)
			{
				if (preferences.ContainsKey(key) && preferences[key] is JsonArray)
					return (JsonArray)preferences[key];
				else
				{
					preferences[key] = new JsonArray();
					return (JsonArray)preferences[key];
				}
			}
		}

		public void Cleanup()
		{
			File.WriteAllText("preferences.json", preferences.ToString());
			Log.I(TAG, "cleaned up");
		}

		public static JsonObject GetObjectPreference(string key)
		{
			return Instance.GetObjectPreferenceInternal(key);
		}

		public static JsonArray GetArrayPreference(string key)
		{
			return Instance.GetArrayPreferenceInternal(key);
		}
	}
}
