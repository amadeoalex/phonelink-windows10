﻿using Org.BouncyCastle.Asn1.X509;
using Org.BouncyCastle.Crypto.Generators;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PhoneLinkBase.UtilClasses
{
	public class SimpleThread
	{
		/// <summary>
		/// Underlying system thread executing <see cref="Run"/> when <see cref="Start"/> is called.
		/// </summary>
		public Thread SystemThread { get; }
		/// <summary>
		/// Returns true if <see cref="Run"/> is executing and <see cref="SystemThread"/> is alive.
		/// </summary>
		public bool IsAlive { get => RunExecuting && SystemThread.IsAlive; }
		/// <summary>
		/// Returns true is Run method finished it's execution.
		/// </summary>
		public bool Finished { get; private set; }

		private bool RunExecuting { get; set; }

		protected SimpleThread(string threadName = "")
		{
			SystemThread = new Thread(new ThreadStart(RunThread))
			{
				Name = threadName
			};
		}

		private void RunThread()
		{
			Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("en-GB");
			RunExecuting = true;
			Run();
			RunExecuting = false;
			Finished = true;
		}

		/// <summary>
		/// Starts the underlying <see cref="SystemThread"/> which executes <see cref="Run"/>
		/// </summary>
		public void Start()
		{
			SystemThread.Start();
		}

		/// <summary>
		/// Method run by the background thread.
		/// </summary>
		public virtual void Run() { }

		public void Join() => SystemThread.Join();

		/// <summary>
		/// Interrupts the thread.
		/// </summary>
		public virtual void Interrupt()
		{
			SystemThread.Interrupt();
		}
	}
}
