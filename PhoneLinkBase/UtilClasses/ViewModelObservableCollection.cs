﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Windows.Data;

namespace PhoneLinkBase.UtilClasses
{
	/// <summary>
	/// <para>Class used as a wrapper around another <see cref="ObservableCollection{T}"/>.</para>
	/// <para>Every time a new <typeparamref name="Model"/> item is added or removed from <paramref name="collection"/> it is wrapped around <typeparamref name="ViewModel"/> and added.</para>
	/// </summary>
	/// <typeparam name="ViewModel"></typeparam>
	/// <typeparam name="Model"></typeparam>
	public class ViewModelObservableCollection<ViewModel, Model> : ObservableCollection<ViewModel>
		where ViewModel : IViewModelBase<Model>
	{
		private readonly Func<Model, ViewModel> populateViewModelFunc;
		private readonly object lockObject = new object();

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="collection">Collection of models</param>
		/// <param name="populateViewModelFunc">Function called when a new item was added to <paramref name="collection"/></param>
		public ViewModelObservableCollection(ObservableCollection<Model> collection, Func<Model, ViewModel> populateViewModelFunc)
		{
			this.populateViewModelFunc = populateViewModelFunc;

			BindingOperations.EnableCollectionSynchronization(this, lockObject);

			foreach (Model item in collection)
			{
				ViewModel viewModel = populateViewModelFunc(item);
				Add(viewModel);
			}

			collection.CollectionChanged += UnderlyingCollectionChanged;
		}

		private void UnderlyingCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
			switch (e.Action)
			{
				case NotifyCollectionChangedAction.Add:
					{
						foreach (Model item in e.NewItems)
						{
							ViewModel viewModel = populateViewModelFunc(item);
							Add(viewModel);
						}
						break;
					}
				case NotifyCollectionChangedAction.Remove:
					{
						foreach (Model item in e.OldItems)
						{
							ViewModel vmToRemove = this.FirstOrDefault(element => ReferenceEquals(element.Model, item));
							Remove(vmToRemove);
						}
						break;
					}
			}
		}
	}

	public interface IViewModelBase<T>
	{
		T Model { get; set; }
	}
}