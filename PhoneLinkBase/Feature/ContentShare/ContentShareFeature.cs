﻿/*using Microsoft.QueryStringDotNET;
using PhoneLinkBase.Service.Network;
using System;
using System.Collections.Generic;

namespace PhoneLinkBase.Feature.ContentShare
{
	public class ContentShareFeature : FeatureBase
	{
		private const string TAG = "Content Share Feature";

		#region Static fields
		public static string PREFIX = Resources.Strings.ContentSharePrefix;
		public static string NAME = Resources.Strings.ContentShareName;
		#endregion

		#region Constants
		public const string TYPE_URL = "contentshare_url";
		public const string TYPE_IMG = "contentshare_image";
		public const string TYPE_FILE = "contentshare_file";

		public const string KEY_PENDING_DATA = "pendingData";

		public const string KEY_TYPE = "type";

		public const string KEY_TYPE_URL_URL = "url";
		public const string KEY_TYPE_URL_DATE = "date";

		//public const string KEY_TYPE_IMG_BASE64 = "base64";
		//public const string KEY_TYPE_IMG_MODE = "mode";
		//public const string KEY_TYPE_IMG_MODE_SHARE = "share";
		//public const string KEY_TYPE_IMG_MODE_SHARE_AND_CB = "shareandcb";
		//public const string KEY_TYPE_IMG_MODE_CLIPBOARD = "clipboard";
		//public const string KEY_TYPE_IMG_DATE = KEY_TYPE_URL_DATE;

		public const string KEY_TYPE_FILE_BASE64 = "payload";
		public const string KEY_TYPE_FILE_NAME = "name";
		public const string KEY_TYPE_FILE_DATE = KEY_TYPE_URL_DATE;
		public const string KEY_TYPE_FILE_MODE = "mode";
		public const string KEY_TYPE_FILE_MODE_SHARE = "share";
		public const string KEY_TYPE_FILE_MODE_SHARE_AND_CB = "shareandcb";
		public const string KEY_TYPE_FILE_MODE_CLIPBOARD = "clipboard";
		#endregion

		public event EventHandler<ContentShareReceivedEventArgs> ContentReceived;

		public override string Prefix { get => PREFIX; }
		public override string Name { get => NAME; }

		public override void OnConnected()
		{

		}

		public override void OnCreate()
		{

		}

		public override void OnDestroy()
		{

		}

		public override void OnNetPackageReceived(NetPackage netPackage)
		{
			long now = Utils.UnixMiliseconds();
			long delta = now - long.Parse(netPackage[KEY_TYPE_FILE_DATE]);
			Log.D(TAG, $"delta {delta}");

			ContentReceived?.Invoke(this, new ContentShareReceivedEventArgs
			{
				NetPackage = netPackage
			});
		}

		public override void OnNetPackageFailed(NetPackage netPackage)
		{

		}

		public override void OnStart()
		{

		}

		public override void OnStop()
		{

		}
	}
}
*/