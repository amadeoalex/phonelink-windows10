﻿using PhoneLinkBase.Service.Network;
using System;

namespace PhoneLinkBase.Feature.ContentShare
{
	public class ContentShareReceivedEventArgs : EventArgs
	{
		public NetPackage NetPackage { get; set; }
	}
}
