﻿using DeviceNS;
using PhoneLinkBase.Service.Network;
using System;
using System.Json;

namespace PhoneLinkBase.Feature
{
	public abstract class FeatureBase
	{
		public abstract string Tag { get; }
		public abstract string Key { get; }
		public abstract string NetPackagePrefix { get; }

		public abstract Device Device { get; }

		private bool isEnabled = false;
		public bool Enabled
		{
			get { return isEnabled; }
			set
			{
				if ((value && isEnabled) || (!value && !isEnabled))
					return;

				isEnabled = value;
				if (isEnabled) OnEnabled();
				else if (!isEnabled) OnDisabled();
			}
		}

		public abstract void OnEnabled();
		public abstract void OnDisabled();

		public abstract void OnConnected();
		public abstract void OnNetPackageReceived(NetPackage netPackage);
		public abstract void OnNetPackageFailed(NetPackage netPackage);
		public abstract void OnDisconnected();

		private const string KEY_FEATURE_PREF = "featurePreferences";
		private JsonObject preferences;
		[Obsolete]
		public JsonObject Preferences
		{
			get
			{
				if (preferences == null)
				{
					JsonObject featurePreferences = PreferenceManager.GetObjectPreference(KEY_FEATURE_PREF);
					preferences = featurePreferences.OptJsonObject(Key);
				}

				return preferences;
			}
		}
	}
}


