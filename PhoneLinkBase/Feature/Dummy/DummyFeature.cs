﻿using DeviceNS;
using PhoneLinkBase.Service.Network;

namespace PhoneLinkBase.Feature.Dummy
{
	public class DummyFeature : FeatureBase
	{
		private const string TAG = "Dummy Feature";

		public const string PREFIX = "df_";
		public const string PACKAGE_TYPE_PING = PREFIX + "ping";
		public const string PACKAGE_TYPE_PONG = PREFIX + "pong";

		public override string Tag => TAG;
		public override string Key => "dummy_feature";
		public override string NetPackagePrefix => PREFIX;

		public override Device Device { get; }

		public DummyFeature(Device device)
		{
			Device = device;
		}

		public override void OnEnabled()
		{
			Log.I(TAG, "onenabled");
		}

		public override void OnDisabled()
		{
			Log.I(TAG, "ondisabled");
		}

		public override void OnConnected()
		{
			Log.I(TAG, "onconnected");
		}

		public override void OnDisconnected()
		{
			Log.I(TAG, "ondisconnected");
		}

		public override void OnNetPackageReceived(NetPackage netPackage)
		{
			switch (netPackage.Type)
			{
				case PACKAGE_TYPE_PING:
					Device.Connection.SendNetPackage(new NetPackage(PACKAGE_TYPE_PONG));
					break;
			}
		}

		public override void OnNetPackageFailed(NetPackage netPackage)
		{
			Log.I(TAG, "onnetpackagefailed");
		}

		public void SendPing()
		{
			Device.Connection.SendNetPackage(new NetPackage(PACKAGE_TYPE_PING));
		}

	}
}
