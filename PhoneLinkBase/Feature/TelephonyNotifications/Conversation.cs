﻿/*using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Json;

namespace PhoneLinkBase.Feature.TelephonyNotifications
{
	public class Conversation : INotifyPropertyChanged
	{
		#region Property changed
#pragma warning disable 67
		public event PropertyChangedEventHandler PropertyChanged;
#pragma warning restore 67
		#endregion

		private const string TAG = "Conversation";

		public const string KEY_PREFERENCES = "preferences";

		public string Number { get; set; }
		public string Name { get; set; }
		public ObservableCollection<Message> Messages { get; set; }
		public JsonObject Preferences { get; set; }

		private Conversation() { }

		/// <summary>
		/// Creates empty conversation with supplied number
		/// </summary>
		/// <param name="number"></param>
		public Conversation(string number)
		{
			Number = number;
			Name = number;
			Messages = new ObservableCollection<Message>();
			Preferences = new JsonObject();
		}

		/// <summary>
		/// Adds message to conversation
		/// </summary>
		/// <param name="message"></param>
		public void AddMessage(Message message)
		{
			Messages.Add(message);
		}

		/// <summary>
		/// Adds multiple messages to conversation
		/// </summary>
		/// <param name="messages"></param>
		public void AddMessages(JsonArray messages)
		{
			if (messages.Count == 0)
			{
				Log.I(TAG, "empty message collection");
				return;
			}


			Message lastMessage = GetLastMessage();
			Message newestMessage = new Message((JsonObject)messages[messages.Count - 1]);

			if (lastMessage.Equals(newestMessage))
				RemoveLastMessage();

			foreach (Message m in messages)
			{
				Messages.Add(m);
			}
		}

		/// <summary>
		/// Returns last message
		/// </summary>
		/// <returns></returns>
		public Message GetLastMessage()
		{
			return Messages.Count == 0 ? new Message("", "", "") : Messages[Messages.Count - 1];
		}

		/// <summary>
		/// Removes last message
		/// </summary>
		public void RemoveLastMessage()
		{
			//TODO: check if it works
			Messages.Remove(GetLastMessage());
		}

		#region Save and load
		/// <summary>
		/// Parses supplied <paramref name="jsonString"/> and returns Conversation
		/// </summary>
		/// <param name="jsonString"></param>
		/// <returns></returns>
		public static Conversation Unserialize(string jsonString)
		{
			JsonObject conversationJson = (JsonObject)JsonValue.Parse(jsonString);
			JsonArray messagesJsonArray = (JsonArray)conversationJson[TelephonyNotificationsFeature.KEY_MESSAGES];
			ObservableCollection<Message> messages = new ObservableCollection<Message>();
			for (int i = 0; i < messagesJsonArray.Count; i++)
			{
				messages.Add(new Message((JsonObject)messagesJsonArray[i]));
			}

			Conversation conversation = new Conversation
			{
				Number = conversationJson[TelephonyNotificationsFeature.KEY_NUMBER],
				Name = conversationJson[TelephonyNotificationsFeature.KEY_NAME],
				Messages = messages,
				Preferences = (JsonObject)JsonValue.Parse(conversationJson[KEY_PREFERENCES])
			};

			Log.I(TAG, "unserialized");

			return conversation;
		}

		/// <summary>
		/// Returns Json representation of Conversation
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			JsonObject obj = new JsonObject
			{
				[TelephonyNotificationsFeature.KEY_NUMBER] = Number,
				[TelephonyNotificationsFeature.KEY_MESSAGES] = new JsonArray(Messages),
				[TelephonyNotificationsFeature.KEY_NAME] = Name,
				[KEY_PREFERENCES] = Preferences.ToString()
			};

			return obj.ToString();
		}
		#endregion
	}
}
*/