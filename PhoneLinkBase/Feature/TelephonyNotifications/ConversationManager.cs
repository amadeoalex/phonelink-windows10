﻿/*using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;

namespace PhoneLinkBase.Feature.TelephonyNotifications
{
	public sealed class ConversationManager
	{
		private const string TAG = "Conversation Manager";

		private const string CONVERSATIONS_PATH = "conversations";
		private const string CONVERSATION_FILE_EXTENSION = ".con";

		#region Instance
		private static readonly Lazy<ConversationManager> lazyInstance = new Lazy<ConversationManager>(() => new ConversationManager(), true);

		public static ConversationManager Instance { get { return lazyInstance.Value; } }
		#endregion

		private readonly ConcurrentDictionary<string, Conversation> conversations;

		/// <summary>
		/// Currently known contacts
		/// </summary>
		public ICollection<string> Contacts { get => conversations.Keys; }

		private ConversationManager()
		{
			Log.I(TAG, "constructor");

			conversations = new ConcurrentDictionary<string, Conversation>();

			//create conversations directory
			Directory.CreateDirectory(CONVERSATIONS_PATH);

			PreloadContacts();
		}

		/// <summary>
		/// Preloads conversations without loading their contents.
		/// </summary>
		private void PreloadContacts()
		{
			string[] conversationFiles = Directory.GetFiles(CONVERSATIONS_PATH, $"*{CONVERSATION_FILE_EXTENSION}");
			foreach (string fileName in conversationFiles)
				conversations[Path.GetFileNameWithoutExtension(fileName)] = null;
		}

		/// <summary>
		/// Returns conversation with given <paramref name="number"/>.
		/// If no such conversation exists, returns empty one.
		/// </summary>
		/// <param name="number"></param>
		/// <returns></returns>
		public Conversation GetConversation(string number)
		{
			//Check if conversation is already loaded
			if (!conversations.ContainsKey(number) || conversations[number] == null)
			{
				string file = $"{CONVERSATIONS_PATH}/{number}{CONVERSATION_FILE_EXTENSION}";
				//Check if there is saved conversation on disk
				if (File.Exists(file))
				{
					string jsonString = File.ReadAllText(file);
					conversations[number] = Conversation.Unserialize(jsonString);
				}
				else
				{
					Log.I(TAG, $"no saved conversation with number {number}");
					conversations[number] = new Conversation(number);
				}
			}

			return conversations[number];
		}

		/// <summary>
		/// Saves all conversations
		/// </summary>
		public void Cleanup()
		{
			if (conversations.Count == 0)
				return;

			///check if conversation directory exists and create one if not
			Directory.CreateDirectory(CONVERSATIONS_PATH);

			foreach (KeyValuePair<string, Conversation> c in conversations)
			{
				//save each conversation to file
				if (c.Value != null)
					File.WriteAllText($"{CONVERSATIONS_PATH}/{c.Key}{CONVERSATION_FILE_EXTENSION}", c.Value.ToString());
			}

			Log.I(TAG, "cleaned up");
		}
	}
}
*/