﻿/*using PhoneLinkBase.Service.Network;
using System;
using System.Json;

namespace PhoneLinkBase.Feature.TelephonyNotifications
{
	public class Message : JsonObject, IEquatable<Message>
	{
		public string Number
		{
			get
			{
				return ContainsKey(TelephonyNotificationsFeature.KEY_NUMBER) ? this[TelephonyNotificationsFeature.KEY_NUMBER] : null;
			}
			set => this[TelephonyNotificationsFeature.KEY_NUMBER] = value;
		}
		public string Body { get => this[TelephonyNotificationsFeature.KEY_BODY]; }
		public string Date { get => this[TelephonyNotificationsFeature.KEY_DATE]; }

		/// <summary>
		/// Creates message with given parameters
		/// </summary>
		/// <param name="number"></param>
		/// <param name="body"></param>
		/// <param name="date"></param>
		public Message(string number, string body, string date)
		{
			this[TelephonyNotificationsFeature.KEY_NUMBER] = number;
			this[TelephonyNotificationsFeature.KEY_BODY] = body;
			this[TelephonyNotificationsFeature.KEY_DATE] = date;
		}

		/// <summary>
		/// Creates message based on given <paramref name="jsonObject"/>
		/// Provided <paramref name="jsonObject"/> should contain three strings values:
		/// "number"
		/// "body"
		/// "date"
		/// </summary>
		/// <param name="jsonObject"></param>
		public Message(JsonObject jsonObject) : base(jsonObject)
		{

		}

		/// <summary>
		/// Returns true if <paramref name="other"/> message's <see cref="Date"/> is exactly or less <paramref name="milisecons"/> apart from callers <see cref="Date"/>
		/// <para><see cref="Body"/> and <see cref="Number"/> must be the same.</para>
		/// </summary>
		/// <param name="other"></param>
		/// <returns></returns>
		public bool IsDateWithinRange(Message other, int miliseconds = 2000)
		{
			if (Body != other.Body || Number != other.Number)
				return false;

			long timestamp = long.Parse(Date);
			long otherTimestamp = long.Parse(other.Date);

			Log.I("Message", $"{Math.Abs(otherTimestamp - timestamp)}");

			return Math.Abs(otherTimestamp - timestamp) <= miliseconds;
		}

		/// <summary>
		/// Returns Message based on provided <paramref name="netPackage"/>.
		/// </summary>
		/// <param name="netPackage"></param>
		/// <returns></returns>
		public static Message FromNetPackage(NetPackage netPackage)
		{
			return new Message(netPackage[TelephonyNotificationsFeature.KEY_NUMBER], netPackage[TelephonyNotificationsFeature.KEY_BODY], netPackage[TelephonyNotificationsFeature.KEY_DATE]);
		}


		#region Equals section
		/// <summary>
		/// Compares two Message objects
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public override bool Equals(object obj)
		{
			if (this == obj) return true;
			if (obj == null || GetType() != obj.GetType()) return false;
			Message message = (Message)obj;
			return Number == message.Number && Body == message.Body && Date == message.Date;
		}

		/// <summary>
		/// Returns hash code of the Message based on Number, Body and Date
		/// </summary>
		/// <returns></returns>
		public override int GetHashCode()
		{
			return new { Number, Body, Date }.GetHashCode();
		}

		public bool Equals(Message other)
		{
			if (ReferenceEquals(this, other))
				return true;

			return Number == other.Number && Body == other.Body && Date == other.Date;
		}
		#endregion
	}
}
*/