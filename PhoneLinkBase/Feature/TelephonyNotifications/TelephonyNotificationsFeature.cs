﻿/*using Microsoft.QueryStringDotNET;
using PhoneLinkBase.Device;
using PhoneLinkBase.Service.Network;
using System;
using System.Collections.Generic;
using System.Json;

namespace PhoneLinkBase.Feature.TelephonyNotifications
{
	public class TelephonyNotificationsFeature : FeatureBase
	{
		private const string TAG = "Telephony Notifications Feature";

		#region Static fields
		public static string PREFIX = Resources.Strings.TelephonyNotificationsPrefix;
		public static string NAME = Resources.Strings.TelephonyNotificationsName;
		#endregion

		#region Constants
		public const string SETTING_SYNC_ON_CONNECT = "syncOnConnect";
		public const string SETTING_SYNC_NAMES_ON_CONNECT = "syncNamesOnConnect";

		public const bool SETTING_SYNC_ON_CONNECT_DEFAULT = true;
		public const bool SETTING_SYNC_NAMES_ON_CONNECT_DEFAULT = true;

		public const string KEY_DATE = "date";
		public const string KEY_BODY = "body";
		public const string KEY_NUMBER = "number";
		public const string KEY_NAME = "name";
		public const string KEY_NUMBERS = "numbers";
		public const string KEY_LAST_MESSAGES = "last_messages";
		public const string KEY_MESSAGES = "messages";
		public const string KEY_NUMBER_NAMES = "number_names";
		public const string KEY_ACTION = "action";

		public const string TYPE_SMS = "telephony_sms";
		public const string TYPE_SMS_SEND = "telephony_send_sms";
		public const string TYPE_SMS_SENT = "telephony_sent_sms";
		public const string TYPE_SMS_READ = "telephony_read_sms";
		public const string TYPE_SMS_DENIED = "telephony_denied_sms";
		public const string TYPE_CONVERSATION_SYNC = "telephony_conversation_sync";
		public const string TYPE_CONVERSATION_SYNC_DATA = "telephony_conversation_sync_data";
		public const string TYPE_CONVERSATION_SYNC_ALL = "telephony_conversation_sync_all";
		public const string TYPE_CONVERSATION_NAME_FROM_NUMBER = "telephony_conversation_name_from_number";
		public const string TYPE_CONVERSATION_NAME_FROM_NUMBER_DATA = "telephony_conversation_name_from_number_data";
		public const string TYPE_UPDATE_TIMESTAMP = "telephony_update_time_stamp";
		#endregion

		public event EventHandler<NotificationReceivedEventArgs> NotificationReceived;

		public override string Prefix { get => PREFIX; }
		public override string Name { get => NAME; }

		public override void OnCreate()
		{
			Log.I(TAG, "created");
		}

		public override void OnDestroy()
		{
			ConversationManager.Instance.Cleanup();
			Log.I(TAG, "destroyed");
		}


		public override void OnStart()
		{
			Log.I(TAG, "started");
		}

		public override void OnStop()
		{
			Log.I(TAG, "stopped");
		}

		public override void OnConnected()
		{
			Log.I(TAG, "connected");

			if (ConversationManager.Instance.Contacts.Count == 0)
				return;

			if (Preferences.OptBool(SETTING_SYNC_NAMES_ON_CONNECT, SETTING_SYNC_NAMES_ON_CONNECT_DEFAULT))
			{
				//get contacts that may need name change
				JsonArray numbers = new JsonArray();
				foreach (string number in ConversationManager.Instance.Contacts)
				{
					Conversation conversation = ConversationManager.Instance.GetConversation(number);
					if (conversation.Name == conversation.Number)
						numbers.Add(number);
				}

				//send net package only if necessary
				if (numbers.Count > 0)
				{
					NetPackage netPackage = new NetPackage(TYPE_CONVERSATION_NAME_FROM_NUMBER)
					{
						[KEY_NUMBERS] = numbers
					};

					DeviceManagerHelper.SendNetPackageToAll(netPackage);
				}
			}

			if (Preferences.OptBool(SETTING_SYNC_ON_CONNECT, SETTING_SYNC_ON_CONNECT_DEFAULT))
			{
				JsonArray numbers = new JsonArray();
				JsonObject lastMessages = new JsonObject();

				foreach (string number in ConversationManager.Instance.Contacts)
				{
					Message lastMessage = ConversationManager.Instance.GetConversation(number).GetLastMessage();
					if (lastMessage != null && !string.IsNullOrEmpty(lastMessage.Date))
					{
						numbers.Add(number);
						lastMessages.Add(number, lastMessage);
					}
				}

				if (numbers.Count > 0)
				{
					NetPackage netPackage = new NetPackage(TYPE_CONVERSATION_SYNC)
					{
						[KEY_NUMBERS] = numbers,
						[KEY_LAST_MESSAGES] = lastMessages
					};

					DeviceManagerHelper.SendNetPackageToAll(netPackage);
				}
			}
		}

		public override void OnNetPackageReceived(NetPackage netPackage)
		{
			NetPackage returnNetPakcage = null;

			string type = netPackage.Type;
			Log.W(TAG, type);
			switch (type)
			{
				//sms was received by remote device
				case TYPE_SMS:
					{
						Conversation conversation = ConversationManager.Instance.GetConversation(netPackage[KEY_NUMBER]);

						//check if conversation name needs to be updated
						string newName = netPackage[KEY_NAME];
						if (conversation.Name != newName)
						{
							Log.I(TAG, $"updating conversation name from {conversation.Name} to {newName}");
							conversation.Name = newName;
						}

						Message message = Message.FromNetPackage(netPackage);
						conversation.AddMessage(message);

						NotificationReceived?.Invoke(this, new NotificationReceivedEventArgs { Conversation = conversation, Message = message });

						break;
					}

				//send sms request was denied by remote device
				case TYPE_SMS_DENIED:
					{
						Conversation conversation = ConversationManager.Instance.GetConversation(netPackage[KEY_NUMBER]);
						Message messageToRemove = Message.FromNetPackage(netPackage);

						//TODO: abandon null=sent message convention, it only generates more and more problems, add IsSent property for class and field for net packages
						messageToRemove.Number = null;

						conversation.Messages.Remove(messageToRemove);

						break;
					}

				//sms was sent by remote device
				case TYPE_SMS_SENT:
					{
						Conversation conversation = ConversationManager.Instance.GetConversation(netPackage[KEY_NUMBER]);

						Message newMessage = Message.FromNetPackage(netPackage);
						newMessage.Number = null;
						Message lastMessage = conversation.GetLastMessage();
						if (!newMessage.IsDateWithinRange(lastMessage))
							conversation.AddMessage(newMessage);

						break;
					}

				//received requested conversation messages from remote device
				case TYPE_CONVERSATION_SYNC_DATA:
					{
						JsonObject messages = (JsonObject)netPackage[KEY_MESSAGES];

						foreach (string number in netPackage[KEY_NUMBERS])
						{
							JsonArray conversationMessages = (JsonArray)messages[number];
							ConversationManager.Instance.GetConversation(number).AddMessages(conversationMessages);
						}

						break;
					}

				//received requested known names associated with numbers from remote device
				case TYPE_CONVERSATION_NAME_FROM_NUMBER_DATA:
					{
						//TODO: refactor name to number system 
						JsonObject numbers = (JsonObject)netPackage[KEY_NUMBER_NAMES];
						foreach (KeyValuePair<string, JsonValue> entry in numbers)
							ConversationManager.Instance.GetConversation(entry.Key).Name = entry.Value;

						break;
					}


				//an unknown net package was received
				default:
					Log.I(TAG, $"unknown package '{type}' received");
					break;
			}

			if (returnNetPakcage != null)
			{
				DeviceManagerHelper.SendNetPackageToAll(returnNetPakcage);
			}
		}

		public override void OnNetPackageFailed(NetPackage netPackage)
		{
			Conversation conversation = ConversationManager.Instance.GetConversation(netPackage[KEY_NUMBER]);
			Message messageToRemove = Message.FromNetPackage(netPackage);

			//TODO: abandon null=sent message convention, it only generates more and more problems, add IsSent property for class and field for net packages
			messageToRemove.Number = null;

			conversation.Messages.Remove(messageToRemove);
		}

		public void SendSMSMessageRequest(string number, string message)
		{
			Log.I(TAG, "sending sms request");

			NetPackage netPackage = new NetPackage(TYPE_SMS_SEND)
			{
				[KEY_NUMBER] = number,
				[KEY_BODY] = message,
				[KEY_DATE] = Utils.RemoveHundreds(Utils.UnixMiliseconds()).ToString()
			};

			DeviceManagerHelper.SendNetPackageToAll(netPackage);
		}

		public void SendSMSReadPackage(string body)
		{
			NetPackage netPackage = new NetPackage(TYPE_SMS_READ)
			{
				[KEY_BODY] = body
			};

			DeviceManagerHelper.SendNetPackageToAll(netPackage);
		}
	}
}
*/