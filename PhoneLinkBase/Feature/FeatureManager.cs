﻿using DeviceNS;
using PhoneLinkBase.Feature.Dummy;
using PhoneLinkBase.Service.Network;
using System;
using System.Collections.Generic;
using System.Json;

namespace PhoneLinkBase.Feature
{
	public class FeatureManager
	{
		private const string TAG = "Feature Manager";
		private const string APPENDIX_ENABLED = "_enabled";


		private const string KEY_FEATURE_MANAGER_PREF = "features";

		private readonly JsonObject preferences;

		private readonly Dictionary<string, FeatureBase> features = new Dictionary<string, FeatureBase>();
		private readonly Dictionary<string, FeatureBase> enabledFeatures = new Dictionary<string, FeatureBase>();

		public FeatureManager(Device device)
		{
			preferences = PreferenceManager.GetObjectPreference(KEY_FEATURE_MANAGER_PREF);
			AddFeature(new DummyFeature(device));
		}

		private void AddFeature(FeatureBase feature)
		{
			features[feature.Key] = feature;
		}

		public FeatureBase Get(Type featureType)
		{
			foreach (KeyValuePair<string, FeatureBase> entry in features)
			{
				if(entry.Value.GetType() == featureType)
					return entry.Value;
			}

			return null;
		}

		public void StartEnabledFeatures()
		{
			lock (features)
			{
				foreach (KeyValuePair<string, FeatureBase> item in features)
				{
					//TODO: temporary fix to enable all featrures by default
					if (!preferences.ContainsKey(item.Key + APPENDIX_ENABLED) || (preferences.ContainsKey(item.Key + APPENDIX_ENABLED) && preferences[item.Key + APPENDIX_ENABLED]))
					{
						lock (enabledFeatures) { enabledFeatures[item.Key] = item.Value; }
						item.Value.Enabled = true;
					}
				}
			}
		}

		public void SetFeatureEnabled(string key, bool enabled)
		{
			if (enabled)
				EnableFeature(key);
			else
				DisableFeature(key);
		}

		private void EnableFeature(string key)
		{
			if (!features.ContainsKey(key) || enabledFeatures.ContainsKey(key))
				return;

			preferences[key + APPENDIX_ENABLED] = true;
			lock (enabledFeatures)
			{
				FeatureBase feature = features[key];
				enabledFeatures[key] = feature;
				feature.Enabled = true;
			}
		}

		private void DisableFeature(string key)
		{
			if (!features.ContainsKey(key) || !enabledFeatures.ContainsKey((key)))
				return;

			preferences[key + APPENDIX_ENABLED] = false;
			lock (enabledFeatures)
			{
				FeatureBase feature = features[key];
				feature.Enabled = false;
				enabledFeatures.Remove(key);
			}
		}

		public void OnConnected()
		{
			lock (enabledFeatures)
			{
				foreach (KeyValuePair<string, FeatureBase> entry in features)
				{
					entry.Value.OnConnected();
				}
			}
		}

		public void OnNetPackageReceived(NetPackage netPackage)
		{
			lock (enabledFeatures)
			{
				foreach (KeyValuePair<string, FeatureBase> entry in features)
				{
					if (netPackage.Type.StartsWith(entry.Value.Key))
						entry.Value.OnNetPackageReceived(netPackage);
				}
			}
		}

		public void OnNetPackageFailed(NetPackage netPackage)
		{
			lock (enabledFeatures)
			{
				foreach (KeyValuePair<string, FeatureBase> entry in features)
				{
					if (netPackage.Type.StartsWith(entry.Value.Key))
						entry.Value.OnNetPackageFailed(netPackage);
				}
			}
		}

		public void OnDisconnected()
		{
			lock (enabledFeatures)
			{
				foreach (KeyValuePair<string, FeatureBase> entry in features)
				{
					entry.Value.OnDisconnected();
				}
			}
		}

		public void Cleanup()
		{
			lock (enabledFeatures)
			{
				foreach (KeyValuePair<string, FeatureBase> entry in features)
				{
					entry.Value.OnDisabled();
				}
			}
		}

		/*		#region Feature management
				public void RegisterFeature(FeatureBase feature)
				{
					if (features.ContainsKey(feature.Name))
					{
						Log.I(TAG, $"feature {feature.Name} already registered");
						return;
					}

					features[feature.Name] = feature;
					feature.OnCreate();

					if (!disabledFeatures.ContainsKey(feature.Name))
						feature.OnStart();
					else
						Log.I(TAG, $"feature {feature.Name} registered but disabled");
				}

				public FeatureBase Get(string name)
				{
					return features[name];
				}

				public bool IsEnabled(string name)
				{
					return !disabledFeatures.ContainsKey(name);
				}

				public void Enable(string name)
				{
					if (IsEnabled(name))
					{
						Log.I(TAG, $"feature {name} already enabled");
						return;
					}

					disabledFeatures.Remove(name);

					FeatureBase feature = Get(name);
					feature.OnStart();
				}

				public void Disable(string name)
				{
					if (!IsEnabled(name))
					{
						Log.I(TAG, $"feature {name} is already disabled");
						return;
					}

					FeatureBase feature = Get(name);
					feature.OnStop();
					disabledFeatures.Add(name, "");
				}
				#endregion

				#region Feature functions
				/// <summary>
				/// Notifies all enabled features that connection with remote device has been established.
				/// </summary>
				public void Connected()
				{
					lock (lockObject)
					{
						foreach (KeyValuePair<string, FeatureBase> entry in features)
						{
							if (IsEnabled(entry.Key))
								entry.Value.OnConnected();
						}
					}
				}

				/// <summary>
				/// Notifies feature about received net package.
				/// </summary>
				/// <param name="netPackage"></param>
				public void ParseNetPackage(NetPackage netPackage)
				{
					lock (lockObject)
					{
						foreach (KeyValuePair<string, FeatureBase> entry in features)
						{
							if (IsEnabled(entry.Key) && netPackage.Type.StartsWith(entry.Value.Prefix))
								entry.Value.OnNetPackageReceived(netPackage);
						}
					}
				}

				/// <summary>
				/// Notifies feature about failed net package.
				/// </summary>
				/// <param name="netPackage"></param>
				public void FailedNetPackage(NetPackage netPackage)
				{
					lock (lockObject)
					{
						foreach (KeyValuePair<string, FeatureBase> entry in features)
						{
							if (IsEnabled(entry.Key) && netPackage.Type.StartsWith(entry.Value.Prefix))
								entry.Value.OnNetPackageFailed(netPackage);
						}
					}
				}

				/// <summary>
				/// Performs cleanup before program shutdown.
				/// </summary>
				public void Cleanup()
				{
					foreach (KeyValuePair<string, FeatureBase> entry in features)
					{
						if (IsEnabled(entry.Key))
						{
							entry.Value.OnStop();
							entry.Value.OnDestroy();
						}
					}
				}
				#endregion*/
	}
}
