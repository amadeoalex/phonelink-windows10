﻿using Org.BouncyCastle.Asn1;
using Org.BouncyCastle.Asn1.Pkcs;
using Org.BouncyCastle.Asn1.X509;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Crypto.Operators;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Crypto.Prng;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Pkcs;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.X509;
using System;
using System.Json;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Windows.Documents;

namespace PhoneLinkBase
{
	public static class SecurityUtils
	{
		private const string TAG = "Security Utils";

		public static readonly string KEY_SECURITY_PREF = "security";
		public static readonly string KEY_SECURITY_PUBLIC = "publicKey";
		public static readonly string KEY_SECURITY_PRIVATE = "privateKey";
		public static readonly string KEY_SECURITY_CERTIFICATE = "certificate";

		private static X509Certificate2 deviceCertificate = null;
		private static JsonObject securityPreferences = null;

		private static SecureRandom secureRandom = new SecureRandom();
		private static string characterPool = "abcdefghijklmnopqrstuvwxyz" +
												"0123456789" +
												"ABCDEFGHIJKLMNOPQRSTUVWXYZ";



		public static void Init()
		{
			if (securityPreferences != null)
				return;

			securityPreferences = PreferenceManager.GetObjectPreference(KEY_SECURITY_PREF);

			if (!securityPreferences.ContainsKey(KEY_SECURITY_PUBLIC) || !securityPreferences.ContainsKey(KEY_SECURITY_PRIVATE))
			{
				AsymmetricCipherKeyPair keyPair = GenerateKeyPair();

				byte[] publicEncodedKey = SubjectPublicKeyInfoFactory.CreateSubjectPublicKeyInfo(keyPair.Public).GetDerEncoded();
				byte[] privateEncodedKey = PrivateKeyInfoFactory.CreatePrivateKeyInfo(keyPair.Private).GetDerEncoded();

				securityPreferences[KEY_SECURITY_PUBLIC] = Convert.ToBase64String(publicEncodedKey);
				securityPreferences[KEY_SECURITY_PRIVATE] = Convert.ToBase64String(privateEncodedKey);
			}

			if (!securityPreferences.ContainsKey(KEY_SECURITY_CERTIFICATE))
			{
				AsymmetricKeyParameter publicKey = PublicKeyFactory.CreateKey(Convert.FromBase64String(securityPreferences[KEY_SECURITY_PUBLIC]));
				AsymmetricKeyParameter privateKey = PrivateKeyFactory.CreateKey(Convert.FromBase64String(securityPreferences[KEY_SECURITY_PRIVATE]));

				string subjectIssuerData = $"O=Amadeo,OU=Phone Link,CN={Environment.MachineName}";
				deviceCertificate = GenerateCertificate(subjectIssuerData, publicKey, privateKey);


				byte[] certificateBytes = deviceCertificate.Export(X509ContentType.Pkcs12);
				securityPreferences[KEY_SECURITY_CERTIFICATE] = Convert.ToBase64String(certificateBytes);
				Log.I(TAG, "generated new device certificate");
			}
			else
			{
				deviceCertificate = new X509Certificate2(Convert.FromBase64String(securityPreferences[KEY_SECURITY_CERTIFICATE]));
				Log.I(TAG, "loaded device certificate");
			}
		}

		public static X509Certificate2 GetCertificate()
		{
			Init();
			return deviceCertificate;
		}

		public static PublicKey GetPublicKey()
		{
			Init();
			return deviceCertificate.PublicKey;
		}

		public static AsymmetricAlgorithm GetPrivateKey()
		{
			Init();
			return deviceCertificate.PrivateKey;
		}

		private static AsymmetricCipherKeyPair GenerateKeyPair()
		{
			CryptoApiRandomGenerator cryptoApiRandomGenerator = new CryptoApiRandomGenerator();
			SecureRandom secureRandom = new SecureRandom(cryptoApiRandomGenerator);

			KeyGenerationParameters keyGenerationParameters = new KeyGenerationParameters(secureRandom, 2048);
			RsaKeyPairGenerator keyPairGenerator = new RsaKeyPairGenerator();
			keyPairGenerator.Init(keyGenerationParameters);
			AsymmetricCipherKeyPair keyPair = keyPairGenerator.GenerateKeyPair();

			return keyPair;
		}

		private static X509Certificate2 GenerateCertificate(String subjectIssuerData, AsymmetricKeyParameter publicKey, AsymmetricKeyParameter privateKey)
		{
			Init();

			X509V3CertificateGenerator certificateGenerator = new X509V3CertificateGenerator();
			certificateGenerator.SetPublicKey(publicKey);

			certificateGenerator.SetSerialNumber(BigInteger.One);
			certificateGenerator.SetIssuerDN(new X509Name(subjectIssuerData));
			certificateGenerator.SetSubjectDN(new X509Name(subjectIssuerData));

			DateTime from = DateTime.UtcNow.Date;
			DateTime to = from.AddYears(5);

			certificateGenerator.SetNotBefore(from);
			certificateGenerator.SetNotAfter(to);

			ISignatureFactory signatureFactory = new Asn1SignatureFactory("SHA256withRSA", privateKey);
			var certificate = certificateGenerator.Generate(signatureFactory);

			PrivateKeyInfo privateKeyInfo = PrivateKeyInfoFactory.CreatePrivateKeyInfo(privateKey);

			X509Certificate2 certificate2 = new X509Certificate2(certificate.GetEncoded());

			Asn1Sequence sequence = (Asn1Sequence)Asn1Object.FromByteArray(privateKeyInfo.ParsePrivateKey().GetEncoded());
			if (sequence.Count != 9)
				throw new PemException("malformed sequence in private RSA key");

			RsaPrivateKeyStructure privateKeyStructure = RsaPrivateKeyStructure.GetInstance(sequence);
			RsaPrivateCrtKeyParameters rsaParams = new RsaPrivateCrtKeyParameters(
				privateKeyStructure.Modulus, privateKeyStructure.PublicExponent,
				privateKeyStructure.PrivateExponent, privateKeyStructure.Prime1,
				privateKeyStructure.Prime2, privateKeyStructure.Exponent1,
				privateKeyStructure.Exponent2, privateKeyStructure.Coefficient);

			certificate2.PrivateKey = DotNetUtilities.ToRSA(rsaParams);

			return certificate2;
		}

		public static string GenerateRandomString(int min = 32, int max = 128)
		{
			int length = secureRandom.Next(min, max);
			StringBuilder stringBuilder = new StringBuilder();
			for (int i = 0; i < length; i++)
				stringBuilder.Append(characterPool[secureRandom.Next(characterPool.Length)]);

			return stringBuilder.ToString();
		}

		public static byte[] Encrypt(byte[] data, PublicKey publicKey)
		{
			byte[] bytes = null;
			using (RSACryptoServiceProvider rsa = (RSACryptoServiceProvider)publicKey.Key)
			{
				bytes = rsa.Encrypt(data, false); //TODO: think again about using PKCS
			}

			return bytes;
		}

		public static byte[] Decrypt(byte[] data, AsymmetricAlgorithm privateKey)
		{
			byte[] bytes = null;
			using (RSACryptoServiceProvider rsa = (RSACryptoServiceProvider)privateKey)
			{
				bytes = rsa.Decrypt(data, false); //TODO: think again about using PKCS
			}

			return bytes;
		}
	}
}
