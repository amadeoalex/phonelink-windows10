﻿using System.Net;

namespace PhoneLinkBase
{
	public static class NetworkUtils
	{
		//TODO: add debug check

		/// <summary>
		/// Return whether supplied <paramref name="address"/> is the address of the local DeviceNS.
		/// </summary>
		/// <param name="address"></param>
		/// <returns></returns>
		public static bool IsMyOwnAddres(IPAddress address)
		{
			if (IPAddress.IsLoopback(address))
				return true;

			IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
			foreach (IPAddress addr in host.AddressList)
			{
				if (address.Equals(addr))
					return true;
			}

			return false;
		}
	}
}
