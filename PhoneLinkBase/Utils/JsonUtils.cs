﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Json;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneLinkBase
{
	static class JsonUtils
	{
		public static readonly JsonObject InvalidJsonObject = new JsonObject();
		public static readonly JsonArray InvalidJsonArray = new JsonArray();

		public static JsonObject VerifyJsonObject(string serialized)
		{
			JsonObject jsonObject = InvalidJsonObject;
			try
			{
				jsonObject = (JsonObject)JsonValue.Parse(serialized);
			}
			catch (IOException) { }

			return jsonObject;
		}

		public static JsonArray VerifyJsonArray(string serialized)
		{
			JsonArray jsonArray = InvalidJsonArray;
			try
			{
				jsonArray = (JsonArray)JsonValue.Parse(serialized);
			}
			catch (IOException) { }

			return jsonArray;
		}
	}
}
