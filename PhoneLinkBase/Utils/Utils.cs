﻿using System;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Threading;
using Windows.Devices.Power;
using Windows.System.Power;

namespace PhoneLinkBase
{
	public static class Utils
	{
		private static readonly Random random = new Random();

		/// <summary>
		/// Replaces all hundreds from <paramref name="number"/> with zeros.
		/// </summary>
		/// <param name="number"></param>
		/// <returns></returns>
		public static long RemoveHundreds(long number)
		{
			return number / 1000 * 1000;
		}

		/// <summary>
		/// Returns milliseconds elapsed from Thursday, 1 January 1970
		/// </summary>
		/// <returns></returns>
		public static long UnixMiliseconds()
		{
			return DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
		}

		/// <summary>
		/// Returns ID of thread which called this function
		/// </summary>
		/// <returns></returns>
		public static int GetThreadId()
		{
			return Thread.CurrentThread.ManagedThreadId;
		}

		/// <summary>
		/// Sets calling thread UI and default culture to specified <paramref name="culture"/>.
		/// Defaults to en-GB.
		/// </summary>
		public static void SetThreadCultureTo(string culture = "en-GB")
		{
			CultureInfo.DefaultThreadCurrentUICulture = Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(culture);
		}

		/// <summary>
		/// Returns random integer within specified <paramref name="min"/> - <paramref name="max"/> range.
		/// </summary>
		/// <param name="min">Minimum value</param>
		/// <param name="max">Maximum value</param>
		/// <returns></returns>
		public static int GetRandomInt(int min, int max)
		{
			return random.Next(min, max);
		}

		/// <summary>
		/// Returns <see langword="true"/> if machine on which program is running is desktop or battery powered DeviceNS.
		/// </summary>
		/// <returns></returns>
		public static bool IsDesktopMachine()
		{
			BatteryReport batteryReport = Battery.AggregateBattery.GetReport();
			return batteryReport.Status == BatteryStatus.NotPresent;
		}
	}
}
