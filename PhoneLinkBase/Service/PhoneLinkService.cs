﻿using DeviceNS;
using PhoneLinkBase.DeviceNS;
using PhoneLinkBase.DeviceNS.Connection.Pairing;
using PhoneLinkBase.Feature;
using PhoneLinkBase.Service.Network;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;




namespace PhoneLinkBase.Service
{
	public sealed class PhoneLinkService : IUDPNetPackageHandler, IDisposable
	{
		private const string TAG = "PhoneLink Service";

		#region Instance
		private static readonly Lazy<PhoneLinkService> lazyInstance = new Lazy<PhoneLinkService>(() => new PhoneLinkService(), true);

		/// <summary>
		/// Returns PhoneLinkService instance
		/// </summary>
		public static PhoneLinkService Instance { get { return lazyInstance.Value; } }
		#endregion

		#region Properties
		public UDPServer UdpServer { get; }
		public DeviceManager DeviceManager { get; }
		public PairingManager PairingManager { get; }
		#endregion

		#region Easy access properties
		public static UDPServer UdpServerI { get => Instance.UdpServer; }
		public static DeviceManager DeviceManagerI { get => Instance.DeviceManager; }
		public static PairingManager PairingManagerI { get => Instance.PairingManager; }
		#endregion

		public List<string> PackageTypes { get; } = new List<string>
		{
			PairingProcess.PACKAGE_TYPE_PAIR_REQUEST
		};

		#region Constructor
		private PhoneLinkService()
		{
			Log.I(TAG, "constructor");

			UdpServer = new UDPServer();
			UdpServer.AddPackageHandler(this);
			DeviceManager = new DeviceManager(UdpServer);
			PairingManager = new PairingManager(DeviceManager, UdpServer);
		}
		#endregion

		public void Start()
		{
			UdpServer.Start();
		}

		public bool HandleUDPNetPackage(NetPackage netPackage, IPEndPoint endPoint)
		{
			switch (netPackage.Type)
			{
				//TODO: move to phone link service
				case PairingProcess.PACKAGE_TYPE_PAIR_REQUEST:
					{
						Log.D(TAG, $"pair request package received from {endPoint.Address}");

						Device device = Device.CreateFromIdNetPackage(netPackage);
						device.Address = endPoint.Address.ToString();
						PairingProcess pairingProcess = PairingManager.GetPairingProcess(device, true);
						if (!pairingProcess.IsAlive)
						{
							pairingProcess.OnFinished = (success, deviceCandidate) =>
							{
								if (success)
									DeviceManager.Add(deviceCandidate);
							};
							pairingProcess.Start();
						}

						return true;
					}

				default:
					return false;
			}
		}

		public void Cleanup()
		{
			Dispose();
			Log.I(TAG, "cleaned up");
		}

		#region IDisposable Support

		private bool disposed;

		public void Dispose()
		{
			if (disposed)
				return;

			if (UdpServer != null)
			{
				UdpServer.Interrupt();
				UdpServer.Join();
			}

			DeviceManager?.Dispose();

			disposed = true;
		}

		~PhoneLinkService()
		{
			Dispose();
		}
		#endregion
	}
}
