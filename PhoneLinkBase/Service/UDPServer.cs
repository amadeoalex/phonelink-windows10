﻿using DeviceNS;
using PhoneLinkBase.Service.Network;
using PhoneLinkBase.UtilClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace PhoneLinkBase.Service
{
	public interface IUDPNetPackageHandler
	{
		List<string> PackageTypes { get; }
		bool HandleUDPNetPackage(NetPackage netPackage, IPEndPoint endPoint);
	}

	public class UDPServer : SimpleThread, IDisposable
	{
		private const string TAG = "UDPServer";

		public const int LISTEN_PORT = 4848;

		private readonly List<IUDPNetPackageHandler> packageHandlers = new List<IUDPNetPackageHandler>();
		private readonly UdpClient udpClient;

		public bool Running { get; private set; } = true;

		public UDPServer()
		{
			udpClient = new UdpClient(4848);
			udpClient.EnableBroadcast = true;
		}

		public override void Run()
		{
			try
			{
				IPEndPoint endPoint = new IPEndPoint(IPAddress.Any, 0);
				while (Running)
				{
					byte[] buffer = udpClient.Receive(ref endPoint);
					try
					{
						NetPackage netPackage = NetPackage.Deserialize(Encoding.ASCII.GetString(buffer));
						NetPackageReceived(netPackage, endPoint);
					}
					catch (ArgumentException e)
					{
						Log.D(TAG, $"invalid json: {Encoding.ASCII.GetString(buffer)}, e: {e.Message}");
					}

				}
			}
			catch (SocketException e)
			{
				Running = false;
				Log.D(TAG, $"socket exception: {e.Message}");
			}
		}

		private void NetPackageReceived(NetPackage netPackage, IPEndPoint endPoint)
		{
			if (netPackage[Device.KEY_ID] == Environment.MachineName)
				return;

			packageHandlers.RemoveAll(item => item == null);

			bool handled = false;
			foreach (var handler in packageHandlers)
			{
				if (handler.PackageTypes.Contains(netPackage.Type))
					if (handler.HandleUDPNetPackage(netPackage, endPoint))
						handled = true;
			}

			if (!handled)
				Log.D(TAG, $"unknown package received {netPackage.Type}");
		}

		public void AddPackageHandler(IUDPNetPackageHandler handler)
		{
			lock (packageHandlers)
			{
				packageHandlers.Add(handler);
				packageHandlers.RemoveAll(item => item == null);
			}
		}

		public void SendNetPackage(NetPackage netPackage, IPEndPoint endPoint)
		{
			byte[] buffer = Encoding.ASCII.GetBytes(netPackage.ToString());
			udpClient.Send(buffer, buffer.Length, endPoint.Address.ToString(), endPoint.Port);
		}

		public override void Interrupt()
		{
			base.Interrupt();
			if (Running)
			{
				Running = false;
				udpClient?.Close();
			}
		}

		#region IDisposable Support

		private bool disposed;
		public void Dispose()
		{
			if (disposed)
				return;

			if (udpClient != null)
				udpClient.Close();

			disposed = true;
		}

		~UDPServer()
		{
			Dispose();
		}
		#endregion
	}
}
