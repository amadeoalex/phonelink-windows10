﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace PhoneLinkBase.Service.Network
{
	public sealed class TimeoutUdpClient : IDisposable
	{
		private Exception exception;
		private readonly ManualResetEvent timeout = new ManualResetEvent(false);

		private readonly UdpClient udpClient;

		public IPEndPoint remoteEndPoint;

		private bool received = false;
		private byte[] data;

		public TimeoutUdpClient()
		{
			udpClient = new UdpClient();
		}

		/// <summary>
		/// Sends <paramref name="data"/> to <paramref name="ipAddress"/> on <paramref name="port"/>.
		/// </summary>
		/// <param name="ipAddress"></param>
		/// <param name="port"></param>
		/// <param name="data"></param>
		public void Send(IPAddress ipAddress, int port, string data)
		{
			remoteEndPoint = new IPEndPoint(ipAddress, port);
			byte[] bytes = Encoding.ASCII.GetBytes(data);
			udpClient.Send(bytes, bytes.Length, remoteEndPoint);
		}

		/// <summary>
		/// Receives data from <paramref name="ipAddress"/> on <paramref name="port"/>.
		/// Throws <see cref="TimeoutException"/> after <paramref name="timeoutMSec"/>.
		/// </summary>
		/// <param name="ipAddress"></param>
		/// <param name="port"></param>
		/// <param name="timeoutMSec"></param>
		/// <returns></returns>
		public byte[] Receive(IPAddress ipAddress, int port, int timeoutMSec = 30)
		{
			remoteEndPoint = new IPEndPoint(ipAddress, port);

			exception = null;
			timeout.Reset();

			udpClient.BeginReceive(ReceiveCallback, udpClient);

			if (timeout.WaitOne(timeoutMSec, false))
			{
				if (received)
					return data;
				else
					throw exception;
			}
			else
			{
				udpClient.Close();
				throw new TimeoutException("listener timeout exception");
			}
		}

		private void ReceiveCallback(IAsyncResult asyncResult)
		{
			try
			{
				data = udpClient.EndReceive(asyncResult, ref remoteEndPoint);
				if (data?.Length > 0)
					received = true;


			}
			catch (Exception e)
			{
				exception = e;
			}
			finally
			{
				timeout.Set();
			}
		}

		#region IDisposable Support

		private bool disposed;

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2213:DisposableFieldsShouldBeDisposed", Justification = "Null-conditional confuses the code analysis.")]
		public void Dispose()
		{
			if (disposed)
				return;

			udpClient?.Close();
			timeout?.Close();

			disposed = true;
		}

		~TimeoutUdpClient()
		{
			Dispose();
		}
		#endregion
	}
}
