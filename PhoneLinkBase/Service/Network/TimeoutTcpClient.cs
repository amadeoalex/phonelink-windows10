﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace PhoneLinkBase.Service.Network
{
	public sealed class TimeoutTcpClient : IDisposable
	{
		private bool connected;
		private Exception exception;
		private readonly ManualResetEvent timeout = new ManualResetEvent(false);

		private readonly TcpClient tcpClient;

		public TimeoutTcpClient()
		{
			tcpClient = new TcpClient();
		}

		/// <summary>
		/// Connects to <paramref name="address"/> on <paramref name="port"/> and returns <see cref="TcpClient"/>.
		/// Throws <see cref="TimeoutException"/> after <paramref name="timeoutMSec"/>.
		/// </summary>
		/// <param name="address"></param>
		/// <param name="port"></param>
		/// <param name="timeoutMSec"></param>
		/// <returns></returns>
		public TcpClient Connect(string host, int port, int timeoutMSec = 30)
		{
			connected = false;
			exception = null;
			timeout.Reset();

			tcpClient.BeginConnect(host, port, new AsyncCallback(ConnectionCallback), tcpClient);

			if (timeout.WaitOne(timeoutMSec, false))
			{
				if (connected)
					return tcpClient;
				else
					throw exception;
			}
			else
			{
				tcpClient.Close();
				throw new TimeoutException("socket timeout exception");
			}
		}

		private void ConnectionCallback(IAsyncResult asyncResult)
		{
			try
			{
				TcpClient tcpClient = asyncResult.AsyncState as TcpClient;
				if (tcpClient.Client != null)
				{
					tcpClient.EndConnect(asyncResult);
					connected = true;
				}
			}
			catch (Exception e)
			{
				exception = e;
			}
			finally
			{
				if (!disposed && !timeout.WaitOne(0))
					timeout.Set();
			}
		}

		#region IDisposable Support

		private bool disposed;

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2213:DisposableFieldsShouldBeDisposed", Justification = "Null-conditional confuses the code analysis.")]
		public void Dispose()
		{
			if (disposed)
				return;

			tcpClient?.Close();
			timeout?.Close();

			disposed = true;
		}

		~TimeoutTcpClient()
		{
			Dispose();
		}
		#endregion
	}
}
