﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace PhoneLinkBase.Service.Network
{
	public sealed class TimeoutTcpListener : IDisposable
	{
		private bool connected;
		private Exception exception;
		private readonly ManualResetEvent timeout = new ManualResetEvent(false);

		private readonly TcpListener tcpListener;
		private TcpClient tcpClient;

		public TimeoutTcpListener(IPAddress address, int port)
		{
			tcpListener = new TcpListener(address, port);
			tcpListener.Server.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
		}

		/// <summary>
		/// Listens on provided address and port.
		/// Throws <see cref="TimeoutException"/> after <paramref name="timeoutMSec"/>.
		/// </summary>
		/// <param name="timeoutMSec"></param>
		/// <returns></returns>
		public TcpClient Accept(int timeoutMSec = 30)
		{
			connected = false;
			exception = null;
			timeout.Reset();

			tcpListener.Start();
			tcpListener.BeginAcceptTcpClient(ConnectedCallback, tcpListener);

			if (timeout.WaitOne(timeoutMSec, false))
			{
				if (connected)
					return tcpClient;
				else
					throw exception;
			}
			else
			{
				tcpListener.Stop();
				throw new TimeoutException("listener socket timeout exception");
			}
		}

		private void ConnectedCallback(IAsyncResult asyncResult)
		{
			if (disposed)
				return;

			try
			{
				TcpListener tcpListener = asyncResult.AsyncState as TcpListener;
				tcpClient = tcpListener.EndAcceptTcpClient(asyncResult);
				if (tcpClient.Client != null)
					connected = true;
			}
			catch (Exception e)
			{
				exception = e;
			}
			finally
			{
				if (!disposed)
					timeout.Set();
			}
		}

		#region IDisposable Support

		private bool disposed;

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2213:DisposableFieldsShouldBeDisposed", Justification = "Null-conditional confuses the code analysis.")]
		public void Dispose()
		{
			if (disposed)
				return;

			tcpListener.Stop();
			timeout?.Close();

			disposed = true;
		}

		~TimeoutTcpListener()
		{
			Dispose();
		}
		#endregion
	}
}
