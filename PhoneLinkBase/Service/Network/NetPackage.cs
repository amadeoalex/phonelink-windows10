﻿using System;
using System.Collections.Generic;
using System.Json;

namespace PhoneLinkBase.Service.Network
{
	public class NetPackage : JsonObject
	{
		private const string KEY_TYPE = "__type__";

		public string Type
		{
			get { return this[KEY_TYPE]; }
			set { this[KEY_TYPE] = value; }
		}

		/// <summary>
		/// Returns <paramref name="type"/> net package.
		/// </summary>
		/// <param name="type"></param>
		public NetPackage(string type = "")
		{
			this[KEY_TYPE] = type;
		}

		private NetPackage(IEnumerable<KeyValuePair<string, JsonValue>> items) : base(items)
		{

		}

		/// <summary>
		/// Returns net package deserialized from <paramref name="serialized"/>.
		/// </summary>
		/// <param name="serialized"></param>
		/// <returns></returns>
		public static NetPackage Deserialize(string serialized)
		{
			JsonObject jsonObject = (JsonObject)JsonValue.Parse(serialized);
			return new NetPackage((IEnumerable<KeyValuePair<string, JsonValue>>)jsonObject);
		}
	}
}
