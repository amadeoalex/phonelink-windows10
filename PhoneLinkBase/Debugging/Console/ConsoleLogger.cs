﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Documents;

namespace PhoneLinkBase.Debug
{
	class ConsoleLogger
	{
		private readonly object lockObject = new object();

		private const bool logEnabled = true;
		private ReadOnlyObservableCollection<LogEntry> logs;

		public ConsoleLogger()
		{
			logs = Log.Instance.Logs;
			foreach (var log in logs)
			{
				PrintLog(log);
			}

			((INotifyCollectionChanged)logs).CollectionChanged += NewLog;
		}

		private void NewLog(object sender, NotifyCollectionChangedEventArgs e)
		{
			if (e.Action != NotifyCollectionChangedAction.Add)
				return;

			foreach (LogEntry item in e.NewItems)
				PrintLog(item);
		}

		private void PrintLog(LogEntry logEntry)
		{
			(ConsoleColor textColor, ConsoleColor backgroundColor) = GetLogColors(logEntry);

			ConsoleColor oldTextColor = Console.ForegroundColor;
			ConsoleColor oldBackgroundColor = Console.BackgroundColor;

			Console.ForegroundColor = textColor;
			Console.BackgroundColor = backgroundColor;

			Console.WriteLine(logEntry.ToString());

			Console.ForegroundColor = oldTextColor;
			Console.BackgroundColor = oldBackgroundColor;
		}

		private (ConsoleColor, ConsoleColor) GetLogColors(LogEntry logEntry)
		{
			ConsoleColor textColor = ConsoleColor.White;
			ConsoleColor backgroundColor = ConsoleColor.Black;

			switch (logEntry.LogLevel)
			{
				case LogLevel.Verbose:
					{
						textColor = ConsoleColor.Gray;
						backgroundColor = ConsoleColor.Black;
						break;
					}
				case LogLevel.Info:
					{
						textColor = ConsoleColor.DarkYellow;
						backgroundColor = ConsoleColor.Black;
						break;
					}
				case LogLevel.Debug:
					{
						textColor = ConsoleColor.White;
						backgroundColor = ConsoleColor.Black;
						break;
					}
				case LogLevel.Warning:
					{
						textColor = ConsoleColor.DarkRed;
						backgroundColor = ConsoleColor.Black;
						break;
					}
				case LogLevel.Error:
					{
						textColor = ConsoleColor.Red;
						backgroundColor = ConsoleColor.Black;
						break;
					}
			}

			return (textColor, backgroundColor);
		}
	}
}
