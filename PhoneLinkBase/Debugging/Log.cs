﻿using PhoneLinkBase.Debug;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Data;
using System.Windows.Documents;

namespace PhoneLinkBase
{
	public class Log : INotifyPropertyChanged
	{
		#region Instance
		private static readonly Lazy<Log> lazyInstance = new Lazy<Log>(() => new Log(), true);

		/// <summary>
		/// Returns Log instance
		/// </summary>
		public static Log Instance { get { return lazyInstance.Value; } }
		#endregion

		#region Property changed
#pragma warning disable 67
		public event PropertyChangedEventHandler PropertyChanged;
#pragma warning restore 67
		#endregion

		private object logsLockObject = new object();
		private ObservableCollection<LogEntry> internalLogs;
		private readonly ReadOnlyObservableCollection<LogEntry> readOnlyInternalLogs;

		public ReadOnlyObservableCollection<LogEntry> Logs { get { return readOnlyInternalLogs; } }

		public Log()
		{
			internalLogs = new ObservableCollection<LogEntry>();
			readOnlyInternalLogs = new ReadOnlyObservableCollection<LogEntry>(internalLogs);

			BindingOperations.EnableCollectionSynchronization(Logs, logsLockObject);
		}

		/// <summary>
		/// Adds verbose log message.
		/// </summary>
		/// <param name="tag"></param>
		/// <param name="contents"></param>
		public static void V(string tag, object message)
		{
			Instance.internalLogs.Add(new LogEntry(LogLevel.Verbose, tag, message));
		}

		/// <summary>
		/// Adds information log message.
		/// </summary>
		/// <param name="tag"></param>
		/// <param name="contents"></param>
		public static void I(string tag, object message)
		{
			Instance.internalLogs.Add(new LogEntry(LogLevel.Info, tag, message));
		}

		/// <summary>
		/// Adds debug log message.
		/// </summary>
		/// <param name="tag"></param>
		/// <param name="contents"></param>
		public static void D(string tag, object message)
		{
			Instance.internalLogs.Add(new LogEntry(LogLevel.Debug, tag, message));
		}

		/// <summary>
		/// Adds information warning message.
		/// </summary>
		/// <param name="tag"></param>
		/// <param name="contents"></param>
		public static void W(string tag, object message)
		{
			Instance.internalLogs.Add(new LogEntry(LogLevel.Warning, tag, message));
		}

		/// <summary>
		/// Adds error log message.
		/// </summary>
		/// <param name="tag"></param>
		/// <param name="contents"></param>
		public static void E(string tag, object message)
		{
			Instance.internalLogs.Add(new LogEntry(LogLevel.Error, tag, message));
		}
	}
}
