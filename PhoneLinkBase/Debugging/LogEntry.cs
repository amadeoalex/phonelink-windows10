﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneLinkBase.Debug
{
	public enum LogLevel
	{
		Verbose,
		Info,
		Debug,
		Warning,
		Error
	}

	public class LogEntry
	{
		public LogLevel LogLevel { get; }
		public string Tag { get; }
		public object Message { get; }

		public LogEntry(LogLevel logLevel, string tag, object message)
		{
			LogLevel = logLevel;
			Tag = tag;
			Message = message;
		}

		public override string ToString()
		{
			return $"[{Tag}] {Message}";
		}
	}
}
