﻿using System;
using System.Json;

namespace PhoneLinkBase
{
	public static class JsonExtension
	{
		/// <summary>
		/// Returns JsonObject with given key.
		/// If requested object does not exist, creates new one and returns it.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="key">Requested object</param>
		/// <returns></returns>
		public static JsonObject OptJsonObject(this JsonObject source, string key)
		{
			if (!source.ContainsKey(key))
				source[key] = new JsonObject();

			return (JsonObject)source[key];
		}

		/// <summary>
		/// Returns JsonArray with given key.
		/// If requested array does not exist, creates new one and returns it.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="key">Requested object</param>
		/// <returns></returns>
		public static JsonArray OptJsonArray(this JsonObject source, string key)
		{
			if (!source.ContainsKey(key))
				source[key] = new JsonArray();

			return (JsonArray)source[key];
		}

		/// <summary>
		/// Returns boolean with given key.
		/// If requested boolean does not exist, inserts fallback value with provided key and returns it.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="key"></param>
		/// <param name="fallback"></param>
		/// <returns></returns>
		public static bool OptBool(this JsonObject source, string key, bool fallback)
		{
			if (!source.ContainsKey(key))
				source[key] = fallback;

			return source[key];
		}

		/// <summary>
		/// Returns string with given key.
		/// If requested string does not exist, inserts fallback value with provided key and returns it.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="key"></param>
		/// <param name="fallback"></param>
		/// <returns></returns>
		public static string OptString(this JsonObject source, string key, string fallback)
		{
			if (!source.ContainsKey(key))
				source[key] = fallback;

			return source[key];
		}

		/// <summary>
		/// Returns int with given key.
		/// If required int does not exits, inserts fallback value with provided key and returns it.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="key"></param>
		/// <param name="fallback"></param>
		/// <returns></returns>
		public static int OptInt(this JsonObject source, string key, int fallback)
		{
			if (!source.ContainsKey(key))
				source[key] = fallback;

			return source[key];
		}
	}
}
