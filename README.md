# Phone Link

Phone Link for Windows 10 is an app designed to act as a bridge between Windows 10 and Android device.
Main functionality of this application is to enable user to read and respond to messages received on their Android device.

## Project status

 - [x] Alive - in development
 - [x] Pending partial rewrite


## Installation

For now the only way to use the app is to download the sources and build it.

## Built With

* [Visual Studio](https://visualstudio.microsoft.com/pl/)

## Authors

Paweł Wojtczak

## License

This project is licensed under the MIT License - see the [LICENSE.txt](LICENSE.txt) file for details

## Note

This is my first "bigger" project involving use of C# language. I did my best to use only recommended practises but some parts of the app require minor polishing - hence the pending partial rewrite status.

I am aware that there is software both for Windows 10 and Android with the same functionality that I am trying to achieve. My goal is to create an open source app requiring no account creation or third party personal data transfer.
