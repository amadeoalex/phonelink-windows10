﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using Windows.UI.ViewManagement;

namespace PhoneLinkUI
{
	public static class ThemeResources
	{
		public enum Theme
		{
			Light,
			Dark,
			System
		}

		public static Theme AppTheme { get; set; } = Theme.Light;
		public static bool IsDarkTheme { get; } = AppTheme == Theme.Dark;

		#region Colors
		public static Color ForegroundColor { get; }
		public static Color BackgroundColor { get; }

		public static Color AccentColor { get; }
		public static Color AccentColorDark1 { get; }
		public static Color AccentColorDark2 { get; }
		public static Color AccentColorDark3 { get; }
		public static Color AccentColorLight1 { get; }
		public static Color AccentColorLight2 { get; }
		public static Color AccentColorLight3 { get; }

		public static Color AccentColorHigher1 { get; }
		public static Color AccentColorHigher2 { get; }
		public static Color AccentColorHigher3 { get; }
		public static Color AccentColorLower1 { get; }
		public static Color AccentColorLower2 { get; }
		public static Color AccentColorLower3 { get; }

		public static Color StatusBarColor { get; }
		#endregion

		#region Brushes
		public static Brush ForegroundBrush { get; }
		public static Brush BackgroundBrush { get; }

		public static Brush AccentBrush { get; }
		public static Brush AccentBrushDark1 { get; }
		public static Brush AccentBrushDark2 { get; }
		public static Brush AccentBrushDark3 { get; }
		public static Brush AccentBrushLight1 { get; }
		public static Brush AccentBrushLight2 { get; }
		public static Brush AccentBrushLight3 { get; }

		public static Brush StatusBarBrush { get; }
		#endregion

		static ThemeResources()
		{
			UISettings uiSettings = new UISettings();

			ForegroundColor = SystemColors.BaseHigh;
			BackgroundColor = SystemColors.AltHigh;

			AccentColor = uiSettings.GetColorValue(UIColorType.Accent).ToMediaColor();
			AccentColorDark1 = uiSettings.GetColorValue(UIColorType.AccentDark1).ToMediaColor();
			AccentColorDark2 = uiSettings.GetColorValue(UIColorType.AccentDark2).ToMediaColor();
			AccentColorDark3 = uiSettings.GetColorValue(UIColorType.AccentDark3).ToMediaColor();
			AccentColorLight1 = uiSettings.GetColorValue(UIColorType.AccentLight1).ToMediaColor();
			AccentColorLight2 = uiSettings.GetColorValue(UIColorType.AccentLight2).ToMediaColor();
			AccentColorLight3 = uiSettings.GetColorValue(UIColorType.AccentLight3).ToMediaColor();

			AccentColorHigher1 = IsDarkTheme ? AccentColorDark1 : AccentColorLight1;
			AccentColorHigher2 = IsDarkTheme ? AccentColorDark2 : AccentColorLight2;
			AccentColorHigher3 = IsDarkTheme ? AccentColorDark3 : AccentColorLight3;

			AccentColorLower1 = IsDarkTheme ? AccentColorLight1 : AccentColorDark1;
			AccentColorLower2 = IsDarkTheme ? AccentColorLight2 : AccentColorDark2;
			AccentColorLower3 = IsDarkTheme ? AccentColorLight3 : AccentColorDark3;

			StatusBarColor = IsStatusBarColored() ? AccentColor : ForegroundColor;

			ForegroundBrush = new SolidColorBrush(ForegroundColor);
			BackgroundBrush = new SolidColorBrush(BackgroundColor);

			AccentBrush = new SolidColorBrush(AccentColor);
			AccentBrushDark1 = new SolidColorBrush(AccentColorDark1);
			AccentBrushDark2 = new SolidColorBrush(AccentColorDark2);
			AccentBrushDark3 = new SolidColorBrush(AccentColorDark3);
			AccentBrushLight1 = new SolidColorBrush(AccentColorLight1);
			AccentBrushLight2 = new SolidColorBrush(AccentColorLight2);
			AccentBrushLight3 = new SolidColorBrush(AccentColorLight3);

			StatusBarBrush = new SolidColorBrush(StatusBarColor);
		}

		private static bool IsStatusBarColored()
		{
			string registryKey = @"HKEY_CURRENT_USER\Software\Microsoft\Windows\DWM";
			string registryValue = "ColorPrevalence";
			int value = (int)Registry.GetValue(registryKey, registryValue, 0);
			return value == 1;
		}
	}
}
