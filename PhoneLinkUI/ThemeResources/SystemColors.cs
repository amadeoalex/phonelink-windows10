﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace PhoneLinkUI
{
	public static class SystemColors
	{
		public static Color AltLightHigh => (Color)ColorConverter.ConvertFromString("#FFFFFFFF");
		public static Color AltLightLow => (Color)ColorConverter.ConvertFromString("#33FFFFFF");
		public static Color AltLightMedium => (Color)ColorConverter.ConvertFromString("#99FFFFFF");
		public static Color AltLightMediumHigh => (Color)ColorConverter.ConvertFromString("#CCFFFFFF");
		public static Color AltLightMediumLow => (Color)ColorConverter.ConvertFromString("#66FFFFFF");
		public static Color BaseLightHigh => (Color)ColorConverter.ConvertFromString("#FF000000");
		public static Color BaseLightLow => (Color)ColorConverter.ConvertFromString("#33000000");
		public static Color BaseLightMedium => (Color)ColorConverter.ConvertFromString("#99000000");
		public static Color BaseLightMediumHigh => (Color)ColorConverter.ConvertFromString("#CC000000");
		public static Color BaseLightMediumLow => (Color)ColorConverter.ConvertFromString("#66000000");
		public static Color ChromeLightAltLow => (Color)ColorConverter.ConvertFromString("#FF171717");
		public static Color ChromeLightBlackHigh => (Color)ColorConverter.ConvertFromString("#FF000000");
		public static Color ChromeLightBlackLow => (Color)ColorConverter.ConvertFromString("#33000000");
		public static Color ChromeLightBlackMediumLow => (Color)ColorConverter.ConvertFromString("#66000000");
		public static Color ChromeLightBlackMedium => (Color)ColorConverter.ConvertFromString("#CC000000");
		public static Color ChromeLightDisabledHigh => (Color)ColorConverter.ConvertFromString("#FFCCCCCC");
		public static Color ChromeLightDisabledLow => (Color)ColorConverter.ConvertFromString("#FF7A7A7A");
		public static Color ChromeLightHigh => (Color)ColorConverter.ConvertFromString("#FFCCCCCC");
		public static Color ChromeLightLow => (Color)ColorConverter.ConvertFromString("#FFF2F2F2");
		public static Color ChromeLightMedium => (Color)ColorConverter.ConvertFromString("#FFE6E6E6");
		public static Color ChromeLightMediumLow => (Color)ColorConverter.ConvertFromString("#FFF2F2F2");
		public static Color ChromeLightWhite => (Color)ColorConverter.ConvertFromString("#FFFFFFFF");
		public static Color ListLightLow => (Color)ColorConverter.ConvertFromString("#19000000");
		public static Color ListLightMedium => (Color)ColorConverter.ConvertFromString("#33000000");

		public static Color AltDarkHigh => (Color)ColorConverter.ConvertFromString("#FF000000");
		public static Color AltDarkLow => (Color)ColorConverter.ConvertFromString("#33000000");
		public static Color AltDarkMedium => (Color)ColorConverter.ConvertFromString("#99000000");
		public static Color AltDarkMediumHigh => (Color)ColorConverter.ConvertFromString("#CC000000");
		public static Color AltDarkMediumLow => (Color)ColorConverter.ConvertFromString("#66000000");
		public static Color BaseDarkHigh => (Color)ColorConverter.ConvertFromString("#FFFFFFFF");
		public static Color BaseDarkLow => (Color)ColorConverter.ConvertFromString("#33FFFFFF");
		public static Color BaseDarkMedium => (Color)ColorConverter.ConvertFromString("#99FFFFFF");
		public static Color BaseDarkMediumHigh => (Color)ColorConverter.ConvertFromString("#CCFFFFFF");
		public static Color BaseDarkMediumLow => (Color)ColorConverter.ConvertFromString("#66FFFFFF");
		public static Color ChromeDarkAltLow => (Color)ColorConverter.ConvertFromString("#FFF2F2F2");
		public static Color ChromeDarkBlackHigh => (Color)ColorConverter.ConvertFromString("#FF000000");
		public static Color ChromeDarkBlackLow => (Color)ColorConverter.ConvertFromString("#33000000");
		public static Color ChromeDarkBlackMediumLow => (Color)ColorConverter.ConvertFromString("#66000000");
		public static Color ChromeDarkBlackMedium => (Color)ColorConverter.ConvertFromString("#CC000000");
		public static Color ChromeDarkDisabledHigh => (Color)ColorConverter.ConvertFromString("#FF333333");
		public static Color ChromeDarkDisabledLow => (Color)ColorConverter.ConvertFromString("#FF858585");
		public static Color ChromeDarkHigh => (Color)ColorConverter.ConvertFromString("#FF767676");
		public static Color ChromeDarkLow => (Color)ColorConverter.ConvertFromString("#FF171717");
		public static Color ChromeDarkMedium => (Color)ColorConverter.ConvertFromString("#FF1F1F1F");
		public static Color ChromeDarkMediumLow => (Color)ColorConverter.ConvertFromString("#FF2B2B2B");
		public static Color ChromeDarkWhite => (Color)ColorConverter.ConvertFromString("#FFFFFFFF");
		public static Color ListDarkLow => (Color)ColorConverter.ConvertFromString("#19FFFFFF");
		public static Color ListDarkMedium => (Color)ColorConverter.ConvertFromString("#33FFFFFF");

		public static Color AltHigh => ThemeResources.IsDarkTheme ? AltDarkHigh : AltLightHigh;
		public static Color AltLow => ThemeResources.IsDarkTheme ? AltDarkLow : AltLightLow;
		public static Color AltMedium => ThemeResources.IsDarkTheme ? AltDarkMedium : AltLightMedium;
		public static Color AltMediumHigh => ThemeResources.IsDarkTheme ? AltDarkMediumHigh : AltLightMediumHigh;
		public static Color AltMediumLow => ThemeResources.IsDarkTheme ? AltDarkMediumLow : AltLightMediumLow;
		public static Color BaseHigh => ThemeResources.IsDarkTheme ? BaseDarkHigh : BaseLightHigh;
		public static Color BaseLow => ThemeResources.IsDarkTheme ? BaseDarkLow : BaseLightLow;
		public static Color BaseMedium => ThemeResources.IsDarkTheme ? BaseDarkMedium : BaseLightMedium;
		public static Color BaseMediumHigh => ThemeResources.IsDarkTheme ? BaseDarkMediumHigh : BaseLightMediumHigh;
		public static Color BaseMediumLow => ThemeResources.IsDarkTheme ? BaseDarkMediumLow : BaseLightMediumLow;
		public static Color ChromeAltLow => ThemeResources.IsDarkTheme ? ChromeDarkAltLow : ChromeLightAltLow;
		public static Color ChromeBlackHigh => ThemeResources.IsDarkTheme ? ChromeDarkBlackHigh : ChromeLightBlackHigh;
		public static Color ChromeBlackLow => ThemeResources.IsDarkTheme ? ChromeDarkBlackLow : ChromeLightBlackLow;
		public static Color ChromeBlackMediumLow => ThemeResources.IsDarkTheme ? ChromeDarkBlackMediumLow : ChromeLightBlackMediumLow;
		public static Color ChromeBlackMedium => ThemeResources.IsDarkTheme ? ChromeDarkBlackMedium : ChromeLightBlackMedium;
		public static Color ChromeDisabledHigh => ThemeResources.IsDarkTheme ? ChromeDarkDisabledHigh : ChromeLightDisabledHigh;
		public static Color ChromeDisabledLow => ThemeResources.IsDarkTheme ? ChromeDarkDisabledLow : ChromeLightDisabledLow;
		public static Color ChromeHigh => ThemeResources.IsDarkTheme ? ChromeDarkHigh : ChromeLightHigh;
		public static Color ChromeLow => ThemeResources.IsDarkTheme ? ChromeDarkLow : ChromeLightLow;
		public static Color ChromeMedium => ThemeResources.IsDarkTheme ? ChromeDarkMedium : ChromeLightMedium;
		public static Color ChromeMediumLow => ThemeResources.IsDarkTheme ? ChromeDarkMediumLow : ChromeLightMediumLow;
		public static Color ChromeWhite => ThemeResources.IsDarkTheme ? ChromeDarkWhite : ChromeLightWhite;
		public static Color ListLow => ThemeResources.IsDarkTheme ? ListDarkLow : ListLightLow;
		public static Color ListMedium => ThemeResources.IsDarkTheme ? ListDarkMedium : ListLightMedium;
	}
}
