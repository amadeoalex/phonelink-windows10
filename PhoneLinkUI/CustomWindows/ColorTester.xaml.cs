﻿using System;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Windows;
using System.Windows.Media;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;

namespace PhoneLinkUI.CustomWindows
{
	/// <summary>
	/// Interaction logic for ColorTester.xaml
	/// </summary>
	public partial class ColorTester : System.Windows.Window
	{
		public ColorTester()
		{
			InitializeComponent();

			ObservableCollection<ColorAndName> l = new ObservableCollection<ColorAndName>();

			foreach (PropertyInfo i in typeof(SystemColors).GetProperties())
			{
				if (i.PropertyType == typeof(Color))
				{
					Color color = (Color)i.GetValue(new Color(), BindingFlags.GetProperty, null, null, null);
					ColorAndName cn = new ColorAndName
					{
						Color = color,
						Name = i.Name,
						Hex = $"{color.A:X2}{color.R:X2}{color.G:X2}{color.B:X2}"
					};

					l.Add(cn);
				}
			}

			l.Add(new ColorAndName { Color = Colors.White, Name = "UISettings Colors BELOW" });

			UISettings uiSettings = new UISettings();

			foreach (UIColorType colorType in Enum.GetValues(typeof(UIColorType)))
			{
				try
				{
					Windows.UI.Color uiColor = uiSettings.GetColorValue(colorType);
					ColorAndName cn = new ColorAndName
					{
						Color = Color.FromArgb(uiColor.A, uiColor.R, uiColor.G, uiColor.B),
						Name = Enum.GetName(typeof(UIColorType), colorType),
						Hex = $"{uiColor.A:X2}{uiColor.R:X2}{uiColor.G:X2}{uiColor.B:X2}"
					};

					l.Add(cn);
				}
				catch (Exception e)
				{

				}
			}

			l.Add(new ColorAndName { Color = Colors.White, Name = "SystemColors BELOW" });

			foreach (PropertyInfo i in typeof(SystemColors).GetProperties())
			{
				if (i.PropertyType == typeof(Color))
				{
					Color color = (Color)i.GetValue(new Color(), BindingFlags.GetProperty, null, null, null);
					ColorAndName cn = new ColorAndName
					{
						Color = color,
						Name = i.Name,
						Hex = $"{color.A:X2}{color.R:X2}{color.G:X2}{color.B:X2}"
					};

					l.Add(cn);
				}
			}


			DataContext = l;
		}
	}

	class ColorAndName
	{
		public Color Color { get; set; }
		public string Name { get; set; }
		public string Hex { get; set; }
	}
}
