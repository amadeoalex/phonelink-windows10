﻿using PhoneLinkUI.ViewModels.Windows.LogWindow;
using System.Windows;

namespace PhoneLinkUI.CustomWindows
{
	/// <summary>
	/// Interaction logic for LogWindow.xaml
	/// </summary>
	public partial class LogWindow : Window
	{
		public LogWindow()
		{
			InitializeComponent();
			DataContext = new LogWindowViewModel();
		}
	}
}
