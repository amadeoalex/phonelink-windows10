﻿using PhoneLinkBase;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media.Imaging;
using Windows.Data.Xml.Dom;

namespace PhoneLinkUI
{
	internal static class UIUtils
	{
		private const string TAG = "UI Utils";

		private static readonly byte[] iconHeader = new byte[] { 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 24, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

		public static Bitmap BitmapFromString(string text, string fontFamily, int fontSize, Color backgroundColor, Color foregroundColor, int width, int height, int offsetX = 0, int offsetY = 0)
		{
			Bitmap bitmap = new Bitmap(width, height);
			using (Graphics graphics = Graphics.FromImage(bitmap))
			{
				Font font = new Font(fontFamily, fontSize);
				graphics.FillRectangle(new SolidBrush(backgroundColor), 0, 0, bitmap.Width, bitmap.Height);
				graphics.DrawString(text, font, new SolidBrush(foregroundColor), offsetX, offsetY);
				graphics.Flush();

				font.Dispose();
			}

			return bitmap;
		}

		public static BitmapSource BitmapSourceFromBitmap(Bitmap bitmap)
		{
			IntPtr bitmapHandlePointer = bitmap.GetHbitmap();
			BitmapSource bitmapSource = null;
			try
			{
				bitmapSource = Imaging.CreateBitmapSourceFromHBitmap(bitmapHandlePointer, IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
			}
			finally
			{
				NativeMethods.DeleteObject(bitmapHandlePointer);
			}

			return bitmapSource;
		}

		public static Icon IconFromBitmap(Bitmap bitmap)
		{
			byte[] pngData;
			using (MemoryStream memoryStream = new MemoryStream())
			{
				bitmap.Save(memoryStream, ImageFormat.Png);
				memoryStream.Position = 0;
				pngData = memoryStream.ToArray();
			}

			using (MemoryStream memoryStream = new MemoryStream())
			{
				iconHeader[6] = (byte)bitmap.Width;
				iconHeader[7] = (byte)bitmap.Width;
				iconHeader[14] = (byte)(pngData.Length & 255);
				iconHeader[15] = (byte)(pngData.Length / 256);
				iconHeader[18] = (byte)iconHeader.Length;

				memoryStream.Write(iconHeader, 0, iconHeader.Length);
				memoryStream.Write(pngData, 0, pngData.Length);
				memoryStream.Position = 0;

				return new Icon(memoryStream);
			}
		}

		/// <summary>
		/// Returns XmlDocument generated from provided <paramref name="xmlString"/>.
		/// </summary>
		/// <param name="xmlString"></param>
		/// <returns></returns>
		public static XmlDocument GetXmlDocument(string xmlString)
		{
			XmlDocument document = new XmlDocument();
			try
			{
				document.LoadXml(xmlString);
			}
			catch (Exception e)
			{
				Log.W(TAG, $"error loading XML document: {e.Message} \n Faulty string: {xmlString}");
			}
			return document;
		}

		public static string GetDependencyPropertyName(string dependencyProperty)
		{
			if (!dependencyProperty.EndsWith("Property")) throw new ArgumentException($"{dependencyProperty} should end with 'Property'");

			return dependencyProperty.Replace("Property", "");
		}
	}
}
