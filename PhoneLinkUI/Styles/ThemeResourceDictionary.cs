﻿using PhoneLinkBase;
using System;
using System.Windows;
using System.Windows.Markup;
using Windows.UI;
using Windows.UI.ViewManagement;
using static PhoneLinkUI.ThemeProvider;

[assembly: XmlnsDefinition("http://schemas.microsoft.com/winfx/2006/xaml/presentation",
"Utils")]
namespace PhoneLinkUI.Styles
{
	class ThemeResourceDictionary : ResourceDictionary
	{
		public new Uri Source { get { return base.Source; } set {  } }

		private Uri lightTheme;
		private Uri darkTheme;

		public ThemeResourceDictionary()
		{
			lightTheme = new Uri("pack://application:,,,/Styles/LightTheme.xaml");
			darkTheme = new Uri("pack://application:,,,/Styles/DarkTheme.xaml");

			if (ThemeResources.AppTheme == ThemeResources.Theme.Light)
				base.Source = lightTheme;
			else
				base.Source = darkTheme;
		}
	}
}
