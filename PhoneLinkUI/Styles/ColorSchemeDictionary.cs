﻿using PhoneLinkBase;
using System;
using System.Windows;
using System.Windows.Markup;
using Windows.UI;
using Windows.UI.ViewManagement;
using static PhoneLinkUI.ThemeProvider;

[assembly: XmlnsDefinition("http://schemas.microsoft.com/winfx/2006/xaml/presentation",
"Utils")]
namespace PhoneLinkUI.Styles
{
	class ColorSchemeDictionary : ResourceDictionary
	{
		private readonly UISettings uiSettings = new UISettings();

		//public Uri lightScheme;
		//public Uri LightScheme
		//{
		//	get { return lightScheme; }
		//	set
		//	{
		//		lightScheme = value;
		//		Update();
		//	}
		//}

		//Trick to stop visual studio xaml editor complaining about "Object does not match target type"
		//Sadly, the only way to make intellisense detect resources contained within color scheme resource dictionaries is to treat light one as "Source" 
		//(it does not seem to parse dictionaries without "Source" property defined)
		//downside is, when I want to edit dark color scheme, App.xaml needs to be edited to hard code black scheme - visual studio parses "Source" property without evaluating it?
		//TODO: find out other, cleaner way to do this.
		public new Uri Source { get { return base.Source; } set { base.Source = value; } }

		public Uri darkScheme;
		public Uri DarkScheme
		{
			get { return darkScheme; }
			set
			{
				darkScheme = value;
				Update();
			}
		}

		private bool DarkModeEnabled()
		{
			return uiSettings.GetColorValue(UIColorType.Background) != Colors.White;
		}

		public void Update()
		{
			if ((ColorScheme == ColorSchemes.Auto && DarkModeEnabled()) || ColorScheme == ColorSchemes.Dark)
				Source = DarkScheme;

			//Uri uri = ColorSheme == ColorScheme.Light ? Source : DarkScheme;
			//if (uri != null && Source != uri)
			//	Source = uri;
		}
	}
}
