﻿using PhoneLinkBase;
using PhoneLinkBase.Feature.Dummy;
using PhoneLinkBase.Service;
using PhoneLinkUI.FeatureUI;
using PhoneLinkUI.FeatureUI.Dummy;
using PhoneLinkUI.Notifications;
using System;

namespace PhoneLinkUI.UIService
{
	internal sealed class PhoneLinkUIService
	{
		private const string TAG = "Phone Link UI Service";

		#region Instance
		private static readonly Lazy<PhoneLinkUIService> lazyInstance = new Lazy<PhoneLinkUIService>(() => new PhoneLinkUIService(), true);

		/// <summary>
		/// Returns PhoneLinkUIService instance
		/// </summary>
		public static PhoneLinkUIService Instance { get { return lazyInstance.Value; } }
		#endregion

		#region Properties
		public IconManager IconManager { get; }
		public TrayManager TrayManager { get; }
		public WindowManager WindowManager { get; }
		public NotificationManager NotificationManager { get; }
		public FeatureUIManager FeatureUIManager { get; }
		#endregion

		#region Easy access properties
		public static IconManager IconManagerI { get => Instance.IconManager; }
		public static TrayManager TrayManagerI { get => Instance.TrayManager; }
		public static WindowManager WindowManagerI { get => Instance.WindowManager; }
		public static NotificationManager NotificationManagerI { get => Instance.NotificationManager; }
		public static FeatureUIManager FeatureUIManagerI { get => Instance.FeatureUIManager; }
		#endregion

		#region Constructor
		private PhoneLinkUIService()
		{
			Log.I(TAG, "constructor");
			IconManager = new IconManager();
			TrayManager = new TrayManager(IconManager.InvertedIcon);
			WindowManager = new WindowManager();
			NotificationManager = new NotificationManager();
			FeatureUIManager = new FeatureUIManager();

			RegisterFeatureUIs();
		}
		#endregion

		private void RegisterFeatureUIs()
		{
			//TelephonyNotificationsFeature notificationsBaseFeature = (TelephonyNotificationsFeature)PhoneLinkService.FeatureManagerI.Get(TelephonyNotificationsFeature.NAME);
			//FeatureUIManager.RegisterHandler(new TelephonyNotificationsFeatureUI(notificationsBaseFeature));

			//ContentShareFeature contentShareFeature = (ContentShareFeature)PhoneLinkService.FeatureManagerI.Get(ContentShareFeature.NAME);
			//FeatureUIManager.RegisterHandler(new ContentShareFeatureUI(contentShareFeature));

			//DummyFeature dummyFeature = (DummyFeature)PhoneLinkService.FeatureManagerI.Get(typeof(DummyFeature));
			//FeatureUIManager.RegisterHandler(new DummyFeatureUI(dummyFeature));
		}

		public void Cleanup()
		{
			TrayManager.Cleanup();
			WindowManager.Cleanup();
		}
	}
}
