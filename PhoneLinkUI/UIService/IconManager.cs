﻿using System.Drawing;
using System.Windows.Media;

namespace PhoneLinkUI.UIService
{
	public class IconManager
	{
		private const int ICON_SIZE = 512;
		private const int ICON_FONT_SIZE = (int)(ICON_SIZE / 1.3333f);
		private const string ICON_STRING = "\uE975";

		public ImageSource ImageSourceIcon { get; }
		public ImageSource InvertedImageSourceIcon { get; }

		public Icon Icon { get; }
		public Icon InvertedIcon { get; }

		public IconManager()
		{
			Bitmap iconBitmap = UIUtils.BitmapFromString(ICON_STRING, "Segoe MDL2 Assets", ICON_FONT_SIZE, System.Drawing.Color.Transparent, System.Drawing.Color.Black, ICON_SIZE, ICON_SIZE, -86);
			ImageSourceIcon = UIUtils.BitmapSourceFromBitmap(iconBitmap);
			Icon = UIUtils.IconFromBitmap(iconBitmap);

			Bitmap invertedIconBitmap = UIUtils.BitmapFromString(ICON_STRING, "Segoe MDL2 Assets", ICON_FONT_SIZE, System.Drawing.Color.Transparent, System.Drawing.Color.White, ICON_SIZE, ICON_SIZE, -86);
			InvertedImageSourceIcon = UIUtils.BitmapSourceFromBitmap(invertedIconBitmap);
			InvertedIcon = UIUtils.IconFromBitmap(invertedIconBitmap);
		}
	}
}
