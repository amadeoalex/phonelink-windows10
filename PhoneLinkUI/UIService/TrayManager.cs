﻿using PhoneLinkUI.CustomWindows;
using PhoneLinkUI.ViewModels.Windows;
using PhoneLinkUI.WPFWindows;
using System.Drawing;
using System.Windows.Forms;

namespace PhoneLinkUI.UIService
{
	internal class TrayManager
	{
		public NotifyIcon TrayIcon { get; }

		public TrayManager(Icon icon)
		{
			TrayIcon = new NotifyIcon
			{
				Icon = icon,
				Visible = true,
				Text = Resources.Strings.PhoneLink,
			};

			TrayIcon.DoubleClick += (sender, e) =>
			{
				PhoneLinkUIService.WindowManagerI.ShowOneInstance(typeof(MainWindow));
			};

			ToolStripMenuItem exitMenuItem = new ToolStripMenuItem(Resources.Strings.Exit);
			exitMenuItem.Click += (sender, e) =>
			{
				System.Windows.Application.Current.Shutdown();
			};

			ContextMenuStrip contextMenu = new ContextMenuStrip();
			contextMenu.Items.Add(exitMenuItem);

			TrayIcon.ContextMenuStrip = contextMenu;
		}

		public void Cleanup()
		{
			TrayIcon.Visible = false;
		}
	}
}
