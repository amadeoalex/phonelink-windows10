﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace PhoneLinkUI.UIService
{
	internal class WindowManager
	{
		private readonly Dictionary<Type, Window> oneInstanceWindows = new Dictionary<Type, Window>();
		private readonly Dictionary<string, Window> windows = new Dictionary<string, Window>();

		/// <summary>
		/// Checks whether provided <paramref name="window"/> is currently shown to the user.
		/// It does not matter if <paramref name="window"/> was shown as on or multiple instances window.
		/// </summary>
		/// <param name="window"></param>
		/// <returns></returns>
		public bool IsShown(Window window)
		{
			return oneInstanceWindows.ContainsValue(window) || windows.ContainsValue(window);
		}

		/// <summary>
		/// Checks whether window with <paramref name="key"/> is currently shown to the user.
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		public bool IsShown(string key)
		{
			return windows.ContainsKey(key);
		}

		/// <summary>
		/// Checks whether window with <paramref name="key"/> and of <paramref name="windowType"/> is currently shown to the user.
		/// </summary>
		/// <param name="key"></param>
		/// <param name="windowType"></param>
		/// <returns></returns>
		public bool IsShown(string key, Type windowType)
		{
			return IsShown(key) && windows[key].GetType() == windowType;
		}

		/// <summary>
		/// Returns window object of type <paramref name="windowType"/>.
		/// Has to be called from UI thread.
		/// </summary>
		/// <param name="windowType"></param>
		/// <returns></returns>
		/// <exception cref="ArgumentException">If provided <paramref name="windowType"/> is not instance of <see cref="Window"/></exception>
		private Window GetWindowFromType(Type windowType)
		{
			object instance = Activator.CreateInstance(windowType);
			if (!(instance is Window))
			{
				throw new ArgumentException("Provided type is not instace of Window class!");
			}

			return (Window)instance;
		}

		/// <summary>
		/// Shows instance of <paramref name="windowType"/> and assigns ><paramref name="dataContext"/> if not null.
		/// Only one instance of window with type <paramref name="windowType"/> and key <paramref name="key"/> can be show at given time.
		/// Activates window of provided <paramref name="windowType"/> and <paramref name="key"/> if it is already visible to the user.
		/// Can be called from any thread.
		/// </summary>
		/// <param name="windowType"></param>
		/// <param name="dataContext"></param>
		/// <returns></returns>
		public Window Show(string key, Type windowType, object dataContext = null)
		{
			if (Application.Current.Dispatcher.CheckAccess())
			{
				if (windows.ContainsKey(key))
				{
					windows[key].Activate();
					return windows[key];
				}

				Window window = GetWindowFromType(windowType);
				if (dataContext != null)
					window.DataContext = dataContext;

				window.Closed += WindowClosed;
				window.Tag = key;

				windows.Add(key, window);
				window.Show();

				return window;
			}
			else
			{
				return Application.Current.Dispatcher.Invoke(() => Show(key, windowType, dataContext));
			}
		}

		private void WindowClosed(object sender, EventArgs e)
		{
			Window window = (Window)sender;
			if (windows.ContainsValue(window))
				windows.Remove((string)window.Tag);
		}

		/// <summary>
		/// Returns instance of <paramref name="windowType"/> window.
		/// Returns null if no instance of <paramref name="windowType"/> is currently visible to the user.
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		public Window GetWindow(string key)
		{
			return windows.ContainsKey(key) ? windows[key] : null;
		}

		/// <summary>
		/// Checks whether one instance of <paramref name="windowType"/> is currently shown to the user.
		/// </summary>
		/// <returns></returns>
		public bool IsOneInstanceShown(Type windowType)
		{
			return oneInstanceWindows.ContainsKey(windowType);
		}

		/// <summary>
		/// Shows instance of <paramref name="windowType"/> and assigns ><paramref name="dataContext"/> if not null.
		/// Activates window of provided <paramref name="windowType"/> if it is already visible to the user.
		/// Can be called from any thread.
		/// </summary>
		/// <param name="windowType"></param>
		/// <param name="dataContext"></param>
		/// <returns></returns>
		public Window ShowOneInstance(Type windowType, object dataContext = null)
		{
			if (Application.Current.Dispatcher.CheckAccess())
			{
				if (oneInstanceWindows.ContainsKey(windowType))
				{
					oneInstanceWindows[windowType].Activate();
					return oneInstanceWindows[windowType];
				}

				Window window = GetWindowFromType(windowType);
				if (dataContext != null)
					window.DataContext = dataContext;

				window.Closed += OneInstanceWindowClosed;

				oneInstanceWindows.Add(windowType, window);
				window.Show();

				return window;
			}
			else
			{
				return Application.Current.Dispatcher.Invoke(() => ShowOneInstance(windowType, dataContext));
			}
		}

		private void OneInstanceWindowClosed(object sender, EventArgs e)
		{
			Type windowType = sender.GetType();
			if (oneInstanceWindows.ContainsKey(windowType))
				oneInstanceWindows.Remove(windowType);
		}

		/// <summary>
		/// Returns instance of <paramref name="windowType"/> window.
		/// Returns null if no instance of <paramref name="windowType"/> is currently visible to the user.
		/// </summary>
		/// <param name="windowType"></param>
		/// <returns></returns>
		public Window GetOneInstanceWindow(Type windowType)
		{
			return oneInstanceWindows.ContainsKey(windowType) ? oneInstanceWindows[windowType] : null;
		}

		/// <summary>
		/// Closes all currently shown windows.
		/// </summary>
		public void Cleanup()
		{
			foreach (KeyValuePair<string, Window> pair in windows)
				pair.Value.Close();

			foreach (KeyValuePair<Type, Window> pair in oneInstanceWindows)
				pair.Value.Close();
		}
	}
}
