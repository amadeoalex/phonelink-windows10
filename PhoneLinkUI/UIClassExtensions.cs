﻿using System;
using System.Windows.Forms;
using System.Windows.Media;

namespace PhoneLinkUI
{
	internal static class UIClassExtensions
	{
		/// <summary>
		/// Returns integer corresponding to provided <paramref name="color"/>'s brightness.
		/// White - 255
		/// Black - 0
		/// </summary>
		/// <param name="c"></param>
		/// <returns></returns>
		public static int GetBrightness(this Color color)
		{
			return (int)Math.Sqrt((color.R * color.R * .241) + (color.G * color.G * .691) + (color.B * color.B * .068));
		}

		/// <summary>
		/// Returns brush lightened/darkened by specified <paramref name="percentChange"/>
		/// Positive value lightens the brush, negative darkens it.
		/// </summary>
		/// <param name="brush"></param>
		/// <param name="percentChange"></param>
		/// <returns></returns>
		public static SolidColorBrush GetShadedBrush(this SolidColorBrush brush, float percentChange)
		{
			if (percentChange == 0)
				return brush;

			Color shadedColor = brush.Color.GetShadedColor(percentChange);
			return new SolidColorBrush(shadedColor);
		}

		public static Color GetShadedColor(this Color color, float percentChange)
		{
			if (percentChange == 0)
				return color;

			float change = Math.Abs(percentChange);
			HLSColor hlsColor = new HLSColor(color);
			if (percentChange > 0)
				hlsColor.Lighten(change);
			else if (percentChange < 0)
				hlsColor.Darken(change);

			return hlsColor.ToRGBColor();
		}

		public static Color GetTransparent(this Color color, byte transparency)
		{
			return Color.FromArgb(transparency, color.R, color.G, color.B);
		}

		public static Color ToMediaColor(this Windows.UI.Color color)
		{
			return Color.FromArgb(color.A, color.R, color.G, color.B);
		}
	}
}
