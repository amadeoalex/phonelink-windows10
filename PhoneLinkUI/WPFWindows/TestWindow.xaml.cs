﻿using PhoneLinkBase;
using PhoneLinkUI.Controls;
using PhoneLinkUI.DesignViewModelsNS;
using PhoneLinkUI.Styles;
using PhoneLinkUI.UserControls;
using PhoneLinkUI.ViewModels.Windows.MainWindow;
using PhoneLinkUI.ViewModels.Windows.TestWindow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PhoneLinkUI.WPFWindows
{
	/// <summary>
	/// Interaction logic for TestWindow.xaml
	/// </summary>
	public partial class TestWindow : Window
	{
		public TestWindow()
		{
			InitializeComponent();
			DataContext = new TestWindowViewModel();
		}
	}
}
