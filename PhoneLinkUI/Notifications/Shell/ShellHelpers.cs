﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace PhoneLinkUI.Notifications.Shell
{
	[ComImport,
	Guid("000214F9-0000-0000-C000-000000000046"),
	InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface IShellLinkW
	{
		UInt32 GetPath(
			[Out(), MarshalAs(UnmanagedType.LPWStr)] StringBuilder pszFile,
			int cchMaxPath,
			//ref _WIN32_FIND_DATAW pfd,
			IntPtr pfd,
			uint fFlags);
		UInt32 GetIDList(out IntPtr ppidl);
		UInt32 SetIDList(IntPtr pidl);
		UInt32 GetDescription(
			[Out(), MarshalAs(UnmanagedType.LPWStr)] StringBuilder pszFile,
			int cchMaxName);
		UInt32 SetDescription(
			[MarshalAs(UnmanagedType.LPWStr)] string pszName);
		UInt32 GetWorkingDirectory(
			[Out(), MarshalAs(UnmanagedType.LPWStr)] StringBuilder pszDir,
			int cchMaxPath
			);
		UInt32 SetWorkingDirectory(
			[MarshalAs(UnmanagedType.LPWStr)] string pszDir);
		UInt32 GetArguments(
			[Out(), MarshalAs(UnmanagedType.LPWStr)] StringBuilder pszArgs,
			int cchMaxPath);
		UInt32 SetArguments(
			[MarshalAs(UnmanagedType.LPWStr)] string pszArgs);
		UInt32 GetHotKey(out short wHotKey);
		UInt32 SetHotKey(short wHotKey);
		UInt32 GetShowCmd(out uint iShowCmd);
		UInt32 SetShowCmd(uint iShowCmd);
		UInt32 GetIconLocation(
			[Out(), MarshalAs(UnmanagedType.LPWStr)] out StringBuilder pszIconPath,
			int cchIconPath,
			out int iIcon);
		UInt32 SetIconLocation(
			[MarshalAs(UnmanagedType.LPWStr)] string pszIconPath,
			int iIcon);
		UInt32 SetRelativePath(
			[MarshalAs(UnmanagedType.LPWStr)] string pszPathRel,
			uint dwReserved);
		UInt32 Resolve(IntPtr hwnd, uint fFlags);
		UInt32 SetPath(
			[MarshalAs(UnmanagedType.LPWStr)] string pszFile);
	}

	[ComImport,
	Guid("0000010b-0000-0000-C000-000000000046"),
	InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface IPersistFile
	{
		void GetCurFile(
			[Out(), MarshalAs(UnmanagedType.LPWStr)] StringBuilder pszFile
		);
		void IsDirty();
		void Load(
			[MarshalAs(UnmanagedType.LPWStr)] string pszFileName,
			[MarshalAs(UnmanagedType.U4)] long dwMode);
		void Save(
			[MarshalAs(UnmanagedType.LPWStr)] string pszFileName,
			bool fRemember);
		void SaveCompleted(
			[MarshalAs(UnmanagedType.LPWStr)] string pszFileName);
	}

	[ComImport,
	Guid("886D8EEB-8CF2-4446-8D02-CDBA1DBDCF99"),
	InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface IPropertyStore
	{
		void GetCount([Out] out uint propertyCount);
		void GetAt([In] uint propertyIndex, [Out, MarshalAs(UnmanagedType.Struct)] out PROPERTYKEY key);
		void GetValue([In, MarshalAs(UnmanagedType.Struct)] ref PROPERTYKEY key, [Out, MarshalAs(UnmanagedType.Struct)] out PROPVARIANT pv);
		void SetValue([In, MarshalAs(UnmanagedType.Struct)] ref PROPERTYKEY key, [In, MarshalAs(UnmanagedType.Struct)] ref PROPVARIANT pv);
		void Commit();
	}

	[ComImport,
	Guid("00021401-0000-0000-C000-000000000046"),
	ClassInterface(ClassInterfaceType.None)]
	public class CShellLink { }

	[StructLayout(LayoutKind.Explicit)]
	public struct PROPVARIANT : IDisposable
	{
		[FieldOffset(0)]
		public ushort vt;
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2111:PointersShouldNotBeVisible")]
		[FieldOffset(8)]
		public IntPtr unionmember;
		[FieldOffset(8)]
		public UInt64 forceStructToLargeEnoughSize;

		public void Dispose()
		{
			unionmember = IntPtr.Zero;
		}
	}

	public static class ErrorHelper
	{
		public static void VerifySucceeded(UInt32 hresult)
		{
			if (hresult > 1)
			{
				throw new Exception("Failed with HRESULT: " + hresult.ToString("X"));
			}
		}
	}
}