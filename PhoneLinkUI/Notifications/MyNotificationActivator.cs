﻿using Microsoft.QueryStringDotNET;
using PhoneLinkBase.Service;
using PhoneLinkUI.UIService;
using System;
using System.Runtime.InteropServices;

namespace PhoneLinkUI.Notifications
{
	[ClassInterface(ClassInterfaceType.None)]
	[ComSourceInterfaces(typeof(INotificationActivationCallback))]
	[Guid("876afc38-eb32-424c-b3f9-379ef44b7438"), ComVisible(true)]
	public class MyNotificationActivator : NotificationActivator
	{
		public override void OnActivated(string arguments, NotificationUserInput userInput, string appUserModelId)
		{
			Console.WriteLine($"ia: {arguments} | ui: {userInput}");
			QueryString parsedArguments = QueryString.Parse(arguments);
			PhoneLinkUIService.FeatureUIManagerI.Interaction(parsedArguments, userInput);
		}
	}
}
