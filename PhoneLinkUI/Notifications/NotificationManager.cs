﻿using PhoneLinkBase;
using PhoneLinkUI.Notifications.Shell;
using PhoneLinkUI.UIService;
using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace PhoneLinkUI.Notifications
{
	public class NotificationManager
	{
		private const string TAG = "Notification Manager";

		public static readonly string APPLICATION_AUMID = "Amadeo.PhoneLink";
		public static readonly string APPLICATION_CLSID = "876afc38-eb32-424c-b3f9-379ef44b7438";

		public NotificationManager()
		{
			CheckAndInstallShortcut();

			DesktopNotificationManagerCompat.RegisterAumidAndComServer<MyNotificationActivator>(APPLICATION_AUMID);
			DesktopNotificationManagerCompat.RegisterActivator<MyNotificationActivator>();
		}

		/// <summary>
		/// Checks if program shortcut is already present and performs installation if not.
		/// </summary>
		private void CheckAndInstallShortcut()
		{
			//Check if shortcut exists
			string shortcutPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Microsoft\\Windows\\Start Menu\\Programs\\Phone Link.lnk";
			if (!File.Exists(shortcutPath))
			{
				//Find current exe path
				string exePath = Process.GetCurrentProcess().MainModule.FileName;

				//Install shortcut
				InstallShortcut(shortcutPath, exePath);
			}
		}

		/// <summary>
		/// Performs shortcut installation
		/// </summary>
		/// <param name="shorctutPath">desired shortcut path</param>
		/// <param name="exePath">current executable path</param>
		private void InstallShortcut(string shorctutPath, string exePath)
		{
			//Create shortcut
			IShellLinkW shortcut = (IShellLinkW)new CShellLink();

			//Set shortcut exe path
			ErrorHelper.VerifySucceeded(shortcut.SetPath(exePath));

			//Open shortcut property store
			IPropertyStore shortcutProperties = (IPropertyStore)shortcut;

			//Set AppUserModelUd property
			using (PropVariantHelper propertyAMID = new PropVariantHelper())
			{
				propertyAMID.SetValue(APPLICATION_AUMID);
				shortcutProperties.SetValue(PROPERTYKEY.AppUserModel_ID, propertyAMID.Propvariant);
			}


			//Set toast activator CLSID property
			using (PropVariantHelper propertyCLSID = new PropVariantHelper())
			{
				propertyCLSID.VarType = VarEnum.VT_CLSID;
				propertyCLSID.SetValue(new Guid(APPLICATION_CLSID));
				shortcutProperties.SetValue(PROPERTYKEY.AppUserModel_ToastActivatorCLSID, propertyCLSID.Propvariant);
			}

			//Save shortcut to disk
			IPersistFile shortcutFile = (IPersistFile)shortcut;
			shortcutFile.Save(shorctutPath, true);
		}
	}

}
