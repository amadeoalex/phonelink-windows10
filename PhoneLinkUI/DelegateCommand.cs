﻿using System;
using System.Windows.Input;

namespace PhoneLinkUI
{
	public class DelegateCommand : ICommand
	{
		public event EventHandler CanExecuteChanged { add { } remove { } }


		public readonly Action<object> action;

		public DelegateCommand(Action<object> action)
		{
			this.action = action;
		}

		public bool CanExecute(object parameter)
		{
			return true;
		}

		public void Execute(object parameter)
		{
			action?.Invoke(parameter);
		}
	}
}
