﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;

namespace PhoneLinkUI.DesignViewModelsNS
{
	class PairingPageViewModel
	{
		public string Title { get; set; } = "Pairing";

		public List<DeviceViewModel> Devices { get; set; }

		public PairingPageViewModel()
		{
			Devices = new List<DeviceViewModel>
			{
				new DeviceViewModel("id1", "Device Name 1"),
				new DeviceViewModel("id2", "Device Name 2"),
				new DeviceViewModel("id3", "Device Name 3"),
				new DeviceViewModel("id4", "Device Name 4"),
				new DeviceViewModel("id5", "Device Name 5")
			};
		}

		public class DeviceViewModel
		{
			public string Name { get; set; }
			public string Id { get; set; }
			public string PairingStatus { get; set; } = "Pairing Status";

			public DeviceViewModel(string id, string name)
			{
				Name = name;
				Id = id;
			}
		}
	}
}
