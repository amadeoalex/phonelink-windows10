﻿using PhoneLinkUI.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;
using Windows.Services.Maps;
using static PhoneLinkUI.Controls.NavigationMenu;

namespace PhoneLinkUI.DesignViewModelsNS
{
	public class DesignMenuItem
	{
		public string Title { get; set; }
		public string IconChar { get; set; }
		public string Tag { get; set; }
		public bool IsSelected { get; set; }

		public List<DesignMenuItem> InnerItems { get; set; }
	}

	class NavigationMenuDesignViewModel
	{
		public DesignMenuItem CurrentMenu { get; } = new DesignMenuItem()
		{
			Title = "Design Menu",
			InnerItems = new List<DesignMenuItem>
			{
				new DesignMenuItem {Title="title10", IconChar="\uE713", Tag="tag1", IsSelected=true},
				new DesignMenuItem {Title="Ekran", IconChar="\uE7F4", Tag="tag2"},
				new DesignMenuItem {Title="title30", IconChar="\uE713", Tag="tag3",
					InnerItems = new List<DesignMenuItem>
					{
						new DesignMenuItem { Title="internal 10", IconChar="\uE713", Tag="internalx" },
						new DesignMenuItem { Title="internal 20", IconChar="\uE713", Tag="internaly" },
						new DesignMenuItem { Title="internal 30", IconChar="\uE713", Tag="internalz",
							InnerItems = new List<DesignMenuItem>
							{
								new DesignMenuItem { Title="internal 110", IconChar="\uE713", Tag="internalx2" },
								new DesignMenuItem { Title="internal 120", IconChar="\uE713", Tag="internaly2" },
								new DesignMenuItem { Title="internal 130", IconChar="\uE713", Tag="internalz2 "},
							}
						}
					}
				},
				new DesignMenuItem{Title="title4", IconChar="\uE713", Tag="tag4"}
			}
		};

		public DelegateCommand ItemClickedCommand { get; set; }
	}
}
