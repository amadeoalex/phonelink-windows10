﻿using PhoneLinkBase;
using DeviceNS;
using DeviceNS.Types;
using PhoneLinkUI.Pages;
using PhoneLinkUI.ViewModels.Windows.LogWindow;
using PhoneLinkUI.ViewModels.Windows.MainWindow;
using System.Collections.ObjectModel;
using System.Windows.Controls;
using System.Windows.Media;
using PhoneLinkBase.Debug;
using PhoneLinkUI.DesignViewModelsNS;
using PhoneLinkUI.Controls;
using System.Collections.Generic;

namespace PhoneLinkUI.DesignViewModelsNS
{
	internal static class DesignViewModels
	{
		public static NavigationMenuDesignViewModel NavigationMenuViewModel { get; } = new NavigationMenuDesignViewModel();
		public static DevicesPageViewModel DevicesPageViewModel { get; } = new DevicesPageViewModel();
		public static PairingPageViewModel PairingPageViewModel { get; } = new PairingPageViewModel();
	}

}

