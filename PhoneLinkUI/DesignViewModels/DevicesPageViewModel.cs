﻿using DeviceNS;
using DeviceNS.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Windows.Documents;
using static PhoneLinkUI.ViewModels.Pages.DevicesPageViewModel;

namespace PhoneLinkUI.DesignViewModelsNS
{
	class DevicesPageViewModel
	{
		public string Title { get; set; } = "Devices";
		public List<DeviceViewModel> Devices { get; set; }

		public DevicesPageViewModel()
		{
			Devices = new List<DeviceViewModel>
			{
				new DeviceViewModel(new Phone("dp1", "Design Phone 1", new byte[0], "1.2.3.4", 1, 2)),
				new DeviceViewModel(new Phone("dp2", "Design Phone 2", new byte[0], "2.3.4.5", 3, 4)),
				new DeviceViewModel(new Phone("dp3", "Design Phone 3", new byte[0], "6.7.8.9", 5, 6)),
			};
		}


	}
}
