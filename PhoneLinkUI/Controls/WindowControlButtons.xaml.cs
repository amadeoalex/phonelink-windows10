﻿using PhoneLinkUI.MarkupExtensions;
using PhoneLinkUI.Styles;
using PhoneLinkUI.ValueConverters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PhoneLinkUI.Controls
{
	/// <summary>
	/// Interaction logic for WindowControlButtons.xaml
	/// </summary>
	public partial class WindowControlButtons : UserControl
	{
		public static readonly DependencyProperty OnMinimizeProperty = DependencyProperty.Register(UIUtils.GetDependencyPropertyName(nameof(OnMinimizeProperty)), typeof(ICommand), typeof(WindowControlButtons));
		public static readonly DependencyProperty OnMaximizeProperty = DependencyProperty.Register(UIUtils.GetDependencyPropertyName(nameof(OnMaximizeProperty)), typeof(ICommand), typeof(WindowControlButtons));
		public static readonly DependencyProperty OnCloseProperty = DependencyProperty.Register(UIUtils.GetDependencyPropertyName(nameof(OnCloseProperty)), typeof(ICommand), typeof(WindowControlButtons));

		public static readonly DependencyProperty BackgroundColorBrushProperty = DependencyProperty.Register(UIUtils.GetDependencyPropertyName(nameof(BackgroundColorBrushProperty)), typeof(Brush), typeof(WindowControlButtons));

		public ICommand OnMinimize
		{
			get { return (ICommand)GetValue(OnMinimizeProperty); }
			set { SetValue(OnMinimizeProperty, value); }
		}

		public ICommand OnMaximize
		{
			get { return (ICommand)GetValue(OnMaximizeProperty); }
			set { SetValue(OnMaximizeProperty, value); }
		}

		public ICommand OnClose
		{
			get { return (ICommand)GetValue(OnCloseProperty); }
			set { SetValue(OnCloseProperty, value); }
		}

		public Brush BackgroundColorBrush
		{
			get { return (SolidColorBrush)GetValue(BackgroundColorBrushProperty); }
			set { SetValue(BackgroundColorBrushProperty, value); }
		}

		public WindowControlButtons()
		{
			InitializeComponent();
			DataContext=this;

			OnMinimize = new DelegateCommand((paramaeter) =>
			{
				Window window = (Window)paramaeter;
				window.WindowState = WindowState.Minimized;
			});

			OnMaximize = new DelegateCommand((paramaeter) =>
			{
				Window window = (Window)paramaeter;
				window.WindowState = window.WindowState == WindowState.Maximized ? WindowState.Normal : WindowState.Maximized;
			});

			OnClose = new DelegateCommand((paramaeter) =>
			{
				Window window = (Window)paramaeter;
				window.Close();
			});
		}
	}
}
