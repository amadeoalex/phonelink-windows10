﻿using PhoneLinkUI.Styles;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PhoneLinkUI.Controls
{
	/// <summary>
	/// Interaction logic for WindowRoot.xaml
	/// </summary>
	[ContentProperty(nameof(WindowContent))]
	public partial class WindowRoot : UserControl
	{
		public static readonly DependencyProperty WindowControlButtonsProperty = DependencyProperty.Register(
			UIUtils.GetDependencyPropertyName(nameof(WindowControlButtonsProperty)),
			typeof(WindowControlButtons), typeof(WindowRoot));

		public static readonly DependencyProperty WindowContentProperty = DependencyProperty.Register(
			UIUtils.GetDependencyPropertyName(nameof(WindowContentProperty)),
			typeof(object), typeof(WindowRoot));

		public static readonly DependencyProperty TitleBarBrushProperty = DependencyProperty.Register(
			UIUtils.GetDependencyPropertyName(nameof(TitleBarBrushProperty)),
			typeof(Brush), typeof(WindowRoot));

		public WindowControlButtons WindowControlButtons
		{
			get { return (WindowControlButtons)GetValue(WindowControlButtonsProperty); }
			set { SetValue(WindowControlButtonsProperty, value); }
		}

		public object WindowContent
		{
			get { return GetValue(WindowContentProperty); }
			set { SetValue(WindowContentProperty, value); }
		}

		public Brush TitleBarBrush
		{
			get { return (Brush)GetValue(TitleBarBrushProperty); }
			set { SetValue(TitleBarBrushProperty, value); }
		}

		public WindowRoot()
		{
			InitializeComponent();
			TitleBarBrush = ThemeResources.StatusBarBrush;
		}
	}
}
