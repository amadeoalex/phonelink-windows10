﻿using PhoneLinkBase;
using DeviceNS;
using DeviceNS.Types;
using PhoneLinkUI.Pages;
using PhoneLinkUI.ViewModels.Windows.LogWindow;
using PhoneLinkUI.ViewModels.Windows.MainWindow;
using System.Collections.ObjectModel;
using System.Windows.Controls;
using System.Windows.Media;
using PhoneLinkBase.Debug;
using PhoneLinkUI.DesignViewModelsNS;

namespace PhoneLinkUI
{
	internal static class DesignViewModels
	{
		public static NavigationMenuDesignViewModel MainMenuViewModel { get; } = new NavigationMenuDesignViewModel();

		public static MainWindowDesignViewModel MainWindowViewModel { get; } = new MainWindowDesignViewModel { MainMenuViewModel = MainMenuViewModel };

		//public static PairingPageDesignViewModel PairingPageViewModel { get; } = new PairingPageDesignViewModel();

		//public static HomePageDesignViewModel HomePageViewModel { get; } = new HomePageDesignViewModel();

		//public static ChatWindowViewModel ChatWindowViewModel => new ChatWindowDesignViewModel();

		//public static MessageBubbleViewModel MessageBubbleViewModel => new MessageBubbleViewModel
		//{
		//	Message = new Message("", "asdasdasfa asdas asd \n asd asd 12 3 :<", "3333333333"),
		//	SentBrush = (SolidColorBrush)new BrushConverter().ConvertFrom("#E7B50F"),
		//	ReceivedBrush = (SolidColorBrush)new BrushConverter().ConvertFrom("#26A88F")
		//};

		public static LogWindowDesignViewModel LogWindowViewModel { get; } = new LogWindowDesignViewModel();
	}

	//internal class ChatWindowDesignViewModel : ChatWindowViewModel
	//{
	//	public ChatWindowDesignViewModel()
	//	{
	//		WindowTitleVisible = true;

	//		SentBrush = (SolidColorBrush)new BrushConverter().ConvertFrom(DEFAULT_SENT_COLOR);
	//		ReceivedBrush = (SolidColorBrush)new BrushConverter().ConvertFrom(DEFAULT_RECEIVED_COLOR);
	//		InvalidatedBrush = (SolidColorBrush)new BrushConverter().ConvertFrom(DEFAULT_INVALIDATED_COLOR);

	//		Conversation = new Conversation("123456")
	//		{
	//			Number = "123456",
	//			Name = "Design chat contact",
	//			Messages = new ObservableCollection<Message>()
	//			{
	//				new Message("123456", "asdasdasfa", "3333"),
	//				new Message("123456", "ewtwetwery", "3333"),
	//				new Message(null, "kghkghjklhjkl", "3333"),
	//				new Message("123456", "bnmbnmbmnbv", "3333"),
	//				new Message(null, "zxczxczxcz", "3333"),
	//				new Message(null, "qweqweqwrqr", "3333"),
	//				new Message(null, "tyutyityityi", "3333"),
	//			},
	//		};

	//		Messages = new ObservableCollection<MessageBubbleViewModel>
	//		{
	//			new MessageBubbleViewModel{ Message = new Message("123456", "asdasdasfa asdasd asd asdasd asda sda s 1", "3333"), SentBrush=SentBrush, ReceivedBrush=ReceivedBrush, InvalidatedBrush=InvalidatedBrush },
	//			new MessageBubbleViewModel{ Message = new Message("123456", "ewtwetwery 2", "3333"), SentBrush=SentBrush, ReceivedBrush=ReceivedBrush, InvalidatedBrush=InvalidatedBrush  },
	//			new MessageBubbleViewModel{ Message = new Message(null, "kghkghjklhjkl asd asdas as asdas as asd asd 3", "3333"), SentBrush=SentBrush, ReceivedBrush=ReceivedBrush, InvalidatedBrush=InvalidatedBrush  },
	//			new MessageBubbleViewModel{ Message = new Message("123456", "bnmbnmbm asdas d asda s as as das das nbv 4", "3333"), SentBrush=SentBrush, ReceivedBrush=ReceivedBrush, InvalidatedBrush=InvalidatedBrush  },
	//			new MessageBubbleViewModel{ Message = new Message(null, "zxczxczxcz 5", "3333"), SentBrush=SentBrush, ReceivedBrush=ReceivedBrush, InvalidatedBrush=InvalidatedBrush  },
	//			new MessageBubbleViewModel{ Message = new Message(null, "qweqweqwras das asd asda sdas dasda sd s qr 6", "3333"), SentBrush=SentBrush, ReceivedBrush=ReceivedBrush, InvalidatedBrush=InvalidatedBrush  },
	//			new MessageBubbleViewModel{ Message = new Message(null, "tyutyityityi 7", "3333"), SentBrush=SentBrush, ReceivedBrush=ReceivedBrush, InvalidatedBrush=InvalidatedBrush  },
	//		};
	//	}
	//}

	internal class LogWindowDesignViewModel
	{
		public ObservableCollection<LogEntryViewModel> ItemsView { get; } = new ObservableCollection<LogEntryViewModel>()
		{
			new LogEntryViewModel (new LogEntry(LogLevel.Debug, "Debug Tag 1", "debug log 1")),
			new LogEntryViewModel (new LogEntry(LogLevel.Debug, "Debug Tag 1", "debug log 2")),
			new LogEntryViewModel (new LogEntry(LogLevel.Info, "Info Tag 2", "info log 3")),
			new LogEntryViewModel (new LogEntry(LogLevel.Info, "Info Tag 2", "info log 4")),
			new LogEntryViewModel (new LogEntry(LogLevel.Warning, "Warning Tag 3", "earning log 5")),
			new LogEntryViewModel (new LogEntry(LogLevel.Error, "Error Tag 3", "error log 6")),
			new LogEntryViewModel (new LogEntry(LogLevel.Verbose, "Verbose Tag 3", "verbose log 7")),
		};
	}

	internal class MainWindowDesignViewModel
	{
		public NavigationMenuDesignViewModel MainMenuViewModel { get; set; }
	}
}

