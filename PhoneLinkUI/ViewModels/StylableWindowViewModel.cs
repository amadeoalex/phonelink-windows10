﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Input;

namespace PhoneLinkUI.ViewModels
{
	internal class StyleableWindowViewModel : INotifyPropertyChanged
	{
		#region Property changed
#pragma warning disable 67
		public event PropertyChangedEventHandler PropertyChanged;
#pragma warning restore 67
		#endregion

		public bool WindowTitleVisible { get; set; }

		public bool WindowControlMinimizeEnabled { get; set; } = true;
		public bool WindowControlMaximizeEnabled { get; set; } = true;
		public bool WindowControlCloseEnabled { get; set; } = true;

		public ICommand WindowControlMinimizeCommand { get; } = new DelegateCommand((paramaeter) =>
		 {
			 Window window = (Window)paramaeter;
			 window.WindowState = WindowState.Minimized;
		 });

		public ICommand WindowControlMaximizeCommand { get; } = new DelegateCommand((paramaeter) =>
		{
			Window window = (Window)paramaeter;
			window.WindowState = window.WindowState == WindowState.Maximized ? WindowState.Normal : WindowState.Maximized;
		});

		public ICommand WindowControlCloseCommand { get; } = new DelegateCommand((paramaeter) =>
		{
			Window window = (Window)paramaeter;
			window.Close();
		});

	}
}
