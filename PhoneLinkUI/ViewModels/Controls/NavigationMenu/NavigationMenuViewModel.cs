﻿using PhoneLinkBase;
using PhoneLinkUI.Pages;
using PhoneLinkUI.UserControls;
using PropertyChanged;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using Windows.UI.Xaml;

namespace PhoneLinkUI.ViewModels.Controls.NavigationMenu
{
	public class NavigationMenuItem : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		public string Title { get; set; }
		public string IconChar { get; set; }
		public string Tag { get; set; }
		public bool IsSelected { get; set; }

		public Page Page { get; set; }

		public List<NavigationMenuItem> InnerItems { get; set; }
		public NavigationMenuItem SelectedItem
		{
			get
			{
				NavigationMenuItem selectedItem = InnerItems.FirstOrDefault(item => item.IsSelected);
				if (selectedItem == null && InnerItems?.Count > 0)
				{

					InnerItems[0].IsSelected = true;
					selectedItem = InnerItems[0];
				}

				return selectedItem;
			}
		}
	}

	public enum NavigationMenuStyle
	{
		Standard,
		Compact
	}

	class NavigationMenuViewModel : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		private const string TAG = "NavigationMenuVM";

		public Stack<NavigationMenuItem> MenuHistory { get; set; } = new Stack<NavigationMenuItem>();

		public NavigationMenuItem CurrentMenu { get; set; }
		public Page CurrentPage { get; set; }

		public DelegateCommand ItemClickedCommand { get; set; }
		public DelegateCommand BackClickedCommand { get; set; }

		//public delegate void OnItemClickDelegate(string tag);
		//public OnItemClickDelegate OnItemClick { get; set; }

		public NavigationMenuStyle MenuStyle { get; set; }

		public NavigationMenuViewModel()
		{
			ItemClickedCommand = new DelegateCommand((menuItemObject) =>
			{
				NavigationMenuItem menuItem = (NavigationMenuItem)menuItemObject;
				//Log.D(TAG, $"item {menuItem.Tag} clicked");

				if (CurrentMenu == null)
				{
					//Log.D(TAG, "Current menu is null");
					return;
				}

				if (CurrentMenu.SelectedItem == menuItem && (menuItem.InnerItems == null || menuItem.InnerItems.Count == 0))
				{
					//Log.D(TAG, $"menu item {menuItem.Tag} is already selected and does not have inner items");
					return;
				}

				CurrentMenu.InnerItems.ForEach(item => item.IsSelected = false);
				menuItem.IsSelected = true;


				if (menuItem.InnerItems?.Count > 0)
				{
					MenuHistory.Push(CurrentMenu);
					CurrentMenu = menuItem;
				}
				else
					CurrentPage = CurrentMenu.SelectedItem.Page;
			});

			BackClickedCommand = new DelegateCommand((_) =>
			{
				if (MenuHistory.Count == 0)
				{
					//Log.D(TAG, "no menu history to backtrack");
					return;
				}

				CurrentMenu = MenuHistory.Pop();
			});
		}

		public void OnCurrentMenuChanged()
		{
			if (CurrentMenu.SelectedItem.Page != null)
				CurrentPage = CurrentMenu.SelectedItem.Page;
		}
	}
}
