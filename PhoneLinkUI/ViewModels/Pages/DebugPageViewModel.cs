﻿using PhoneLinkBase;
using DeviceNS;
using PhoneLinkBase.Service;
using PhoneLinkBase.Service.Network;
using PhoneLinkUI;
using PhoneLinkUI.UIService;
using PhoneLinkUI.ViewModels.Windows;
using System.Collections.Generic;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using PhoneLinkUI.WPFWindows;

namespace PhoneLink.ViewModels.Pages
{
	internal class DebugPageViewModel
	{
		private const string TAG = "Debug Page";

		public ICommand CommandSendTestNetPackage { get; } = new DelegateCommand((_) =>
		{
			NetPackage netPackage = new NetPackage("test_package");
			DeviceManagerHelper.SendNetPackageToAll(netPackage);
		});

		public ICommand CommandRemovePairedDevices { get; } = new DelegateCommand((_) =>
		{
			List<Device> devices = new List<Device>(PhoneLinkService.DeviceManagerI.Devices);

			foreach (Device device in devices)
			{
				PhoneLinkService.DeviceManagerI.Remove(device);
			}
		});

		public ICommand CommandShowLogWindow { get; } = new DelegateCommand((_) => PhoneLinkUIService.WindowManagerI.ShowOneInstance(typeof(LogWindow)));

		public ICommand CommandShowTestWindow { get; } = new DelegateCommand((_) => PhoneLinkUIService.WindowManagerI.ShowOneInstance(typeof(TestWindow)));

		public ICommand CommandShowColorsWindow { get; } = new DelegateCommand((_) => PhoneLinkUIService.WindowManagerI.ShowOneInstance(typeof(PhoneLinkUI.CustomWindows.ColorTester)));

		public ICommand CommandListDevices { get;} = new DelegateCommand((_) =>
		{
			List<Device> devices = new List<Device>(PhoneLinkService.DeviceManagerI.Devices);

			foreach (Device device in devices)
			{
				Log.I(TAG, device.Id);
			}
		});

		//public ICommand CommandAddMessages { get; } = new DelegateCommand((_) =>
		//{
		//	Conversation c = ConversationManager.Instance.GetConversation("123456");
		//	//JsonArray messages = new JsonArray {
		//	//	new Message("123456", "asdadfaf", "123455"),
		//	//	new Message("123456", "asdasgf32512356", "123455"),
		//	//	new Message(null, "myuhjljhk", "123455"),
		//	//	new Message(null, "xcvxcbxcvnx", "123455"),
		//	//	new Message("123456", "XXXXXXXXXXXXXXX", "123455"),
		//	//};
		//	//c.AddMessages(messages);

		//	for (int i = 0; i < 1024; i++)
		//	{
		//		Message m = new Message("123456", "nah", "3578458");
		//		Message m2 = new Message(null, "nah", "3578458");
		//		c.AddMessage(m);
		//		c.AddMessage(m2);
		//	}
		//});

		//public ICommand CommandViewChat { get; } = new DelegateCommand((_) =>
		//{
		//	ChatWindowViewModel dataContext = new ChatWindowViewModel("123456");
		//	PhoneLinkUIService.WindowManagerI.ShowOneInstance(typeof(ChatWindow), dataContext);
		//});

		//public ICommand CommandWindowFromThread { get; } = new DelegateCommand((_) =>
		//{
		//	new Thread(() =>
		//	{
		//		ChatWindowViewModel dataContext = new ChatWindowViewModel("123456");
		//		Window w = PhoneLinkUIService.WindowManagerI.ShowOneInstance(typeof(ChatWindow), dataContext);
		//	}).Start();
		//});


	}
}
