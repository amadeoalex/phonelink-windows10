﻿using DeviceNS;
using PhoneLinkBase;
using PhoneLinkBase.Service;
using PhoneLinkBase.UtilClasses;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneLinkUI.ViewModels.Pages
{
	class DevicesPageViewModel
	{
		public string Title { get; set; } = "Devices";
		public ViewModelObservableCollection<DeviceViewModel, Device> Devices { get; set; }

		public DelegateCommand ItemClicked { get; set; }

		public DevicesPageViewModel()
		{
			Devices = new ViewModelObservableCollection<DeviceViewModel, Device>(PhoneLinkService.DeviceManagerI.Devices, (modelItem) => new DeviceViewModel(modelItem));

			ItemClicked = new DelegateCommand((vm) =>
			{
				DeviceViewModel deviceViewModel = (DeviceViewModel)vm;
				Log.D("Devices Page VM", $"device {deviceViewModel.Id} clicked");
			});
		}

		public class DeviceViewModel : IViewModelBase<Device>
		{
			public Device Model { get; set; }
			public string Id { get { return Model.Id; } }
			public string Name { get { return Model.Name; } }
			public string Type { get { return Model.Type; } }

			public DeviceViewModel(Device device)
			{
				Model = device;
			}
		}
	}
}
