﻿using PhoneLinkBase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Json;
using System.Linq;

namespace PhoneLinkUI.ViewModels.Pages
{
	internal class UISettingsPageViewModel : INotifyPropertyChanged
	{
		#region Property changed
#pragma warning disable 67
		public event PropertyChangedEventHandler PropertyChanged;
#pragma warning restore 67
		#endregion

		private readonly JsonObject preferences;

		private readonly Dictionary<ThemeProvider.ColorSchemes, string> colorSchemes;

		public List<string> ColorSchemeItems { get => colorSchemes.Values.ToList(); }
		public string ColorSchemeSelectedItem
		{
			get
			{
				string schemeString = preferences.OptString(ThemeProvider.SETTING_KEY_COLOR_SCHEME, ThemeProvider.SETTING_KEY_COLOR_SCHEME_DEFAULT);
				return colorSchemes[(ThemeProvider.ColorSchemes)Enum.Parse(typeof(ThemeProvider.ColorSchemes), schemeString)];
			}
			set
			{
				ThemeProvider.ColorSchemes colorScheme = colorSchemes.First(x => x.Value == value).Key;
				preferences[ThemeProvider.SETTING_KEY_COLOR_SCHEME] = colorScheme.ToString("g");
			}
		}

		public UISettingsPageViewModel()
		{
			preferences = PreferenceManager.GetObjectPreference(ThemeProvider.KEY_THEME_SETTINGS);

			colorSchemes = new Dictionary<ThemeProvider.ColorSchemes, string>();
			foreach (ThemeProvider.ColorSchemes scheme in Enum.GetValues(typeof(ThemeProvider.ColorSchemes)))
			{
				string resourceName = $"{scheme.ToString("g")}ColorScheme";
				colorSchemes.Add(scheme, Resources.Strings.ResourceManager.GetString(resourceName));
			}
		}
	}
}
