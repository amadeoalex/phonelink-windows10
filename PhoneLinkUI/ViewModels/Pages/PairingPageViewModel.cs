﻿using DeviceNS;
using PhoneLinkBase.DeviceNS.Connection.Pairing;
using PhoneLinkBase.Service;
using PhoneLinkBase.UtilClasses;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneLinkUI.ViewModels.Pages
{
	class PairingPageViewModel
	{
		public string Title { get; set; } = "Pairing";

		public ViewModelObservableCollection<DeviceViewModel, Device> Devices { get; set; }

		public DelegateCommand RefreshCommand { get; set;} = new DelegateCommand((_) => PhoneLinkService.DeviceManagerI.DeviceDiscovery.StartScan());

		public PairingPageViewModel()
		{
			Devices = new ViewModelObservableCollection<DeviceViewModel, Device>(PhoneLinkService.DeviceManagerI.DeviceDiscovery.FoundDevices, (modelItem) => new DeviceViewModel(modelItem));
		}

		public class DeviceViewModel : IViewModelBase<Device>
		{
			public Device Model { get; set; }
			public string Id { get { return Model.Id; } }
			public string Name { get { return string.IsNullOrEmpty(Model.Name) ? Model.Id : Model.Name; } }
			public string Type { get { return Model.Type; } }

			public string PairingStatus { get; set; }
			public PairingProcess PairingProcess { get; set; }

			public DelegateCommand ItemClickedCommand { get; set; }

			public DeviceViewModel(Device device)
			{
				Model = device;

				ItemClickedCommand = new DelegateCommand((_) =>
				{
					if (PhoneLinkService.DeviceManagerI.IsDeviceWithIdPaired(Model.Id))
					{
						PairingStatus = "Device is already paired";
						return;
					}

					PairingProcess = PhoneLinkService.PairingManagerI.GetPairingProcess(Model, false);
					if (!PairingProcess.IsAlive)
					{
						PairingProcess.OnUpdate = (status) =>
						{
							string statusText = "x";
							switch (status)
							{
								case PairingProcess.Status.Initializing:
									statusText = "Initializing";
									break;
								case PairingProcess.Status.WaitingForAccept:
									statusText = "Waiting for accept";
									break;
								case PairingProcess.Status.Pairing:
									statusText = "Pairing";
									break;
								case PairingProcess.Status.Finished:
									statusText = "Finished";
									break;
								case PairingProcess.Status.Error:
									statusText = "Error";
									break;
							}

							PairingStatus = statusText;
						};

						PairingProcess.OnFinished = (success, pairedDevice) =>
						{
							if(success)
								PhoneLinkService.DeviceManagerI.Add(pairedDevice);
						};

						PairingProcess.Start();
					}
				});
			}
		}
	}
}
