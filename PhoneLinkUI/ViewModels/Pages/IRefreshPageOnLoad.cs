﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneLinkUI.ViewModels.Pages
{
	internal interface IRefreshPageOnLoad
	{
		void OnLoaded();
	}
}
