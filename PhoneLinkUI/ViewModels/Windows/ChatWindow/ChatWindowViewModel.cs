﻿/*using PhoneLinkBase;
using PhoneLinkBase.Service;
using PhoneLinkUI.UIService;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;

namespace PhoneLinkUI.ViewModels.Windows.ChatWindow
{
	internal class ChatWindowViewModel : StyleableWindowViewModel
	{
		private const string TAG = "Chat Window View Model";

		protected const string DEFAULT_SENT_COLOR = "#4F4E4A";
		protected const string DEFAULT_RECEIVED_COLOR = "#E7B50F";
		protected const string DEFAULT_INVALIDATED_COLOR = "#FA380B";

		public string Number { get; }
		public ICommand SendCommand { get; }
		public Conversation Conversation { get; set; }

		private readonly object collectionLockObject = new object();
		public ObservableCollection<MessageBubbleViewModel> Messages { get; set; }

		public SolidColorBrush SentBrush { get; set; }
		public SolidColorBrush ReceivedBrush { get; set; }
		public SolidColorBrush InvalidatedBrush { get; set; }

		public ChatWindowViewModel() { }

		public ChatWindowViewModel(string number)
		{
			this.Number = number;

			WindowTitleVisible = true;

			Messages = new ObservableCollection<MessageBubbleViewModel>();
			BindingOperations.CollectionRegistering += (sender, e) => BindingOperations.EnableCollectionSynchronization(Messages, collectionLockObject);

			Conversation = ConversationManager.Instance.GetConversation(Number);

			SentBrush = (SolidColorBrush)new BrushConverter().ConvertFrom(Conversation.Preferences.OptString(TelephonyNotificationsFeatureUI.SETTING_SENT_COLOR, DEFAULT_SENT_COLOR));
			SentBrush.Freeze();
			ReceivedBrush = (SolidColorBrush)new BrushConverter().ConvertFrom(Conversation.Preferences.OptString(TelephonyNotificationsFeatureUI.SETTING_RECEIVED_COLOR, DEFAULT_RECEIVED_COLOR));
			ReceivedBrush.Freeze();
			InvalidatedBrush = (SolidColorBrush)new BrushConverter().ConvertFrom(Conversation.Preferences.OptString(TelephonyNotificationsFeatureUI.SETTING_INVALIDATED_COLOR, DEFAULT_INVALIDATED_COLOR));
			InvalidatedBrush.Freeze();

			SendCommand = new DelegateCommand((obj) =>
			{
				TextBox textBox = (TextBox)obj;
				string message = textBox.Text;

				if (string.IsNullOrEmpty(message))
					return;

				Message m = new Message(null, textBox.Text, Utils.RemoveHundreds(Utils.UnixMiliseconds()).ToString());
				Conversation.Messages.Add(m);
				int index = Conversation.Messages.IndexOf(m);
				//Log.I(TAG, $"index is {index}");

				(PhoneLinkService.FeatureManagerI.Get(TelephonyNotificationsFeature.NAME) as TelephonyNotificationsFeature)?.SendSMSMessageRequest(number, textBox.Text);
				textBox.Clear();
			});

			foreach (Message message in Conversation.Messages)
			{
				Messages.Add(CreateMessageBubbleViewModel(message));
			}

			Conversation.Messages.CollectionChanged += Messages_CollectionChanged;
		}

		private MessageBubbleViewModel CreateMessageBubbleViewModel(Message message)
		{
			return new MessageBubbleViewModel
			{
				Message = message,
				SentBrush = SentBrush,
				ReceivedBrush = ReceivedBrush,
				InvalidatedBrush = InvalidatedBrush
			};
		}

		/// <summary>
		/// Returns <see cref="MessageBubbleViewModel"/> containing <paramref name="message"/> from <see cref="Messages"/>
		/// </summary>
		/// <param name="message"></param>
		/// <returns></returns>
		private MessageBubbleViewModel GetMessageBubbleViewModel(Message message)
		{
			foreach (MessageBubbleViewModel messageBubbleViewModel in Messages)
			{
				if (messageBubbleViewModel.Message == message)
					return messageBubbleViewModel;
			}

			return null;
		}

		private void Messages_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
			switch (e.Action)
			{
				case NotifyCollectionChangedAction.Add:
					//Log.W(TAG, "add");
					foreach (Message m in e.NewItems)
					{
						lock (collectionLockObject)
							Messages.Add(CreateMessageBubbleViewModel(m));
					}
					break;

				case NotifyCollectionChangedAction.Remove:
					foreach (Message m in e.OldItems)
					{
						//Log.D(TAG, $"removing {m}");
						MessageBubbleViewModel messageBubbleViewModel = GetMessageBubbleViewModel(m);
						if (messageBubbleViewModel != null)
							messageBubbleViewModel.Invalidated = true;
					}
					break;
			}
		}
	}
}
*/