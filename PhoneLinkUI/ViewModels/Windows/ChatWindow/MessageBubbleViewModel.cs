﻿/*using PhoneLinkBase.Feature.TelephonyNotifications;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace PhoneLinkUI.ViewModels.Windows.ChatWindow
{
	internal class MessageBubbleViewModel : INotifyPropertyChanged
	{
		#region Property changed
#pragma warning disable 67
		public event PropertyChangedEventHandler PropertyChanged;
#pragma warning restore 67
		#endregion

		public Thickness MarginSent { get; } = (Thickness)new ThicknessConverter().ConvertFrom("16 2 8 2");
		public Thickness MarginRecevied { get; } = (Thickness)new ThicknessConverter().ConvertFrom("8 2 16 2");

		public Message Message { get; set; }
		#region Simplified getters for xaml
		public string Number { get => Message.Number; }
		public string Body { get => Message.Body; }
		public string Date { get => Message.Date; }
		#endregion

		public string FormattedDate
		{
			get
			{
				DateTimeOffset date = DateTimeOffset.FromUnixTimeMilliseconds(long.Parse(Date));
				if (date.Date == DateTimeOffset.UtcNow.Date)
					return date.ToLocalTime().ToString("HH:mm");
				else
					return date.ToLocalTime().ToString("HH:mm, dd-MM-yyyy");
			}
		}

		public bool Invalidated { get; set; }

		public SolidColorBrush SentBrush { get; set; }
		public SolidColorBrush ReceivedBrush { get; set; }
		public SolidColorBrush InvalidatedBrush { get; set; }
		public SolidColorBrush TextBrush { get; set; }
		public SolidColorBrush BackgroundBrush
		{
			get
			{
				if (Invalidated)
					return InvalidatedBrush;
				else
					return string.IsNullOrEmpty(Message.Number) ? SentBrush : ReceivedBrush;
			}
		}

		public HorizontalAlignment HorizontalAlignment { get { return string.IsNullOrEmpty(Message.Number) ? HorizontalAlignment.Right : HorizontalAlignment.Left; } }
		public TextAlignment TextAlignment { get { return string.IsNullOrEmpty(Message.Number) ? TextAlignment.Right : TextAlignment.Left; } }
		public Thickness Margin { get { return string.IsNullOrEmpty(Message.Number) ? MarginSent : MarginRecevied; } }
		public FlowDirection FlowDirection { get { return string.IsNullOrEmpty(Message.Number) ? FlowDirection.RightToLeft : FlowDirection.LeftToRight; } }
		public SolidColorBrush PredictedTextBrush { get { return BackgroundBrush.Color.GetBrightness() > 128 ? Brushes.Black : Brushes.White; } }

		public ICommand ClickCommand { get; set; } = new DelegateCommand((obj) =>
		{
			MessageBubbleViewModel bubbleViewModel = (MessageBubbleViewModel)obj;
			bubbleViewModel.Invalidated = true;
		});
	}
}
*/