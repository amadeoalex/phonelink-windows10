﻿using PhoneLinkBase;
using PhoneLinkUI.Pages;
using PhoneLinkUI.ViewModels.Controls.NavigationMenu;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using static PhoneLinkUI.Controls.NavigationMenu;

namespace PhoneLinkUI.ViewModels.Windows.MainWindow
{
	class MainWindowViewModel : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		public NavigationMenuViewModel MenuViewModel { get; set; }
		public DelegateCommand WindowSizeChanged { get; set; }

		public MainWindowViewModel()
		{
			MenuViewModel = new NavigationMenuViewModel
			{
				CurrentMenu = new NavigationMenuItem()
				{
					Title = "Main Menu",
					InnerItems = new List<NavigationMenuItem>
					{
						new NavigationMenuItem {Title="Devices", IconChar="\uEA6C", Tag="devices", IsSelected=true, Page=new DevicesPage() },
						new NavigationMenuItem {Title="Pair", IconChar="\uE836", Tag="pair", Page=new PairingPage() },
						new NavigationMenuItem {Title="Settings", IconChar="\uE713", Tag="pair", InnerItems=new List<NavigationMenuItem>{
								new NavigationMenuItem { Title="UI Settings", IconChar="\uF259", Tag="ui_settings", Page=new UISettingsPage()}
							}
						},
						new NavigationMenuItem {Title="Control Test", IconChar="\uE7F4", Tag="control_test", Page=new ControlsTestPage() },
						new NavigationMenuItem {Title="Debug", IconChar="\uEBE8", Tag="debug", Page=new DebugPage() },
					}
				}
			};

			WindowSizeChanged = new DelegateCommand((size) =>
			{
				Log.D("Main Window", size);

				double windowSize = (double)size;
				NavigationMenuStyle style = windowSize < 700 ? NavigationMenuStyle.Compact : NavigationMenuStyle.Standard;
				if (MenuViewModel.MenuStyle != style)
					MenuViewModel.MenuStyle = style;
			});
		}
	}
}


/*Title = "Test Menu Title";
			Items = new ObservableCollection<MenuItemViewModel>() {
					new MenuItemViewModel{ IconChar="\uE80F", Title="Home", Page=new HomePage(), Selected=true},
					new MenuItemViewModel{ IconChar="\uE836", Title="Pair", Page=new PairingPage()},
					new MenuItemViewModel{ IconChar="\uE713", Title="Settings", Items=new ObservableCollection<MenuItemViewModel>{
							new MenuItemViewModel { IconChar="\uF259", Title="UI Settings", Page=new UISettingsPage()}
						}
					},
					new MenuItemViewModel{ IconChar="\uE734", Title="Features", Items=GetFeaturePages()},
					new MenuItemViewModel{ IconChar="\uEBE8", Title="Debug", Page=new DebugPage()}
				 };*/
