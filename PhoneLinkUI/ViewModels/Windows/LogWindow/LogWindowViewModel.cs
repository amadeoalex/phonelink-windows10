﻿using PhoneLinkBase;
using PhoneLinkBase.Debug;
using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Windows.Data;
using System.Windows.Input;

namespace PhoneLinkUI.ViewModels.Windows.LogWindow
{
	internal class LogWindowViewModel : StyleableWindowViewModel
	{
		private readonly object lockObject = new object();

		public ObservableCollection<LogEntryViewModel> Items { get; } = new ObservableCollection<LogEntryViewModel>();
		public ICollectionView ItemsView { get { return CollectionViewSource.GetDefaultView(Items); } }

		public string Filter { get; set; }
		public void OnFilterChanged()
		{
			ItemsView.Refresh();
		}

		public ICommand ClearLogCommand { get; set; }
		public ICommand ClearFilterCommand { get; set; }

		public LogWindowViewModel()
		{
			ClearLogCommand = new DelegateCommand((_) =>
			{
				Items.Clear();
				ItemsView.Refresh();
			});
			ClearFilterCommand = new DelegateCommand((_) => Filter = "");

			ItemsView.Filter = new Predicate<object>(o => CheckFilter(o as LogEntryViewModel));

			BindingOperations.EnableCollectionSynchronization(Items, lockObject);

			foreach (LogEntry entry in Log.Instance.Logs)
			{
				Items.Add(new LogEntryViewModel(entry));
			}

			((INotifyCollectionChanged)Log.Instance.Logs).CollectionChanged += LogHistory_CollectionChanged;
		}

		private bool CheckFilter(LogEntryViewModel logEntryViewModel)
		{
			return string.IsNullOrEmpty(Filter) || logEntryViewModel.Tag.Contains(Filter) || logEntryViewModel.Contents.Contains(Filter);
		}

		private void LogHistory_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
			if (e.Action == NotifyCollectionChangedAction.Add)
			{
				foreach (LogEntry entry in e.NewItems)
				{
					Items.Add(new LogEntryViewModel(entry));
				}
			}
		}
	}
}
