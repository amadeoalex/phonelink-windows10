﻿using PhoneLinkBase;
using PhoneLinkBase.Debug;
using System;
using System.Collections.Generic;
using System.Windows.Media;

namespace PhoneLinkUI.ViewModels.Windows.LogWindow
{
	internal class LogEntryViewModel
	{
		private static readonly Dictionary<ConsoleColor, SolidColorBrush> ConsoleColorBrushes = new Dictionary<ConsoleColor, SolidColorBrush>
		{
			{ ConsoleColor.Black, Brushes.Black },
			{ ConsoleColor.Blue, Brushes.Blue },
			{ ConsoleColor.Cyan, Brushes.Cyan },
			{ ConsoleColor.DarkBlue, Brushes.DarkBlue },
			{ ConsoleColor.DarkCyan, Brushes.DarkCyan },
			{ ConsoleColor.DarkGray, Brushes.DarkGray },
			{ ConsoleColor.DarkGreen, Brushes.DarkGreen },
			{ ConsoleColor.DarkMagenta, Brushes.DarkMagenta },
			{ ConsoleColor.DarkRed, Brushes.DarkRed },
			{ ConsoleColor.DarkYellow, Brushes.Yellow },
			{ ConsoleColor.Gray, Brushes.Gray },
			{ ConsoleColor.Green, Brushes.Green },
			{ ConsoleColor.Magenta, Brushes.Magenta },
			{ ConsoleColor.Red, Brushes.Red },
			{ ConsoleColor.White, Brushes.White },
			{ ConsoleColor.Yellow, Brushes.LightYellow }
		};

		public LogEntry LogEntry { get; private set; }

		public string Tag { get => $"[{LogEntry.Tag}]"; }
		public string Contents { get => LogEntry.Message.ToString(); }
		public SolidColorBrush ColorBrush { get; }
		public SolidColorBrush BackgroundColorBrush { get; }

		public LogEntryViewModel(LogEntry logEntry)
		{
			LogEntry = logEntry;

			ConsoleColor textColor = ConsoleColor.White;
			ConsoleColor backgroundColor = ConsoleColor.Black;
			switch (logEntry.LogLevel)
			{
				case LogLevel.Verbose:
					{
						textColor = ConsoleColor.Gray;
						backgroundColor = ConsoleColor.Black;
						break;
					}
				case LogLevel.Info:
					{
						textColor = ConsoleColor.DarkYellow;
						backgroundColor = ConsoleColor.Black;
						break;
					}
				case LogLevel.Debug:
					{
						textColor = ConsoleColor.White;
						backgroundColor = ConsoleColor.Black;
						break;
					}
				case LogLevel.Warning:
					{
						textColor = ConsoleColor.DarkRed;
						backgroundColor = ConsoleColor.Black;
						break;
					}
				case LogLevel.Error:
					{
						textColor = ConsoleColor.Red;
						backgroundColor = ConsoleColor.Black;
						break;
					}
			}

			ColorBrush = BrushFromConsoleColor(textColor);
			BackgroundColorBrush = BrushFromConsoleColor(backgroundColor);
		}

		private SolidColorBrush BrushFromConsoleColor(ConsoleColor consoleColor)
		{
			return ConsoleColorBrushes.ContainsKey(consoleColor) ? ConsoleColorBrushes[consoleColor] : Brushes.Black;
		}

	}
}
