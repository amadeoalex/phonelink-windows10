﻿using Org.BouncyCastle.Utilities.IO;
using PhoneLinkBase;
using PhoneLinkBase.UtilClasses;
using PhoneLinkUI.Controls;
using PhoneLinkUI.Pages;
using PhoneLinkUI.ViewModels.Controls.NavigationMenu;
using System;
using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using Windows.UI.ViewManagement;

namespace PhoneLinkUI.ViewModels.Windows.TestWindow
{
	class TestWindowViewModel : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		public ObservableCollection<Item> Items { get; set; } = new ObservableCollection<Item>();
		public ViewModelObservableCollection<ItemVM, Item> Items2 { get; set; }

		public DelegateCommand AddCommand { get; set; }
		public DelegateCommand RemoveCommand { get; set; }

		public Stack<Item> itemStack = new Stack<Item>();

		public TestWindowViewModel()
		{
			AddCommand = new DelegateCommand((_) =>
			{
				Item item = new Item(Utils.GetRandomInt(10, 9999999).ToString());
				itemStack.Push(item);
				Items.Add(item);
			});

			RemoveCommand = new DelegateCommand((_) =>
			{
				if (itemStack.Count > 0)
				{
					Item item = itemStack.Pop();
					Items.Remove(item);
				}
			});


			Items.Add(new Item("title 1"));
			Items.Add(new Item("title 2"));
			Items.Add(new Item("title 3"));
			Items.Add(new Item("title 4"));

			Items2 = new ViewModelObservableCollection<ItemVM, Item>(Items, (itemObject) => new ItemVM(itemObject));
		}

		public class Item
		{
			public string Title { get; set; }
			public Item(string title)
			{
				Title = title;
			}
		}

		public class ItemVM : IViewModelBase<Item>
		{
			public Item Model { get; set; }

			public string Title
			{
				get
				{
					return Model.Title;
				}
			}

			public ItemVM(Item item)
			{
				Model = item;
			}
		}
	}
}
