﻿using System.Windows.Controls;
using PhoneLinkUI.ViewModels.Pages;

namespace PhoneLinkUI.Pages
{
	/// <summary>
	/// Interaction logic for PairingPage.xaml
	/// </summary>
	public partial class PairingPage : Page
	{
		public PairingPage()
		{
			InitializeComponent();

			DataContext = new PairingPageViewModel();
		}
	}
}
