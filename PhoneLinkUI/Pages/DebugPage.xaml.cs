﻿using PhoneLink.ViewModels.Pages;
using System.Windows.Controls;

namespace PhoneLinkUI.Pages
{
	/// <summary>
	/// Interaction logic for DebugPage.xaml
	/// </summary>
	public partial class DebugPage : Page
    {
        public DebugPage()
        {
            InitializeComponent();

			DataContext = new DebugPageViewModel();
        }
    }
}
