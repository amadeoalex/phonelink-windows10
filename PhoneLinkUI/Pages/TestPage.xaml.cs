﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Media;

namespace PhoneLinkUI.Pages
{
	/// <summary>
	/// Interaction logic for TestPage.xaml
	/// </summary>
	public partial class TestPage : Page, INotifyPropertyChanged
	{
		#region Property changed
#pragma warning disable 67
		public event PropertyChangedEventHandler PropertyChanged;
#pragma warning restore 67
		#endregion

		private const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

		private readonly Random random = new Random();

		public SolidColorBrush Brush { get; set; }
		public string Text { get; set; }

		public TestPage()
		{
			InitializeComponent();
			DataContext = this;

			textBlock.Text = GetRandomString(32);
		}

		public TestPage(bool randomizeColor, string text = null) : this()
		{
			if (randomizeColor)
				root.Background = new SolidColorBrush(Color.FromArgb(255, (byte)random.Next(0, 200), (byte)random.Next(0, 200), (byte)random.Next(0, 200)));

			if (!string.IsNullOrEmpty(text))
				textBlock.Text = text;
		}

		private string GetRandomString(int length)
		{
			return new string(Enumerable.Repeat(chars, length)
			  .Select(s => s[random.Next(s.Length)]).ToArray());
		}
	}
}
