﻿using PhoneLinkUI.ViewModels.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PhoneLinkUI.Pages
{
	/// <summary>
	/// Interaction logic for DevicesPage.xaml
	/// </summary>
	public partial class DevicesPage : Page
	{
		public DevicesPage()
		{
			InitializeComponent();
			DataContext = new DevicesPageViewModel();
		}
	}
}
