﻿using Microsoft.Win32;
using PhoneLinkBase;
using System;
using System.ComponentModel;
using System.Json;
using System.Windows;
using System.Windows.Media;

namespace PhoneLinkUI
{
	public sealed class ThemeProvider : INotifyPropertyChanged
	{
		#region Property changed
#pragma warning disable 67
		public event PropertyChangedEventHandler PropertyChanged;
#pragma warning restore 67
		#endregion

		#region Instance
		private static readonly Lazy<ThemeProvider> lazyInstance = new Lazy<ThemeProvider>(() => new ThemeProvider(), true);

		/// <summary>
		/// Returns PhoneLinkService instance
		/// </summary>
		public static ThemeProvider Instance { get { return lazyInstance.Value; } }
		#endregion

		#region Constants
		public const string KEY_THEME_SETTINGS = "themeSettings";

		public const string SETTING_KEY_COLOR_SCHEME = "colorScheme";
		#endregion

		#region Static fields
		public static readonly string SETTING_KEY_COLOR_SCHEME_DEFAULT = nameof(ColorSchemes.Auto);
		#endregion

		public enum ColorSchemes
		{
			Auto,
			Light,
			Dark
		}

		#region Private fields
		private readonly JsonObject preferences;

		private SolidColorBrush InternalGlassBrush { get; set; }

		private ColorSchemes InternalColorScheme
		{
			get
			{
				string savedColorScheme = preferences.OptString(SETTING_KEY_COLOR_SCHEME, SETTING_KEY_COLOR_SCHEME_DEFAULT);
				return (ColorSchemes)Enum.Parse(typeof(ColorSchemes), savedColorScheme);
			}
			set
			{
				preferences[SETTING_KEY_COLOR_SCHEME] = value.ToString("g");
			}
		}
		#endregion

		private ThemeProvider()
		{
			preferences = PreferenceManager.GetObjectPreference(KEY_THEME_SETTINGS);

			SystemEvents.UserPreferenceChanged += (sender, e) =>
			{
				InternalGlassBrush = (SolidColorBrush)SystemParameters.WindowGlassBrush;
				if (e.Category == UserPreferenceCategory.General)
					InternalGlassBrush = (SolidColorBrush)SystemParameters.WindowGlassBrush;
			};
		}


		#region Static properties
		public static SolidColorBrush GlassBrush
		{
			get
			{
				return Instance.InternalGlassBrush;
			}
		}
		public static ColorSchemes ColorScheme
		{
			get
			{
				return Instance.InternalColorScheme;
			}
			set
			{
				Instance.InternalColorScheme = value;
			}
		}
		#endregion
	}
}