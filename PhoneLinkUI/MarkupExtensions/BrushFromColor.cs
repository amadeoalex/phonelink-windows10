﻿using Org.BouncyCastle.Asn1.Mozilla;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Markup;
using System.Windows.Media;

namespace PhoneLinkUI.MarkupExtensions
{
	class BrushFromColor : MarkupExtension
	{
		public Color Color { get; set; }

		public override object ProvideValue(IServiceProvider serviceProvider)
		{
			return new SolidColorBrush(Color);
		}
	}
}
