﻿using PhoneLinkBase;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;

namespace PhoneLinkUI.MarkupExtensions
{
	class StaticThemedResourceExtension : MarkupExtension
	{
		private object resourceKey;

		[ConstructorArgument(nameof(resourceKey))]
		public object ResourceKey
		{
			get
			{
				return resourceKey;
			}
			set
			{
				if (value == null) throw new ArgumentNullException(nameof(resourceKey));
				resourceKey = value;
			}
		}

		public StaticThemedResourceExtension(object resourceKey)
		{
			if (resourceKey == null) throw new ArgumentNullException(nameof(resourceKey));
			this.resourceKey = resourceKey;
		}


		public override object ProvideValue(IServiceProvider serviceProvider)
		{
			Log.D("TEST", ResourceKey.ToString());
			Debugger.Break();
			return null;
		}


	}
}
