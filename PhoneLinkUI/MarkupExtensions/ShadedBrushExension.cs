﻿using Org.BouncyCastle.Asn1.Mozilla;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Markup;
using System.Windows.Media;

namespace PhoneLinkUI.MarkupExtensions
{
	class ShadedBrushExtension : MarkupExtension
	{
		public SolidColorBrush Brush { get; set; }
		public float Percentage { get; set; }

		public override object ProvideValue(IServiceProvider serviceProvider)
		{
			return Percentage == 0 ? Brush : new SolidColorBrush(Brush.Color.GetShadedColor(Percentage));
		}
	}
}
