﻿using Org.BouncyCastle.Asn1.Mozilla;
using PhoneLinkUI.ValueConverters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Markup;
using System.Windows.Media;

namespace PhoneLinkUI.MarkupExtensions
{
	class BlackOrWhiteBrushExtension : MarkupExtension
	{
		public SolidColorBrush Brush { get; set; }

		public override object ProvideValue(IServiceProvider serviceProvider)
		{
			if(Brush==null)
				return DependencyProperty.UnsetValue;

			return Brush.Color.GetBrightness() > 128 ? Brushes.Black : Brushes.White;
		}
	}
}
