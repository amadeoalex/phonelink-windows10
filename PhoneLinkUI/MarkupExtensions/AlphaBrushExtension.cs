﻿using Org.BouncyCastle.Asn1.Mozilla;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Markup;
using System.Windows.Media;

namespace PhoneLinkUI.MarkupExtensions
{
	class AlphaBrushExtension : MarkupExtension
	{
		public SolidColorBrush Brush { get; set; }
		public byte Transparency { get; set; }

		public override object ProvideValue(IServiceProvider serviceProvider)
		{
			return Brush.Color.A == Transparency ? Brush : new SolidColorBrush(Brush.Color.GetTransparent(Transparency));
		}
	}
}
