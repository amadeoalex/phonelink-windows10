﻿using PhoneLinkBase;
using PhoneLinkBase.Feature.Dummy;
using PhoneLinkBase.Service;
using PhoneLinkUI.Notifications;
using PhoneLinkUI.UIService;
using PhoneLinkUI.WPFWindows;
using System;
using System.Json;
using System.Windows;
using Windows.UI;
using Windows.UI.Notifications;
using Windows.UI.ViewManagement;
using static PhoneLinkUI.ThemeProvider;

namespace PhoneLinkUI
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		private const string TAG = "App";

		protected override void OnStartup(StartupEventArgs e)
		{
			base.OnStartup(e);

			Utils.SetThreadCultureTo();

			SecurityUtils.Init();

			PhoneLinkService.Instance.Start();
			//PhoneLinkService.FeatureManagerI.AddFeature(new DummyFeature());
			//PhoneLinkService.FeatureManagerI.RegisterFeature(new TelephonyNotificationsFeature());
			//PhoneLinkService.FeatureManagerI.RegisterFeature(new ContentShareFeature());

			PhoneLinkUIService.WindowManagerI.ShowOneInstance(typeof(LogWindow));
			PhoneLinkUIService.WindowManagerI.ShowOneInstance(typeof(MainWindow));
			//Test();

			//ToastNotification toast = ToastHelper.GetSMSWithResponseToast("TEST", "234512566", "Wiadomość sms heh", "aasdad", "asd1asd");
			//DesktopNotificationManagerCompat.CreateToastNotifier().Show(toast);
		}

		protected override void OnExit(ExitEventArgs e)
		{
			base.OnExit(e);

			PhoneLinkUIService.Instance.Cleanup();

			PhoneLinkService.Instance.Cleanup();
			PreferenceManager.Instance.Cleanup();
		}

		private void Test()
		{
			const string xmlString = @"<toast launch=""asd"">
  
			  <visual>
				<binding template=""ToastGeneric"">
				  <text>Hello W""orld</text>
				  <text>This is a simple toast message</text>
				</binding>
			  </visual>
  
			</toast>";

			var doc = new Windows.Data.Xml.Dom.XmlDocument();
			doc.LoadXml(xmlString);
			var toast = new ToastNotification(doc);
			DesktopNotificationManagerCompat.CreateToastNotifier().Show(toast);
		}

		private void Test2()
		{
			const string xmlString = @"<toast launch=""""{""test""}"""">
							<visual>
								<binding template=""ToastGeneric"">
									<text>Alarm</text>
									<text>Get up now!!</text>
								</binding>
							</visual>
							<actions>

								<action arguments = ""Start""
										content = ""Start"" />

								<action arguments = ""dismiss""
										content = ""dismiss"" />

							</actions>
						</toast>";

			var doc = new Windows.Data.Xml.Dom.XmlDocument();
			doc.LoadXml(xmlString);
			var toast = new ToastNotification(doc);
			DesktopNotificationManagerCompat.CreateToastNotifier().Show(toast);
		}
	}
}
