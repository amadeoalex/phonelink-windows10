﻿using PhoneLinkBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media;

namespace PhoneLinkUI
{
	public class HLSColor
	{
		public float Luminosity { get; set; }
		public float Saturation { get; set; }
		public float Hue { get; set; }

		public HLSColor(Color color)
		{
			float red, green, blue;
			red = color.R / 255.0f;
			green = color.G / 255.0f;
			blue = color.B / 255.0f;

			float max = Math.Max(red, Math.Max(green, blue));
			float min = Math.Min(red, Math.Min(green, blue));
			Luminosity = (max + min) / 2;

			if (max == min)
			{
				Hue = Saturation = 0.0f;
			}
			else
			{
				float difference = max - min;
				if (Luminosity <= 0.5f)
					Saturation = difference / (max + min);
				else
					Saturation = difference / (2.0f - max - min);

				float redDistributon = (max - red) / difference;
				float greenDistributon = (max - green) / difference;
				float blueDistributon = (max - blue) / difference;

				if (max == red)
					Hue = blueDistributon - greenDistributon;
				else if (max == green)
					Hue = 2 + redDistributon - blueDistributon;
				else if (max == blue)
					Hue = 4 - redDistributon + greenDistributon;

				Hue *= 60;
				if (Hue < 0)
					Hue += 360;
			}


		}

		public void Darken(float percentChange)
		{
			//Log.D("HLS", $"darkening from {Luminosity} to {Luminosity * (1.0f - percentChange)}");
			Luminosity *= (1.0f - percentChange);
		}

		public void Lighten(float percentChange)
		{
			//Log.D("HLS", $"lightening from {Luminosity} to {Luminosity * (1.0f + percentChange)}");
			Luminosity *= (1.0f + percentChange);
		}

		public Color ToRGBColor()
		{
			byte r, g, b;

			if (Saturation == 0)
			{
				r = g = b = (byte)(Luminosity * 255);
			}
			else
			{
				float hue = Hue / 360;

				float q = (Luminosity < 0.5) ? (Luminosity * (1 + Saturation)) : (Luminosity + Saturation - (Luminosity * Saturation));
				float p = (2 * Luminosity) - q;

				r = (byte)(255 * HueToRGB(p, q, hue + (1.0f / 3)));
				g = (byte)(255 * HueToRGB(p, q, hue));
				b = (byte)(255 * HueToRGB(p, q, hue - (1.0f / 3)));
			}

			return Color.FromRgb(r, g, b);
		}

		private float HueToRGB(float p, float q, float t)
		{
			if (t < 0) t++;
			if (t > 1) t--;

			if (t < 1.0f / 6)
				return p + ((q - p) * 6.0f * t);
			else if (t < 1.0f / 2)
				return q;
			else if (t < 2.0f / 3)
				return p + ((q - p) * ((2.0f / 3) - t) * 6.0f);
			else
				return p;
		}

	}
}
