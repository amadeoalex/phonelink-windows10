﻿using Microsoft.QueryStringDotNET;
using System.Collections.Generic;

namespace PhoneLinkBase.Feature
{
	public interface IInteractable
	{
		/// <summary>
		/// Called when user performs interaction.
		/// </summary>
		/// <param name="arguments"></param>
		/// <param name="userInput"></param>
		void OnInteraction(QueryString arguments, IReadOnlyDictionary<string, string> userInput);
	}
}
