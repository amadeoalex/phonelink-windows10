﻿using Microsoft.QueryStringDotNET;
using PhoneLinkBase;
using PhoneLinkBase.Feature;
using System.Collections.Generic;

namespace PhoneLinkUI.FeatureUI
{
	internal class FeatureUIManager
	{
		private const string TAG = "Feature UI Handler Manager";

		private readonly Dictionary<string, IFeatureUI> features = new Dictionary<string, IFeatureUI>();

		/// <summary>
		/// Registers <see cref="IFeatureUI"/>
		/// </summary>
		/// <param name="handler"></param>
		public void RegisterHandler(IFeatureUI handler)
		{
			if (features.ContainsKey(handler.BaseFeature.Key))
			{
				Log.I(TAG, $"UI handler for feature {handler.BaseFeature.Key} already registered");
				return;
			}

			features[handler.BaseFeature.Key] = handler;
		}

		/// <summary>
		/// Returns <see cref="IFeatureUI"/> for provided <paramref name="baseFeatureName"/>
		/// Returns <see langword="null"/> if there is no registered UI handler for provided <paramref name="baseFeatureName"/>
		/// </summary>
		/// <param name="baseFeature"></param>
		/// <returns></returns>
		public IFeatureUI Get(string baseFeatureName)
		{
			return features.ContainsKey(baseFeatureName) ? features[baseFeatureName] : null;
		}

		public List<IFeatureUI> GetAll()
		{
			List<IFeatureUI> items = new List<IFeatureUI>();
			items.AddRange(features.Values);

			return items;
		}

		/// <summary>
		/// Notifies feature about user interaction.
		/// Notification feature must implement <see cref="IInteractable"/>.
		/// </summary>
		/// <param name="arguments"></param>
		/// <param name="userInput"></param>
		public void Interaction(QueryString arguments, IReadOnlyDictionary<string, string> userInput)
		{
			foreach (KeyValuePair<string, IFeatureUI> entry in features)
			{
				if (entry.Value is IInteractable && arguments["type"].Equals(entry.Value.BaseFeature.NetPackagePrefix))
					((IInteractable)entry.Value).OnInteraction(arguments, userInput);
			}
		}
	}
}
