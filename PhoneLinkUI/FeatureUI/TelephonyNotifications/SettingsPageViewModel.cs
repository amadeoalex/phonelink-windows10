﻿/*using PhoneLinkBase;
using PhoneLinkBase.Feature.TelephonyNotifications;
using PhoneLinkBase.Service;
using System.Collections.Generic;
using System.ComponentModel;
using System.Json;
using System.Linq;

namespace PhoneLinkUI.FeatureUI.TelephonyNotifications
{
	internal class SettingsPageViewModel : INotifyPropertyChanged
	{
		#region Property changed
#pragma warning disable 67
		public event PropertyChangedEventHandler PropertyChanged;
#pragma warning restore 67
		#endregion

		private readonly Dictionary<ToastHelper.Type, string> TOAST_TYPES = new Dictionary<ToastHelper.Type, string>
		{
			{ToastHelper.Type.SMS, Resources.Strings.PlainToast},
			{ToastHelper.Type.SMS_WITH_RESPONSE, Resources.Strings.ToastWithQuickResponse}
		};

		private readonly JsonObject preferences;

		public List<string> ToastTypeItems { get => TOAST_TYPES.Values.ToList(); }

		public string ToastTypeSelectedItem
		{
			get
			{
				int typeInt = preferences.OptInt(TelephonyNotificationsFeatureUI.SETTING_TOAST_TYPE, (int)TelephonyNotificationsFeatureUI.SETTING_TOAST_TYPE_DEFAULT);
				return TOAST_TYPES[(ToastHelper.Type)typeInt];
			}
			set
			{
				ToastHelper.Type type = TOAST_TYPES.First(x => x.Value == value).Key;
				preferences[TelephonyNotificationsFeatureUI.SETTING_TOAST_TYPE] = (int)type;
			}
		}

		public bool SyncSetting
		{
			get
			{
				return preferences.OptBool(TelephonyNotificationsFeature.SETTING_SYNC_ON_CONNECT, TelephonyNotificationsFeature.SETTING_SYNC_ON_CONNECT_DEFAULT);
			}

			set
			{
				preferences[TelephonyNotificationsFeature.SETTING_SYNC_ON_CONNECT] = value;
			}
		}

		public bool SyncNamesSetting
		{
			get
			{
				return preferences.OptBool(TelephonyNotificationsFeature.SETTING_SYNC_NAMES_ON_CONNECT, TelephonyNotificationsFeature.SETTING_SYNC_NAMES_ON_CONNECT_DEFAULT);
			}

			set
			{
				preferences[TelephonyNotificationsFeature.SETTING_SYNC_NAMES_ON_CONNECT] = value;
			}
		}

		public SettingsPageViewModel()
		{
			TelephonyNotificationsFeature notificationsFeature = (TelephonyNotificationsFeature)PhoneLinkService.FeatureManagerI.Get(TelephonyNotificationsFeature.NAME);
			preferences = notificationsFeature.Preferences;
		}
	}
}
*/