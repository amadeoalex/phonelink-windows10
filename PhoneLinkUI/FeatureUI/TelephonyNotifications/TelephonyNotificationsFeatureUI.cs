﻿/*

using Microsoft.QueryStringDotNET;
using PhoneLinkBase;
using PhoneLinkBase.Feature;
using PhoneLinkBase.Feature.TelephonyNotifications;
using PhoneLinkUI.CustomWindows;
using PhoneLinkUI.Notifications;
using PhoneLinkUI.UIService;
using PhoneLinkUI.ViewModels.Windows.ChatWindow;
using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Windows.UI.Notifications;

namespace PhoneLinkUI.FeatureUI.TelephonyNotifications
{
	internal class TelephonyNotificationsFeatureUI : IFeatureUI, IInteractable
	{
		private const string TAG = "Notifications UI Event Handler";

		#region Constants
		public const string SETTING_SENT_COLOR = "sentColor";
		public const string SETTING_RECEIVED_COLOR = "receivedColor";
		public const string SETTING_INVALIDATED_COLOR = "invalidatedColor";
		public const string SETTING_TOAST_TYPE = "toastType";

		public const ToastHelper.Type SETTING_TOAST_TYPE_DEFAULT = ToastHelper.Type.SMS;

		public const string UI_ACTION_VIEW = "view";
		public const string UI_ACTION_QUICK_RESPONSE = "quickResponse";
		public const string UI_ACTION_READ = "read";

		public const string UI_INPUT_RESPONSE = "response";
		#endregion

		#region Private fields
		private readonly TelephonyNotificationsFeature notificationsFeature;
		private readonly Dictionary<string, string> notificationBodies = new Dictionary<string, string>();
		#endregion

		public string DisplayName { get => Resources.Strings.TelephonyNotificationsDisplayName; }

		public FeatureBase BaseFeature { get => notificationsFeature; }
		public Page SettingsPage { get; }

		public TelephonyNotificationsFeatureUI(TelephonyNotificationsFeature notificationsFeature)
		{
			this.notificationsFeature = notificationsFeature ?? throw new ArgumentNullException(nameof(notificationsFeature));

			this.notificationsFeature.NotificationReceived += NotificationsFeature_NotificationReceived;

			SettingsPage = new SettingsPage();
		}


		public void OnInteraction(QueryString arguments, IReadOnlyDictionary<string, string> userInput)
		{
			NotificationUserInput notificationUserInput = (NotificationUserInput)userInput;

			//get stored notification body (because for some reason you can't add id to text element...)
			string body = notificationBodies[arguments[TelephonyNotificationsFeature.KEY_NUMBER]];
			string number = arguments[TelephonyNotificationsFeature.KEY_NUMBER];

			//send read confirmation package if there is no chat window currently shown to the user
			if (!PhoneLinkUIService.WindowManagerI.IsShown(number, typeof(ChatWindow)))
				notificationsFeature.SendSMSReadPackage(body);

			string action = arguments[TelephonyNotificationsFeature.KEY_ACTION];
			switch (action)
			{
				case UI_ACTION_VIEW:
					PhoneLinkUIService.WindowManagerI.Show(number, typeof(ChatWindow), new ChatWindowViewModel(arguments[TelephonyNotificationsFeature.KEY_NUMBER]));
					break;

				case UI_ACTION_QUICK_RESPONSE:
					if (notificationUserInput.ContainsKey(UI_INPUT_RESPONSE) && !string.IsNullOrEmpty(notificationUserInput[UI_INPUT_RESPONSE]))
						notificationsFeature.SendSMSMessageRequest(arguments[TelephonyNotificationsFeature.KEY_NUMBER], notificationUserInput[UI_INPUT_RESPONSE]);
					break;

				case UI_ACTION_READ:
					//no action needed, all messages are confirmed either right away if chat window is visible or after any interaction with notification
					break;

				default:
					Log.I(TAG, $"unknown action '{action}'");
					break;
			}
		}

		private void NotificationsFeature_NotificationReceived(object sender, NotificationReceivedEventArgs e)
		{
			Conversation conversation = e.Conversation;
			Message message = e.Message;

			//store message's body
			notificationBodies[conversation.Number] = message.Body;

			//send read confirmation package if chat window is already visible
			if (PhoneLinkUIService.WindowManagerI.IsShown(conversation.Number, typeof(ChatWindow)))
				notificationsFeature.SendSMSReadPackage(message.Body);

			ToastNotification toastNotification;
			//TODO: replace this whole switch block with just call with type as parameter
			ToastHelper.Type type = (ToastHelper.Type)notificationsFeature.Preferences.OptInt(SETTING_TOAST_TYPE, (int)SETTING_TOAST_TYPE_DEFAULT);
			switch (type)
			{
				case ToastHelper.Type.SMS:
					toastNotification = ToastHelper.GetToast(ToastHelper.Type.SMS, conversation.Number, message.Body);
					break;
				case ToastHelper.Type.SMS_WITH_RESPONSE:
					toastNotification = ToastHelper.GetToast(ToastHelper.Type.SMS_WITH_RESPONSE, conversation.Number, message.Body);
					break;
				case ToastHelper.Type.SMS_WITH_RESPONSE_AND_READ:
					toastNotification = ToastHelper.GetToast(ToastHelper.Type.SMS_WITH_RESPONSE_AND_READ, conversation.Number, message.Body);
					break;

				default:
					Log.W(TAG, $"toast type not recognized: {nameof(type)}");
					toastNotification = ToastHelper.GetToast(ToastHelper.Type.SMS_WITH_RESPONSE, conversation.Number, message.Body);
					break;
			}

			toastNotification.Tag = conversation.Number;
			toastNotification.Group = conversation.Number;
			toastNotification.Dismissed += OnToastNotificationDismissed;

			//TODO: consider updating the notification instead of recreating it
			DesktopNotificationManagerCompat.History.RemoveGroup(conversation.Number);
			DesktopNotificationManagerCompat.CreateToastNotifier().Show(toastNotification);
		}


		private void OnToastNotificationDismissed(ToastNotification sender, ToastDismissedEventArgs dismisslArgs)
		{
			//get notification launch arguments
			QueryString launchArguments = QueryString.Parse(sender.Content.ChildNodes[0].Attributes[0].InnerText);
			//get stored notification body (because for some reason you can't add id to text element...)
			string body = notificationBodies[launchArguments[TelephonyNotificationsFeature.KEY_NUMBER]];

			//continue only if user intentionally dismissed toast notification
			//if not, update the notification with mark as read button
			if (dismisslArgs.Reason == ToastDismissalReason.UserCanceled)
			{
				//remove notification from remote device
				notificationsFeature.SendSMSReadPackage(body);
			}
			else
			{
				//update toast notification to include mark as read button
				ToastNotification toastNotification = ToastHelper.GetToast(ToastHelper.Type.SMS_WITH_RESPONSE_AND_READ, launchArguments[TelephonyNotificationsFeature.KEY_NUMBER], body);
				toastNotification.Tag = sender.Tag;
				toastNotification.Group = sender.Group;
				toastNotification.SuppressPopup = true;

				DesktopNotificationManagerCompat.CreateToastNotifier().Show(toastNotification);
			}
		}
	}
}
*/