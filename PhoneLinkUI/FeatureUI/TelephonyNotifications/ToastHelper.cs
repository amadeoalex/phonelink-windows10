﻿/*using PhoneLinkBase.Feature.TelephonyNotifications;
using System.Web;
using Windows.UI.Notifications;

namespace PhoneLinkUI.FeatureUI.TelephonyNotifications
{
	public static class ToastHelper
	{
		private const string TAG = "Telephony Toast Helper";

		public enum Type
		{
			SMS = 0,
			SMS_WITH_RESPONSE = 1,
			SMS_WITH_RESPONSE_AND_READ = 2
		}

		/// <summary>
		/// Returns toast notification from template <paramref name="type"/> populated with provided parameters.
		/// </summary>
		/// <param name="type"></param>
		/// <param name="number"></param>
		/// <param name="body"></param>
		/// <returns></returns>
		public static ToastNotification GetToast(Type type, string number, string body)
		{
			string name = ConversationManager.Instance.GetConversation(number).Name;
			body = HttpUtility.HtmlEncode(body);

			string template = "<toast><visual></visual></toast>";

			switch (type)
			{
				case Type.SMS:
					template = $@"
						<toast launch=""type={TelephonyNotificationsFeature.PREFIX}&amp;{TelephonyNotificationsFeature.KEY_ACTION}={TelephonyNotificationsFeatureUI.UI_ACTION_VIEW}&amp;{TelephonyNotificationsFeature.KEY_NUMBER}={number}"">
							<visual>
								<binding template=""ToastGeneric"">
									<text hint-maxLines=""1"">{name}</text>
									<text>{body}</text>
								</binding>
							</visual>
						</toast>";
					break;
				case Type.SMS_WITH_RESPONSE:
					template = $@"
						<toast launch=""type={TelephonyNotificationsFeature.PREFIX}&amp;{TelephonyNotificationsFeature.KEY_ACTION}={TelephonyNotificationsFeatureUI.UI_ACTION_VIEW}&amp;{TelephonyNotificationsFeature.KEY_NUMBER}={number}"">
							<visual>
								<binding template=""ToastGeneric"">
									<text hint-maxLines=""1"">{name}</text>
									<text>{body}</text>
								</binding>
							</visual>
							<actions>
								<input id=""{TelephonyNotificationsFeatureUI.UI_INPUT_RESPONSE}"" type=""text"" placeHolderContent=""{Resources.Strings.Response}"" />
								<action
									content=""{Resources.Strings.Send}""
									hint-inputId=""{TelephonyNotificationsFeatureUI.UI_INPUT_RESPONSE}""
									arguments=""type={TelephonyNotificationsFeature.PREFIX}&amp;{TelephonyNotificationsFeature.KEY_ACTION}={TelephonyNotificationsFeatureUI.UI_ACTION_QUICK_RESPONSE}&amp;{TelephonyNotificationsFeature.KEY_NUMBER}={number}"" />
							</actions>
						</toast>";

					break;
				case Type.SMS_WITH_RESPONSE_AND_READ:
					template = $@"
						<toast launch=""type={TelephonyNotificationsFeature.PREFIX}&amp;action={TelephonyNotificationsFeatureUI.UI_ACTION_VIEW}&amp;number={number}"">
							<visual>
								<binding template=""ToastGeneric"">
									<text hint-maxLines=""1"">{name}</text>
									<text>{body}</text>
								</binding>
							</visual>
							<actions>
								<input id=""{TelephonyNotificationsFeatureUI.UI_INPUT_RESPONSE}"" type=""text"" placeHolderContent=""Response"" />
								<action
									content=""{Resources.Strings.Send}""
									hint-inputId=""{TelephonyNotificationsFeatureUI.UI_INPUT_RESPONSE}""
									arguments=""type={TelephonyNotificationsFeature.PREFIX}&amp;action={TelephonyNotificationsFeatureUI.UI_ACTION_QUICK_RESPONSE}&amp;number={number}"" />
								<action
									content=""{Resources.Strings.MarkAsRead}""
									arguments=""type={TelephonyNotificationsFeature.PREFIX}&amp;action={TelephonyNotificationsFeatureUI.UI_ACTION_READ}&amp;number={number}"" />
							</actions>
						</toast>";
					break;
			}

			return new ToastNotification(UIUtils.GetXmlDocument(template));
		}
	}
}
*/