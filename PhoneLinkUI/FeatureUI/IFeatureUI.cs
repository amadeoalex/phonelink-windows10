﻿using PhoneLinkBase.Feature;
using System.Windows.Controls;

namespace PhoneLinkUI.FeatureUI
{
	internal interface IFeatureUI
	{
		string DisplayName { get; }
		FeatureBase BaseFeature { get; }
		Page SettingsPage { get; }
	}
}
