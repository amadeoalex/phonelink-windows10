﻿/*using Microsoft.QueryStringDotNET;
using PhoneLinkBase;
using PhoneLinkBase.Feature;
using PhoneLinkBase.Feature.ContentShare;
using PhoneLinkBase.Service.Network;
using PhoneLinkUI.Notifications;
using PhoneLinkUI.Pages;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using Windows.UI.Notifications;

namespace PhoneLinkUI.FeatureUI.ContentShare
{
	internal class ContentShareFeatureUI : IFeatureUI, IInteractable
	{
		private const string TAG = "Content Share Feature UI";

		#region Constants
		public const string KEY_ACTION = "action";
		public const string KEY_SHARE_TYPE = "shareType";
		public const string KEY_FILE_PATH = "filePath";

		public const string UI_ACTION_VIEW = "view";

		public const string SETTING_SAVE_PATH = "savePath";
		#endregion

		#region Private fields
		private readonly ContentShareFeature contentShareFeature;
		#endregion

		public string DisplayName { get => Resources.Strings.ContentShareDisplayName; }

		public FeatureBase BaseFeature { get => contentShareFeature; }
		public Page SettingsPage => new TestPage(); 

		public ContentShareFeatureUI(ContentShareFeature contentShareFeature)
		{
			this.contentShareFeature = contentShareFeature ?? throw new ArgumentNullException(nameof(contentShareFeature));

			this.contentShareFeature.ContentReceived += ContentShareFeature_ContentReceived;
		}

		public void OnInteraction(QueryString arguments, IReadOnlyDictionary<string, string> userInput)
		{
			string shareType = arguments[KEY_SHARE_TYPE];
			switch (shareType)
			{
				case ContentShareFeature.TYPE_URL:
					Process.Start(arguments[ContentShareFeature.KEY_TYPE_URL_URL]);
					break;

				case ContentShareFeature.TYPE_IMG:
					Process.Start(arguments[KEY_FILE_PATH]);
					break;
			}
		}

		private void ContentShareFeature_ContentReceived(object sender, ContentShareReceivedEventArgs e)
		{
			NetPackage contentPackage = e.NetPackage;
			switch (contentPackage.Type)
			{
				case ContentShareFeature.TYPE_URL:
					ToastNotification urlNotification = ToastHelper.GetURLToast(contentPackage[ContentShareFeature.KEY_TYPE_URL_URL]);

					DesktopNotificationManagerCompat.CreateToastNotifier().Show(urlNotification);
					break;

				case ContentShareFeature.TYPE_IMG:
					//get image bytes
					byte[] imageBytes = Convert.FromBase64String(contentPackage[ContentShareFeature.KEY_TYPE_FILE_BASE64]);
					string fileName = contentPackage[ContentShareFeature.KEY_TYPE_FILE_NAME];

					//store file as temporary if user wants image to be only copied to clipboard
					string mode = contentPackage[ContentShareFeature.KEY_TYPE_FILE_MODE];
					if (mode == ContentShareFeature.KEY_TYPE_FILE_MODE_CLIPBOARD)
						fileName = "temp" + Path.GetExtension(fileName);

					//get path where shared files should be stored, default to Pictures folder if custom path is not provided
					string defaultFilePath = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures) + "/" + Resources.Strings.ContentShareFilesFolder;
					string filePath = contentShareFeature.Preferences.OptString(SETTING_SAVE_PATH, defaultFilePath) + "/" + fileName;

					//save image
					Directory.CreateDirectory(Path.GetDirectoryName(filePath));
					using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
					{
						fileStream.Write(imageBytes, 0, imageBytes.Length);
						fileStream.Flush();
					}

					//copy image to clipboard if needed
					if (mode == ContentShareFeature.KEY_TYPE_FILE_MODE_SHARE_AND_CB || mode == ContentShareFeature.KEY_TYPE_FILE_MODE_CLIPBOARD)
					{
						BitmapSource bitmapSource = new BitmapImage(new Uri(filePath));
						Application.Current.Dispatcher.Invoke(() => Clipboard.SetImage(bitmapSource));
					}

					ToastNotification imageNotification = ToastHelper.GetImageToast(contentPackage[ContentShareFeature.KEY_TYPE_FILE_NAME], filePath);
					DesktopNotificationManagerCompat.CreateToastNotifier().Show(imageNotification);
					break;

				case ContentShareFeature.TYPE_FILE:

					break;
			}
		}
	}
}
*/