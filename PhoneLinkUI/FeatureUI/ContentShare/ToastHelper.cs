﻿/*using PhoneLinkBase.Feature.ContentShare;
using System.Web;
using Windows.UI.Notifications;

namespace PhoneLinkUI.FeatureUI.ContentShare
{
	internal static class ToastHelper
	{
		private const string TAG = "Content Share Toast Helper";

		public static ToastNotification GetURLToast(string url)
		{
			url = HttpUtility.HtmlEncode(url);

			string template = $@"
				<toast launch=""type={ContentShareFeature.PREFIX}&amp;{ContentShareFeatureUI.KEY_ACTION}={ContentShareFeatureUI.UI_ACTION_VIEW}&amp;{ContentShareFeatureUI.KEY_SHARE_TYPE}={ContentShareFeature.TYPE_URL}&amp;{ContentShareFeature.KEY_TYPE_URL_URL}={url}"">
					<visual>
						<binding template=""ToastGeneric"">
							<text hint-maxLines=""1"">{Resources.Strings.ContentShareLinkShared}</text>
							<text>{url}</text>
						</binding>
					</visual>
				</toast>";

			return new ToastNotification(UIUtils.GetXmlDocument(template));
		}

		public static ToastNotification GetImageToast(string fileName, string filePath)
		{
			string imageUri = $"file://{filePath}";
			filePath = HttpUtility.HtmlEncode(filePath);

			string template = $@"
				<toast launch=""type={ContentShareFeature.PREFIX}&amp;{ContentShareFeatureUI.KEY_ACTION}={ContentShareFeatureUI.UI_ACTION_VIEW}&amp;{ContentShareFeatureUI.KEY_SHARE_TYPE}={ContentShareFeature.TYPE_IMG}&amp;{ContentShareFeatureUI.KEY_FILE_PATH}={filePath}"">
					<visual>
						<binding template=""ToastGeneric"">
							<text hint-maxLines=""1"">{Resources.Strings.ContentShareImageShared}</text>
							<text>{fileName}</text>
							<image placement=""appLogoOverride"" hint-crop=""circle"" src=""{imageUri}""/>
						</binding>
					</visual>
				</toast>";

			return new ToastNotification(UIUtils.GetXmlDocument(template));
		}

		public static ToastNotification GetFileToast(string fileName)
		{
			string template = $@"
				<toast launch=""type={ContentShareFeature.PREFIX}&amp;{ContentShareFeatureUI.KEY_ACTION}={ContentShareFeatureUI.UI_ACTION_VIEW}&amp;{ContentShareFeatureUI.KEY_SHARE_TYPE}={ContentShareFeature.TYPE_FILE}"">
					<visual>
						<binding template=""ToastGeneric"">
							<text hint-maxLines=""1"">{Resources.Strings.ContentShareFileShared}</text>
							<text>{fileName}</text>
						</binding>
					</visual>
				</toast>";

			return new ToastNotification(UIUtils.GetXmlDocument(template));
		}
	}
}
*/