﻿using DeviceNS;
using PhoneLinkBase.Feature.Dummy;
using PhoneLinkBase.Service;
using PhoneLinkBase.Service.Network;
using System.Windows.Input;

namespace PhoneLinkUI.FeatureUI.Dummy
{
	internal class SettingsPageViewModel
	{
		private const string TAG = "Dummy Settings Page";

		private DummyFeature dummyFeature;

		public ICommand CommandSendPing { get; }

		public SettingsPageViewModel()
		{
			//dummyFeature = (DummyFeature)PhoneLinkService.FeatureManagerI.Get(typeof(DummyFeature);
			CommandSendPing = new DelegateCommand((_) =>
			{
				//dummyFeature.SendPing();
			});
		}

	}
}
