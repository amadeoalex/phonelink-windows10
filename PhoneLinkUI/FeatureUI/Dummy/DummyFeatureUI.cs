﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using PhoneLinkBase.Feature;
using PhoneLinkBase.Feature.Dummy;
using PhoneLinkUI.FeatureUI.Dummy;

namespace PhoneLinkUI.FeatureUI.Dummy
{
	class DummyFeatureUI : IFeatureUI
	{
		private readonly DummyFeature dummyFeature;

		public string DisplayName { get => Resources.Strings.DummyFeatureDisplayName; }
		public FeatureBase BaseFeature { get => dummyFeature; }
		public Page SettingsPage { get; }

		public DummyFeatureUI(DummyFeature dummyFeature)
		{
			this.dummyFeature = dummyFeature ?? throw new ArgumentNullException(nameof(dummyFeature));

			SettingsPage = new SettingsPage();
		}
	}
}
