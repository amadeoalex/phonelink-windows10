﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media;

namespace PhoneLinkUI.ValueConverters
{
	internal class ShadedBrushConverter : BaseValueConverter<ShadedBrushConverter>
	{
		public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if(value == null)
				return DependencyProperty.UnsetValue;

			SolidColorBrush brush = (SolidColorBrush)value;
			float percentChange = float.Parse((string)parameter);

			return percentChange == 0 ? brush : brush.GetShadedBrush(percentChange);
		}

		public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
