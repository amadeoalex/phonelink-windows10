﻿using PhoneLinkBase.DeviceNS.Connection.Pairing;
using System;
using System.Globalization;
using System.Windows;

namespace PhoneLinkUI.ValueConverters
{
	internal class PairingStatusToIconCharConverter : BaseValueConverter<PairingStatusToIconCharConverter>
	{
		public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			PairingProcess.Status status = (PairingProcess.Status)value;

			if(status == PairingProcess.Status.Initializing)
				return '\uEDAB';
			else if (status == PairingProcess.Status.Pairing)
				return '\uE9F5';
			else if (status == PairingProcess.Status.WaitingForAccept)
				return '\uE712';
			else if (status == PairingProcess.Status.Finished)
				return '\uE8FB';
			else if (status == PairingProcess.Status.Error)
				return '\uEA39';
			else
				return '\uE9CE';

		}

		public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
