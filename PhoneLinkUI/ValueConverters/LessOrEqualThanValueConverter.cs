﻿using System;
using System.Globalization;

namespace PhoneLinkUI.ValueConverters
{
	class LessOrEqualThanValueConverter : BaseValueConverter<LessOrEqualThanValueConverter>
	{
		public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			int firstValue = System.Convert.ToInt32(value);
			int secondValue = System.Convert.ToInt32(parameter);

			return firstValue <= secondValue;
		}

		public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
