﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media;

namespace PhoneLinkUI.ValueConverters
{
	internal class AlphaBrushConverter : BaseValueConverter<AlphaBrushConverter>
	{
		public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value == null)
				return DependencyProperty.UnsetValue;

			SolidColorBrush brush = (SolidColorBrush)value;
			byte transparency = byte.Parse((string)parameter);

			return brush.Color.A == transparency ? brush : new SolidColorBrush(brush.Color.GetTransparent(transparency));
		}

		public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
