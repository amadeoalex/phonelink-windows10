﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media;

namespace PhoneLinkUI.ValueConverters
{
	internal class BlackOrWhiteBrushConverter : BaseValueConverter<BlackOrWhiteBrushConverter>
	{
		public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value == null)
				return DependencyProperty.UnsetValue;

			SolidColorBrush brush = (SolidColorBrush)value;
			return brush.Color.GetBrightness() > 128 ? Brushes.Black : Brushes.White;
		}

		public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
