﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Markup;

namespace PhoneLinkUI.ValueConverters
{
	/// <summary>
	/// Base for all value converters with direct XAML usage.
	/// </summary>
	/// <typeparam name="T">Type of converter</typeparam>
	internal abstract class BaseValueConverter<T> : MarkupExtension, IValueConverter where T : class, new()
	{
		#region Markup extension support
		/// <summary>
		/// Static instance of converter
		/// </summary>
		private static T converter = null;

		/// <summary>
		/// Provides static instance of value converter
		/// </summary>
		/// <param name="serviceProvider"></param>
		/// <returns></returns>
		public override object ProvideValue(IServiceProvider serviceProvider)
		{
			return converter ?? (converter = new T());
		}
		#endregion

		public abstract object Convert(object value, Type targetType, object parameter, CultureInfo culture);

		public abstract object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture);
	}
}
