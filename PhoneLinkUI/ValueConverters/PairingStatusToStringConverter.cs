﻿using PhoneLinkBase.DeviceNS.Connection.Pairing;
using System;
using System.Globalization;
using System.Windows;

namespace PhoneLinkUI.ValueConverters
{
	internal class PairingStatusToStringConverter : BaseValueConverter<PairingStatusToStringConverter>
	{
		public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			PairingProcess.Status status = (PairingProcess.Status)value;
			return Resources.Strings.ResourceManager.GetString(status.ToString("g"));
		}

		public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
