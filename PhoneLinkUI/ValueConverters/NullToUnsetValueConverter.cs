﻿using System;
using System.Globalization;
using System.Windows;

namespace PhoneLinkUI.ValueConverters
{
	internal class NullToUnsetValueConverter : BaseValueConverter<NullToUnsetValueConverter>
	{
		public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return value ?? DependencyProperty.UnsetValue;
		}

		public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
