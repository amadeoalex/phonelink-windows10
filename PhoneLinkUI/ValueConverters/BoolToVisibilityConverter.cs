﻿using System;
using System.Globalization;
using System.Windows;

namespace PhoneLinkUI.ValueConverters
{
	class BoolToVisibilityConverter : BaseValueConverter<BoolToVisibilityConverter>
	{
		public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			bool visible = (bool)value;
			return visible ? Visibility.Visible : (parameter != null ? Visibility.Collapsed : Visibility.Hidden);
		}

		public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
