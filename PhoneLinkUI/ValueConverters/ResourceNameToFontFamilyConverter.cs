﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Media;

namespace PhoneLinkUI.ValueConverters
{
	class ResourceNameToFontFamilyConverter : BaseValueConverter<ResourceNameToFontFamilyConverter>
	{
		public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value == null)
				return (FontFamily)Application.Current.FindResource("SymbolThemeFontFamily");

			string resourceName = (string)value;
			return (FontFamily)Application.Current.FindResource(resourceName);
		}

		public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
