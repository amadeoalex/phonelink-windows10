﻿using DeviceNS;
using System;
using System.Globalization;
using System.Windows;

namespace PhoneLinkUI.ValueConverters
{
	internal class DeviceTypeToIconCharConverter : BaseValueConverter<DeviceTypeToIconCharConverter>
	{
		public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			switch ((string)value)
			{
				case Device.TYPE_PHONE:
					return '\uE8EA';
				case Device.TYPE_PC:
					return '\uE977';
				case Device.TYPE_LAPTOP:
					return '\uE7F8';
				case Device.TYPE_TABLET:
					return '\uE8CC';
				default:
					return '\uE9CE';
			}
		}

		public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
