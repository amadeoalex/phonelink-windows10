﻿using System.Windows;
using System.Windows.Controls;

namespace PhoneLinkUI.AttachedProperties
{
	/// <summary>
	/// Scroll view attached property used to scroll to the bottom every time new item is added.
	/// Scrolling occurs only if scroll view is already scrolled to bottom.
	/// Additionally, scroll to bottom occurs after scroll view has been loaded.
	/// </summary>
	public class AutoScrollProperty : BaseAttachedProperty<AutoScrollProperty, bool>
	{
		public override void OnValueChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
		{
			if (!(sender is ScrollViewer scrollViewer))
				return;

			if ((bool)args.NewValue)
			{
				scrollViewer.ScrollChanged -= ScrollViewer_ScrollChanged;
				scrollViewer.ScrollChanged += ScrollViewer_ScrollChanged;

				scrollViewer.Loaded -= ScrollViewer_Loaded;
				scrollViewer.Loaded += ScrollViewer_Loaded;
			}
			else
			{
				scrollViewer.ScrollChanged -= ScrollViewer_ScrollChanged;
				scrollViewer.Loaded -= ScrollViewer_Loaded;
			}
		}

		private void ScrollViewer_Loaded(object sender, RoutedEventArgs e)
		{
			ScrollViewer scrollViewer = (ScrollViewer)sender;
			scrollViewer.ScrollToVerticalOffset(scrollViewer.ExtentHeight);
		}

		private void ScrollViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
		{
			ScrollViewer scrollViewer = (ScrollViewer)sender;
			if (scrollViewer.ScrollableHeight - scrollViewer.VerticalOffset <= 1 && e.ExtentHeightChange != 0)
			{
				scrollViewer.ScrollToVerticalOffset(scrollViewer.ExtentHeight);
			}
		}
	}
}
